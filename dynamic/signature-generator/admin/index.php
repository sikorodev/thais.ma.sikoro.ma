<!-- saved from url=(0090)http://mendes-creation.com/clients/thais/signature-mail-new-v6/badr.elyacoubi@thais.ma.htm -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
.content-classy{display:block;margin:auto;width:100%;background:#CCC;margin-top:40px;}
.content-classy .content-form{}
.content-classy .content-form table{width:100%;}
.content-classy .content-form td, .content-classy .content-form th{border:1px solid #fff;padding:10px;}
.content-classy .content-form .fields{display:inline-block;vertical-align:middle;}
.content-classy .content-form input.small{max-width:40px;}
.msg{display:none;margin-bottom:20px;border:2px solid #fff;padding:10px;color:#060;}
.js-sbmt{display:block;margin-top:20px;border:2px solid #fff;padding:10px;}
.js-sbmt a{display:inline-block;padding:0 16px;line-height:32px;background:#fff;color:#000;text-decoration:none;}
.js-sbmt a.btn-add-ville{float:right;}
</style>
<script src="https://code.jquery.com/jquery.js" type="text/javascript"></script>
</head>
<body>

<div class="content-classy">
	<form>
    	<div class="content-form">
        	<table cellpadding="0" cellspacing="0">
            	<thead>
                	<th>Id</th>
                    <th>Poste</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Action</th>
                </thead>
                <tbody>
                	
                </tbody>
            </table>
        </div>
        <div class="msg">
            </div>
        <div class="js-sbmt">
        	<a href="#" class="btn-save" title="">Valider</a>
            <a href="#" class="btn-add-ville" title="">Ajouter un Nouvea</a>
        </div>
    </form>
</div>


<script type="text/javascript">
$( document ).ready(function() {
	var days = ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi']
	var datas = "";
	$.getJSON('../js/Thais-signatures-mails-v1.0.json?cache='+Math.random(), function (json) {
		var data = [];
		data = JSON.parse(JSON.stringify(json["Staffs"]))
		datas = data;
		var html = "";
		for (i in data)
		{
			var daysS = '';
			html = html + '<tr><td>'+data[i].id+'<input data-parent="" data-name="id" type="hidden" class="idTag" value="'+data[i].id+'"</td><td><input data-parent="" data-name="poste" type="text" name="post" class="post" value="'+data[i].poste+'"></td><td><input data-parent="" data-name="first_name" type="text" value="'+data[i].first_name+'" class="firstName" /></td><td><input data-parent="" data-name="last_name" type="text" class="lastName" value="'+data[i].last_name+'" /></td><td><input data-parent="contacts" data-name="email" type="text" class="email" value="'+data[i].contacts.email+'" /></td><td><input data-parent="contacts" data-name="Mobile" type="text" class="mobile" value="'+data[i].contacts.Mobile+'" /></td><td><a href="" class="js-delete">Delete</a></td></tr>'
		 	//console.log(data[i].name)
		}
		$(".content-form table tbody").append(html);
		change()
		//console.log(data)
	});
	$(".btn-add-ville").click(function(){
		var lent = $(".content-form table tbody tr").size();
		var j = lent;
		$(".content-form table tbody").append('<tr><td>'+lent+'<input data-parent="" data-name="id" type="hidden" class="idTag" value="'+lent+'"</td><td><input data-parent="" data-name="poste" type="text" name="post" class="post" value=""></td><td><input data-parent="" data-name="first_name" type="text" value="" class="firstName" /></td><td><input data-parent="" data-name="last_name" type="text" class="lastName" value="" /></td><td><input data-parent="contacts" data-name="email" type="text" class="email" value="" /></td><td><input data-parent="contacts" data-name="Mobile" type="text" class="mobile" value="" /></td><td><a href="" class="js-delete">Delete</a></td></tr>');
		datas.push({"id":""+lent,"statut":"true","poste":"","first_name":"","last_name":"","other_name":"","contacts":{"email":"","Mobile":""}});
		//console.log(datas)
		change()
		return false;
	})
	$(".btn-save").click(function(){
		var newData = JSON.stringify(datas);
		jQuery.post('save.php', {
			newData: newData
		}, function(response){
			$(".msg").html("Le json est enregistrer").fadeIn();
			setTimeout(function(){ $(".msg").fadeOut() }, 3000);
			console.log(datas)
		})
		return false;
	})
	function change()
	{
		$(".js-delete").click(function(){
			var indx = $(this).parents("tr").index();
			datas.splice(indx, 1);
			$(this).parents("tr").fadeOut().remove();
			return false;
		})
		$("input[type='text']").on("change paste keyup", function() {
		   var indx = $(this).parents("tr").index();
		   var dataNam = $(this).attr("data-name")
		   var dataParent = $(this).attr("data-parent")
			var daysD = "";
			if(dataParent != "") datas[indx][dataParent][dataNam] = $(this).val();
			else datas[indx][dataNam] = $(this).val();
			//datas[indx].name = $(this).val();
			console.log(datas)
		});
		
	}
	
	if (!String.prototype.replaceLast) {
		String.prototype.replaceLast = function(find, replace) {
			var index = this.lastIndexOf(find);
	
			if (index >= 0) {
				return this.substring(0, index) + replace + this.substring(index + find.length);
			}
	
			return this.toString();
		};
	}
	//getArray();
	//console.log(questions);
});
</script>
</body>
</html>