<?php
/**
 * Template Name: Espace Presse
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>
<section class="presse">
    <div class="container">
        <?php
            while ( have_posts() ) : the_post();
        ?>
        	<?php
				$value1 = get_post_meta( get_the_ID(), '_document1', true );
				$your_pdf_src1 = wp_get_attachment_url($value1);
				$document1Titre = get_post_meta( get_the_ID(), '_document_titre', true );
				
				$value2 = get_post_meta( get_the_ID(), '_document2', true );
				$your_pdf_src2 = wp_get_attachment_url($value2);
				$document2Titre = get_post_meta( get_the_ID(), '_document2_titre', true );
				
				$value3 = get_post_meta( get_the_ID(), '_document3', true );
				$your_pdf_src3 = wp_get_attachment_url($value3);
				$document3Titre = get_post_meta( get_the_ID(), '_document3_titre', true );
				
				$value4 = get_post_meta( get_the_ID(), '_document4', true );
				$your_pdf_src4 = wp_get_attachment_url($value4);
				$document4Titre = get_post_meta( get_the_ID(), '_document4_titre', true );
				
			?>
            <h1 class="h1-title"><?php the_title(); ?></h1>
            <?php the_content(); ?> 
            <?php if($your_pdf_src1 != "" || $your_pdf_src2 != "" || $your_pdf_src3 != "" || $your_pdf_src4 != ''){ ?>
            <div class="content">
                <div class="bloc bold g12">
                <p>
                <?php if($your_pdf_src1 != ""){ ?>
                <span class="bloc g12"><strong><?php echo sanitize_text_field( $document1Titre ); ?></strong></span>
                <span class="bloc g12"><a target="_blank" href="<?php echo $your_pdf_src1; ?>">Télécharger</a></span>
                <?php } ?>
                </p>
                <p>
                <?php if($your_pdf_src2 != ""){ ?>
                <span class="bloc g12"><strong><?php echo sanitize_text_field( $document2Titre ); ?></strong></span>
                <span class="bloc g12"><a target="_blank" href="<?php echo $your_pdf_src2; ?>">Télécharger</a></span>
                <?php } ?>
                </p> 
                </div> 
                <div class="bloc g12">
                <p>
                <?php if($your_pdf_src3 != ""){ ?>
                <span class="bloc g12"><strong><?php echo sanitize_text_field( $document3Titre ); ?></strong></span>
                <span class="bloc g12"><a target="_blank" href="<?php echo $your_pdf_src3; ?>">Télécharger</a></span>
                <?php } ?>
                </p> 
                <p>
                <?php if($your_pdf_src4 != ""){ ?>
                <span class="bloc g12"><strong><?php echo sanitize_text_field( $document4Titre ); ?></strong></span>
                <span class="bloc g12"><a target="_blank" href="<?php echo $your_pdf_src4; ?>">Télécharger</a></span>
                <?php } ?>
                </p>
                </div> 
             </div>	 
             <?php } ?>
        <?php
            endwhile;
        ?>
    </div>
</section>

<?php
get_footer();
