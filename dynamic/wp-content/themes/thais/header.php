<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package thais
 */

?>
<!doctype html>
<!--[if IE 8]>
<html class="no-js lt-ie10 lt-ie9" lang="fr"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="fr"><![endif]-->
<!--[if gt IE 9]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5BNGG8C');</script>
<!-- End Google Tag Manager -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0">
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="referrer" content="no-referrer-when-downgrade">
<meta name="robots" content="all">
<!-- {{ seomaticMeta.robots }}-->
<meta name="keywords" content="Facility Management, Property Management, Maintenance multi technique, Prestations multi services, Gestion locative, Syndic de copropriété, Gardiennage, Nettoyage, Accueil, Espaces vert, Electricité, Climatisation/Ventilation, Plomberie, Menuiserie, Second œuvre, Portes/ Barrière automatique, Contrôle d’accès, Détection incendie, Moyen de levage, Vidéo surveillance">
<!-- {{ seomaticMeta.seoKeywords }}-->
<meta name="description" content="THAÏS est un acteur majeur du Property Management et du Facility Management sur le territoire marocain.">
<!-- {{ seomaticMeta.seoDescription }}-->
<meta name="generator" content="SEOmatic">
<!-- SEOmatic-->
<link rel="canonical" href="<?php echo get_home_url(); ?>">
<!-- {{ seomaticMeta.canonicalUrl }} (defaults to craft.request.url)-->
<link rel="alternate" href="<?php echo get_home_url(); ?>" hreflang="x-default">
<meta name="geo.region" content="Casablanca">
<!-- {{ seomaticIdentity.address.addressRegion }}-->
<meta name="geo.position" content="33.592998,-7.620568">
<!-- {{ seomaticIdentity.location.geo.latitude }},{{ seomaticIdentity.location.geo.longitude }}-->
<meta name="ICBM" content="33.592998,-7.620568">
<!-- {{ seomaticIdentity.location.geo.latitude }},{{ seomaticIdentity.location.geo.longitude }}-->
<meta name="geo.placename" content="THAÏS">
<!-- {{ seomaticIdentity.location.name }}-->
<!-- Dublin Core basic info-->
<meta name="dcterms.Identifier" content="<?php echo get_home_url(); ?>">
<!-- {{ seomaticMeta.canonicalUrl }} (defaults to craft.request.url)-->
<meta name="dcterms.Format" content="text/html">
<!-- text/html-->
<meta name="dcterms.Relation" content="THAÏS">
<!-- {{ seomaticSiteMeta.siteSeoName }}-->
<meta name="dcterms.Language" content="fr">
<!-- {{ craft.locale }}-->
<meta name="dcterms.Publisher" content="THAÏS">
<!-- {{ seomaticSiteMeta.siteSeoName }}-->
<meta name="dcterms.Type" content="text/html">
<!-- text/html-->
<meta name="dcterms.Coverage" content="<?php echo get_home_url(); ?>">
<!-- {{ siteUrl }}-->
<meta name="dcterms.Rights" content="Copyright ©2018 THAÏS.">
<!-- {{ seomaticHelper.ownerCopyrightNotice }}-->
<meta name="dcterms.Title" content="Spécialiste de la gestion immobilière">
<!-- {{ seomaticMeta.seoTitle }}-->
<meta name="dcterms.Creator" content="Sikoro">
<!-- {{ seomaticCreator.name }}-->
<meta name="dcterms.Subject" content="Gestion immobilière, Asset Management, Property Management, Facility Management, Project Management, Gestion locative, Gestion de copropriétés, Maintenance multi-techniques, biens immobiliers">
<!-- {{ seomaticMeta.seoKeywords }}-->
<meta name="dcterms.Contributor" content="THAÏS">
<!-- {{ seomaticSiteMeta.siteSeoName }}-->
<meta name="dcterms.Date" content="2018-03-22">
<!-- {{ now | date('Y-m-d') }}-->
<meta name="dcterms.Description" content="THAÏS est un acteur majeur du Property Management et du Facility Management sur le territoire marocain.">
<!-- {{ seomaticMeta.seoDescription }}-->
<!-- Facebook OpenGraph-->
<meta property="og:type" content="website">
<!-- {{ seomatic.og.type }}-->
<meta property="og:locale" content="fr_FR">
<!-- {{ seomatic.og.locale }}-->
<meta property="og:url" content="<?php echo get_home_url(); ?>">
<!-- {{ seomatic.og.url }}-->
<meta property="og:title" content="Spécialiste de la gestion immobilière | THAÏS">
<!-- {{ seomatic.og.title }}-->
<meta property="og:description" content="THAÏS est un acteur majeur du Property Management et du Facility Management sur le territoire marocain.">
<!-- {{ seomatic.og.description }}-->
<meta property="og:image" content="<?php echo get_template_directory_uri() ?>/assets/media/share/Fb-thais-Share-Image@2X-2.png">
<!-- {{ seomatic.og.image }}-->
<meta property="og:image:type" content="image/png">
<!-- {{ seomatic.og.image:type }}-->
<meta property="og:image:width" content="2400">
<!-- {{ seomatic.og.image:width }}-->
<meta property="og:image:height" content="1260">
<!-- {{ seomatic.og.image:height }}-->
<meta property="og:site_name" content="THAÏS">
<!-- {{ seomatic.og.site_name }}-->
<meta property="og:see_also" content="https://twitter.com/THAIS_Maroc">
<!-- {{ seomatic.og.see_also[0] }}-->
<meta property="og:see_also" content="https://web.facebook.com/THAIS-Maroc">
<!-- {{ seomatic.og.see_also[1] }}-->
<meta property="og:see_also" content="https://www.linkedin.com/company-beta/10635970/">
<!-- {{ seomatic.og.see_also[2] }}-->
<!-- Twitter Card-->
<meta name="twitter:card" content="summary_large_image">
<!-- {{ seomatic.twitter.card }}-->
<meta name="twitter:site" content="@https://twitter.com/THAIS_Maroc">
<!-- {{ seomatic.twitter.site }}-->
<meta name="twitter:creator" content="@https://twitter.com/THAIS_Maroc">
<!-- {{ seomatic.twitter.creator }}-->
<meta name="twitter:title" content="Spécialiste de la gestion immobilière | THAÏS">
<!-- {{ seomatic.twitter.title }}-->
<meta name="twitter:description" content="THAÏS est un acteur majeur du Property Management et du Facility Management sur le territoire marocain.">
<!-- {{ seomatic.twitter.description }}-->
<meta name="twitter:image" content="<?php echo get_template_directory_uri() ?>/assets/media/share/Fb-thais-Share-Image@2X.png">
<!-- {{ seomatic.twitter.image }}-->
<!-- Humans.txt authorship http://humanstxt.org-->
<link type="text/plain" rel="author" href="/humans.txt">
<!-- Domain verification-->
<meta name="google-site-verification" content="8f9fd1b8d8831864">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/media/share/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/media/share/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_template_directory_uri() ?>/assets/media/share/site.webmanifest">
<link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/assets/media/share/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#AC1652">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo get_template_directory_uri() ?>/favicon.ico" type="image/x-icon">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5BNGG8C"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
 <?php
			$imgLogo = get_option( 'imageLogo' );
			$imgLogosrc = wp_get_attachment_image_src( $imgLogo, 'full' );
			$you_have_img_slug = is_array( $imgslugsrc );
			?>
<a class="c-nav__close" href=""></a>
<input type="hidden" id="pathTheme" value="<?php echo get_template_directory_uri() ?>">
<header>
  <div class="header-content">
    <div class="row">
      <div class="columns">
        <div class="head-col">
          <div class="top-bar">
          	<?php
				wp_nav_menu( array(
					'theme_location'  => 'topMenu',
					'menu_class' => 'top-menu',
					'container' => false,
					'fallback_cb' => false,
				));
				
			?>
          </div>
          <div class="soc-ico"><a class="facebook" title="Facebook" href="<?php echo get_option( 'linkFacebock' ); ?>" target="_blank"><i class="c-fsik__icon--facebook"></i></a><a class="twitter" title="Twitter" href="<?php echo get_option( 'linkTwitter' ); ?>" target="_blank"><i class="c-fsik__icon--twitter"></i></a><a class="linkedin" title="LinkedIn" href="<?php echo get_option( 'linkLinkedin' ); ?>" target="_blank"><i class="c-fsik__icon--linkedin"></i></a><a class="linkedin" title="LinkedIn" href="<?php echo get_option( 'linkInstagram' ); ?>" target="_blank"><i class="c-fsik__icon--instagram"></i></a></div>
        </div>
        <div class="logo align-right"><a href="<?php echo get_home_url(); ?>"><img class="preload-me" src="<?php echo $imgLogosrc[0]; ?>" alt="THAÏS"></a></div>
        <div class="o-nav">
          <?php
		      global $post;
			  global $wpdb;
		  	  $post_id = $post ? $post->ID : -1;
			  $has_post_parent = $post && $post->post_parent;
			  $top = $has_post_parent ? array_pop( get_post_ancestors($post_id) ) : $post_id;
			  
			  $menu_name = 'headerMenu';
			  $locations = get_nav_menu_locations();
			  $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
			  $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'ASC' ) );
			?>
			<ul class="c-nav__level0">
				<?php
				$count = 0;
				$submenu = false;
				$i = 0;
				$idParent=0;
				foreach( $menuitems as $item ):
					$link = $item->url;
					$title = $item->title;
					// item does not have a parent so menu_item_parent equals 0 (false)
					if ( !$item->menu_item_parent ):
					// save this id for later comparison with sub-menu items
						$parent_id = $item->ID;
						$current_id = $item->object_id;
						$is_current_class = ($current_id == $top) ? 'is-active' : '';
						$i = 0;
						$has_children = $wpdb->get_var("SELECT COUNT(meta_id) FROM wp_postmeta WHERE meta_key='_menu_item_menu_item_parent' AND meta_value='".$item->ID."'");
					?>
				
					<li>
                    	<?php
							$idParent = $item->object_id;
						?>
						<a href="<?php if($has_children > 0) echo "#"; else echo $link; ?>" <?php if($is_current_class != "") echo 'class="'.$is_current_class.'"'; ?>>
							<?php echo $title; ?>
						</a>
					<?php endif; ?>
					
					<?php if ( $parent_id == $item->menu_item_parent ): ?>
                    <?php $i++; ?>
							<?php
								$id = $item->object_id;
								$is_current = ($id == $post_id) ? 'is-active' : '';
							?>
							
							<?php if ( !$submenu ): $submenu = true; ?>
                            <div class="c-nav__show"><span class="c-fsik__icon--arrowRight"></span></div>
							<ul class="c-nav__level1">
							<?php endif; ?>
								<?php
									if($i == 1) {
										echo '<li class="c-nav__hide"><span class="c-fsik__icon--arrowLeft"></span><span class="c-text">'.get_the_title( $idParent ).'</span></li>';
									}
								?>
								<li <?php if($title == "Property Management") echo 'class="has-sub-submenu"'; ?>>
									<a href="<?php echo $link; ?>" class="title <?php echo $is_current; ?> <?php if(($post->post_title == get_the_title( 474 ) || $post->post_title == get_the_title( 486 )) && $title == "Property Management") echo "is-active"; ?>"><?php echo $title; ?></a>
                                    <?php
										if($title == "Property Management")
										{
									?>
                                    	<div class="c-nav__show"><span class="c-fsik__icon--arrowRight"></span></div>
                                    	<ul class="c-nav__level1">
                                        	<li class="c-nav__hide"><span class="c-fsik__icon--arrowLeft"></span><span class="c-text"><?php echo $title; ?></span></li>
                                            <li><a href="<?php echo esc_url( get_page_link( 486 ) ); ?>" class="<?php if($post->post_title == get_the_title( 486 )) echo "is-active"; ?>" ><?php echo get_the_title( 486 ); ?></a></li>
                                            <li><a href="<?php echo esc_url( get_page_link( 474 ) ); ?>" class="<?php if($post->post_title == get_the_title( 474 )) echo "is-active"; ?>" ><?php echo get_the_title( 474 ); ?></a></li>
                                          </ul
                                    ><?php
										}
									?>
								</li>
				
							<?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ): ?>
							</ul>
						<?php $submenu = false; endif; ?>
			
					<?php endif; ?>
                     <?php if ( $menuitems[ $count + 1 ]->menu_item_parent != $parent_id ): ?>
   				 </li>                           
    <?php $submenu = false; endif; ?>
			
			<?php $count++; endforeach; ?>
			</ul>
        </div>
        <div class="c-nav__icon"><span></span></div>
      </div>
    </div>
  </div>
</header>