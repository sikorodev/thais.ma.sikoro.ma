<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>
<main>
 <?php
 			
            while ( have_posts() ) : the_post();
        ?>
        <?php $url = get_the_post_thumbnail_url(211, 'full'); ?>
		<section class="heading">
        <div class="hero-content">
          <div class="hero-inner">
            <hgroup>
              <h1><?php echo the_title(); ?></h1>
            </hgroup>
            <div class="c-breadcrumb">
              <div class="row expanded">
                <div class="columns">
                  <div class="label">Vous êtes ici  :</div>
                  <?php // Breadcrumb navigation
					 if (is_page() && !is_front_page() || is_single() || is_category()) {
					 echo '<ul class="c-breadcrumb__list">';
					 echo '<li><a title="Accueil" rel="nofollow" href="'.get_home_url().'">Accueil</a></li>';
					
					 if (is_page()) {
					 $ancestors = get_post_ancestors($post);
					
					 if ($ancestors) {
					 $ancestors = array_reverse($ancestors);
					
					 foreach ($ancestors as $crumb) {
					 echo '<li><a href="'.get_permalink($crumb).'">'.get_the_title($crumb).'</a></li>';
					 }
					 }
					 }
					 if (is_single()) {
					 $category = get_the_category();
					 echo '<li><a href="'.get_category_link($category[0]->cat_ID).'">'.$category[0]->cat_name.'</a></li>';
					 }
					
					 if (is_category()) {
					 $category = get_the_category();
					 echo '<li>'.$category[0]->cat_name.'</li>';
					 }
					
					 // Current page
					 if (is_page() || is_single()) {
					 echo '<li><span>'.get_the_title().'</span></li>';
					 }
					 echo '</ul>';
					 }
					?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hero-second-illus">
          <div class="hero-bg lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" style="background:url(<?php echo $url; ?>); background-size:cover; background-position:50%;"></div>
        </div>
      </section>
      <section class="page-content post-wrap">
      	<div class="row">
        	<div class="large-8 medium-6 small-12 column">
            	<?php $urlImg = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                <div class="visu">
                	<img class="lazy swiper-lazy" data-src="<?php echo $urlImg; ?>" data-original="<?php echo $urlImg; ?>" src="<?php echo $urlImg; ?>" alt="" />
                    <div class="post-date"><span class="entry-month"><?php echo get_the_date('M',get_the_ID()); ?></span><span class="entry-date updated"><?php echo get_the_date('d',get_the_ID()); ?></span><span class="entry-year"><?php echo get_the_date('Y',get_the_ID()); ?></span></div>
                </div>
            	<?php the_content(); ?> 
                <div class="nav-post">
                    <div class="nav-previous"><?php previous_post_link( '%link', '<i class="c-fsik__icon--prev"></i><span class="meta-nav">Onglet précédent</span> <span class="post-title">%title</span>' ); ?></div>
 					<div class="nav-next"><?php next_post_link( '%link', '<i class="c-fsik__icon--next"></i><span class="meta-nav">Onglet suivant</span> <span class="post-title">%title</span>' ); ?></div>
                </div>
            </div>
            <div class="large-4 medium-6 small-12 column single-related">
            	<h3>Articles similaires</h3>
                <?php
				$tags = wp_get_post_tags(get_the_ID());
				if ($tags) {
				$tag_ids = array();
				foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
				 
				$args=array(
				'tag__in' => $tag_ids,
				'post__not_in' => array(get_the_ID()),
				'showposts'=>4,
				'caller_get_posts'=>1
				);
				$my_query = new wp_query($args);
				if( $my_query->have_posts() ) {
				while ($my_query->have_posts()) {
				$my_query->the_post();
				$imageURL = wp_get_attachment_url( get_post_thumbnail_id($post->ID) )
				 
				?>
				 <div class="item-related">
                 	<div class="post-image">
                    	<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><img title="<?php the_title(); ?>" alt="_" src="<?php print $imageURL;?>"/></a>
                    </div>
                     
                    <h4 class="post-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
                 </div>
				
				<?php
				}
				}
				wp_reset_query();
				}
				?>
            </div>
        </div>
      </section>
	
             
<?php
            endwhile;
        ?>

</main>

<?php
get_footer();
