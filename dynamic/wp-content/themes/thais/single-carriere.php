<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>
<main>
 <?php
 			
            while ( have_posts() ) : the_post();
        ?>
        <?php $url = get_the_post_thumbnail_url(211, 'full'); ?>
		<section class="heading">
        <div class="hero-content">
          <div class="hero-inner">
            <hgroup>
              <h1><?php echo the_title(); ?></h1>
            </hgroup>
            <div class="c-breadcrumb">
              <div class="row expanded">
                <div class="columns">
                  <div class="label">Vous êtes ici  :</div>
                  <ul class="c-breadcrumb__list">
                    <li><a href="<?php echo get_home_url(); ?>">Accueil</a></li>
                    <li><a href="<?php echo get_page_link(213); ?>">Carrière</a></li>
                    <li><?php echo the_title(); ?></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hero-second-illus">
          <div class="hero-bg lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" style="background:url(<?php echo $url; ?>); background-size:cover; background-position:50%;"></div>
        </div>
      </section>
      <section class="page-content single-carriere">
      	<div class="row">
        <?php if(get_post_meta(get_the_ID(),'_carriereAddMetaboxesinfoscarriere_isActive',true ) == true){ ?>
          <?php /*?><div class="large-6 medium-12 small-12 column">
            <div class="job-wrapper">
              <ul class="job-lists">
                	<li>
                    	<div class="job-item">
                        	<article>
                            	<div class="row">
                                    <div class="large-12 medium-12 small-12 column"><span class="label">Ville</span><span class="city-name"><?php echo wp_strip_all_tags(get_the_term_list( get_the_ID(), 'villes', ' ', ' , ', ' ') ); ?></span></div>
                                    <div class="large-12 medium-12 small-12 column js-toggle-content">
                                      <div class="toggle-content"><span class="label">Mission :</span>
                                        <?php the_content(); ?>
                                      </div>
                                    </div>
                                    <div class="large-12 medium-4 small-12 column"><span class="label">profil</span><span class="profil-desc"><?php echo get_post_meta(get_the_ID(),'_carriereAddMetaboxesinfoscarriere_profile',true ); ?></span></div>
                                    
                                </div>
                            </article>
                        </div>
                    </li>
              </ul>
            </div>
          </div><?php */?>
          <?php } ?>
          <?php if(get_post_meta(get_the_ID(),'_carriereAddMetaboxesinfoscarriere_isActive',true ) == true){ ?>
          <div class="large-12 medium-12 small-12 column">
          	  <div class="form-postuler js-postuler-form"  data-title="<?php echo get_the_title($post->ID); ?>" data-link="<?php echo get_permalink(); ?>"></fieldset>
              		<?php echo do_shortcode( '[contact-form-7 id="237" title="carriére" linkjob="'.get_permalink().'"]' ); ?>
              </div>
          </div>
          <?php }else{ ?>
          		<div class="large-12 medium-12 small-12 column">
                  <div class="form-postuler js-postuler-form"  data-title="<?php echo get_the_title($post->ID); ?>" data-link="<?php echo get_permalink(); ?>"></fieldset>
                        <?php echo do_shortcode( '[contact-form-7 id="237" title="carriére" linkjob="'.get_permalink().'"]' ); ?>
                  </div>
              </div>
          <?php } ?>
        </div>
        	
      </section>
	
             
<?php
            endwhile;
        ?>

</main>

<?php
get_footer();
