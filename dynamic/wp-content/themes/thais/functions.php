<?php
/**
 * thais functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package thais
 */




add_action('after_switch_theme', 'mytheme_setup_options');

function mytheme_setup_options () {
	
}


if ( ! function_exists( 'thais_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function thais_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on thais, use a find and replace
	 * to change 'thais' to the name of your theme in all the template files.
	 */
	//load_theme_textdomain( 'thais', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	

	
	
register_nav_menus( array(
		'topMenu' =>'Top Menu',
		'headerMenu' =>'Header Menu',
		'footerMenu' =>'Footer Menu 1',
		'footerMenu2' =>'Footer Menu 2',
	) );

	// Thumbnails Size
	add_image_size( 'itemL', 470, 500, true );
	add_image_size( 'itemM', 353, 200, true );
	add_image_size( 'itemS', 200, 215, true );
	add_image_size( 'intervenantS', 150, 150, true );


	// This theme uses wp_nav_menu() in one location.

}
endif;
add_action( 'after_setup_theme', 'thais_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */


/**
 * Enqueue scripts and styles.
 */
function thais_scripts() {
	wp_enqueue_style( 'thais-style', get_stylesheet_uri() );
	wp_enqueue_style( 'thais-style-main', get_template_directory_uri() . '/assets/styles/styles.css');


}
add_action( 'wp_enqueue_scripts', 'thais_scripts' );









/**
 * Load App Config
 */
require_once get_template_directory() . '/app/config.php';



function doctype_opengraph($output) {
	return $output . '
    xmlns:og="http://opengraphprotocol.org/schema/"
    xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'doctype_opengraph');


function listingPost($atts, $content = null) {
	extract(shortcode_atts(array(
		"pagination" => 'true',
		"query" => '',
		"category" => '',
		"showposts" => '4',
		"type" => '1',
	), $atts));
	global $wp_query,$paged,$post;
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	if($pagination == 'true'){
		$query .= '&paged='.$paged;
	}
	if(!empty($category)){
		$query .= '&"post_status"="any"&category_name='.$category.'&showposts='.$showposts;
	}
	if(!empty($query)){
		$query .= $query;
	}
	//$wp_query->query($query);
	$wp_queryPointV = new WP_Query(array( 
		'posts_per_page' => $showposts,
		'orderby' => 'date',
		'order' => 'DESC',
		'category_name' => $category,
		'paged' => $paged,
	));
	ob_start();
	$cat = get_terms( 'category', array( 'name__like' => $category ) );
	$urlCat = esc_url( get_category_link( $cat[0]->term_id ) ) ;
	?>
    <div class="listing-items-cat">
        <div class="row">
          <?php while ($wp_queryPointV->have_posts()) : $wp_queryPointV->the_post(); ?>
                <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
                <div class="large-4 medium-6 small-12 column">
                  <article class="et_pb_post">
                    <div class="et_pb_image_container"> 
                    <a href="<?php echo get_permalink(); ?>" class="entry-featured-image-url"> <img src="<?php echo $url; ?>" /> </a> 
                    <span class="post-date"><?php echo get_the_date('d F Y',get_the_ID()); ?></span>
                    </div>
                    <!-- .et_pb_image_container -->
                    <h2 class="entry-title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div class="post-content">
                      <p><?php the_excerpt('',FALSE,''); ?></p>
                      <a href="<?php echo get_permalink(); ?>" class="more-link">Lire plus</a></div>
                  </article>
                </div>
          <?php endwhile; ?>  
          <div class="large-12 medium-12 small-12 column">
                <?php
                    $wp_query = $wp_queryPointV;
                    if(function_exists('wp_paginate')):
                        wp_paginate();  
                    else :
                        the_posts_pagination( array(
                            'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                            'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
                        ) );
                    endif;
                ?>
            </div>
          </div>
      </div>
	<?php $wp_queryPointV = null; $wp_queryPointV = $temp;
	$wp_query = null; $wp_query = $temp;
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}
add_shortcode("LstingPost", "listingPost");


function listCategory($atts, $content = null) {
	extract(shortcode_atts(array(
		"pagination" => 'true',
		"query" => '',
		"category" => '',
		"showposts" => '4',
		"type" => '1',
	), $atts));
	global $wp_query,$paged,$post;
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	if($pagination == 'true'){
		//$query .= '&paged='.$paged;
	}
	if(!empty($category)){
		$query .= '&"post_status"="any"&category_name='.$category.'&showposts='.$showposts;
	}
	if(!empty($query)){
		$query .= $query;
	}
	//$wp_query->query($query);
	$wp_queryPointV = new WP_Query(array( 
		'post_status' => 'any',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'ASC',
		'category_name' => $category,
		'showposts' => $showposts,
		'pagination' => true,
		'paged' => $paged,
	));
	ob_start();
	$cat = get_category_by_slug($category);
	$urlCat = esc_url( get_category_link( $cat->term_id ) ) ;
	$i=0;
	?>
    <div class="swiper-container main-slider loading">
          <div class="swiper-wrapper">
          <?php while ($wp_queryPointV->have_posts()) : $wp_queryPointV->the_post(); ?>
          <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
          <div class="swiper-slide">
              <figure class="slide-bgimg lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" style="background-image:url(<?php echo $url; ?>);"><img class="entity-img lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" src="<?php echo $url; ?>"></figure>
              <div class="content">
                <h2 class="title"><?php the_title(); ?></h2>
                <span class="caption animated"><?php the_excerpt('',FALSE,''); ?></span>
                <div class="cta animated"><a class="btn-link" href="<?php echo get_permalink(); ?>">En savoir plus</a></div>
              </div>
            </div>
          <?php endwhile; ?>  
            
          </div>
          <!-- If we need navigation buttons-->
          <div class="swiper-button-prev swiper-button-white"></div>
          <div class="swiper-button-next swiper-button-white"></div>
        </div>
        <!-- Thumbnail navigation-->
        <div class="swiper-container nav-slider loading">
          <div class="swiper-wrapper">
          	<?php while ($wp_queryPointV->have_posts()) : $wp_queryPointV->the_post(); ?>
            	<?php $url = get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>
            	<div class="swiper-slide">
                  <figure class="slide-bgimg lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" style="background-image:url(<?php echo $url; ?>);"><img class="entity-img lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" src="<?php echo $url; ?>"></figure>
                  <div class="content">
                    <h2 class="title"><?php the_title(); ?></h2>
                  </div>
                </div>
            <?php endwhile; ?>  
            
          </div>
        </div>
	<?php $wp_queryPointV = null; $wp_queryPointV = $temp;
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}
add_shortcode("categoryList", "listCategory");


function clientFunction($atts, $content = null) {
	extract(shortcode_atts(array(
		"pagination" => 'true',
		"query" => '',
		"category" => '',
		"showposts" => '4',
	), $atts));
	global $wp_query,$paged,$post;
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	if($pagination == 'true'){
		$query .= '&paged='.$paged;
	}
	if(!empty($category)){
		$query .= '&"post_status"="any"&category_name='.$category.'&showposts='.$showposts;
	}
	if(!empty($query)){
		$query .= $query;
	}
	//$wp_query->query($query);

	$wp_query = new WP_Query(array( 
		'post_status' => 'any',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'ASC',
		'post_type' => "Client",
	));
	ob_start();
	?>
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	 	 <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
      	<?php $link =  get_post_meta( get_the_ID(), '_clientAddMetaboxeslienclient_lienclientD', true ); ?>
      	<div class="logo-grid-inner"><a href="<?php echo $link; ?>" target="_blank" title=""><img src="<?php echo $url; ?>" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" class="lazy swiper-lazy" alt=""></a></div>
      <?php endwhile; ?>
      
	<?php $wp_query = null; $wp_query = $temp;
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}
add_shortcode("client", "clientFunction");

function sliderFunction($atts, $content = null) {
	extract(shortcode_atts(array(
		"pagination" => 'true',
		"query" => '',
		"category" => '',
		"showposts" => '4',
	), $atts));
	global $wp_query,$paged,$post;
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	if($pagination == 'true'){
		$query .= '&paged='.$paged;
	}
	if(!empty($category)){
		$query .= '&"post_status"="any"&category_name='.$category.'&showposts='.$showposts;
	}
	if(!empty($query)){
		$query .= $query;
	}
	//$wp_query->query($query);

	$wp_query = new WP_Query(array( 
		'post_status' => 'any',
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'ASC',
		'post_type' => "Slider",
	));
	ob_start();
	?>
   <section class="intro">
    <div class="section-wrapper">
      <div class="section-content">
        <div class="row expanded">
          <div class="columns">
            <div id="progress"></div>
            <div class="swiper-container js-swiper-hp-intro">
              <div class="swiper-wrapper">
              <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
              <div class="swiper-slide">
                  <div class="swiper-lazy visu lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" style=" background-size:cover; background-position:50%;">
                      <div class="sik-loader swiper-lazy-preloader">
                        <div class="largeBox"></div>
                        <div class="smallBox"></div>
                      </div>
                    </div>
                  <div class="inner-side">
                    <?php the_content(); ?>
                  </div>
                </div>
              <?php endwhile; ?>
              </div>
            </div>
            <div class="swiper-pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
	<?php $wp_query = null; $wp_query = $temp;
	$content = ob_get_contents();
	ob_end_clean();
	return $content;
}
add_shortcode("HomeSlider", "sliderFunction");


function baw_remove_custom_field_meta_boxes() {
    remove_post_type_support( 'post','custom-fields' );
    remove_post_type_support( 'page','custom-fields' );
	remove_post_type_support( 'post','comments' );
    remove_post_type_support( 'page','comments' );
	remove_post_type_support( 'post','author' );
    remove_post_type_support( 'page','author' );
	
}
add_action('init','baw_remove_custom_field_meta_boxes');

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/* Autoriser les fichiers SVG */
function wpc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'wpc_mime_types');

add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
 
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = 'linkjob';
 
    if ( isset( $atts[$my_attr] ) ) {
        $out[$my_attr] = $atts[$my_attr];
    }
 
    return $out;
}

function add_async_attribute($tag, $handle) {
    if ( 'my-js-handle' !== $handle )
        return $tag;
    return str_replace( ' src', ' async="async" src', $tag );
}
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


add_filter('pre_option_link_manager_enabled', '__return_true');
/*
  Plugin Name: WP Admin Custom List Table
 */
 
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
 
class TT_Example_List_Table extends WP_List_Table {
 
    function __construct() {
        global $status, $page;
 
        //Set parent defaults
        parent::__construct(array(
            'singular' => 'List Condidature', //singular name of the listed records
            'plural' => 'List Condidatures', //plural name of the listed records
            'ajax' => false        //does this table support ajax?
        ));
    }
 	function trim_text($input, $length, $ellipses = true, $strip_html = true) {
		//strip tags, if desired
		if ($strip_html) {
			$input = strip_tags($input);
		}
	  
		//no need to trim, already shorter than trim length
		if (strlen($input) <= $length) {
			return $input;
		}
	  
		//find last space within length
		$last_space = strrpos(substr($input, 0, $length), ' ');
		$trimmed_text = substr($input, 0, $last_space);
	  
		//add ellipses (...)
		if ($ellipses) {
			$trimmed_text .= '...';
		}
	  
		return $trimmed_text;
	}
    function column_default($item, $column_name) {
		$upload_dir   = wp_upload_dir();
        switch ($column_name) {
            case 'id': 
				return $item->id;
            case 'cv':
                return "<a href='".$upload_dir['baseurl'].'/nimble_uploads/'.$item->id.'/'.$item->$column_name."' target='_blank'>" . $item->$column_name."</a>";
			case 'message':
			    return $this->trim_text($item->$column_name,80);
            default:
                return $item->$column_name; //Show the whole array for troubleshooting purposes
        }
    }
 
    function get_columns() {
        return $columns = array(
            'id' => __('ID'),
            'title_job' => __('Title'),
            'nom' => __('Nom'),
            'prenom' => __('Prénom'),
			'email' => __('Email'),
			'telephone' => __('Téléphone'),
			'domaines' => __('Domaines'),
			'cv' => __('CV'),
			'message' => __('Message'),
            'link_visible' => __('Visible')
        );
    }
  
    function prepare_items() {
        global $wpdb; //This is used only if making any database queries
 
           /* -- Preparing your query -- */
        $query = "SELECT * FROM SaveContactForm7_2";
 
        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'ASC';
        $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : '';
        if (!empty($orderby) & !empty($order)) {
            $query.=' ORDER BY ' . $orderby . ' ' . $order;
        }
        //
 
        $totalitems = $wpdb->query($query);
 
        /**
         * First, lets decide how many records per page to show
         */
        $perpage = 10;
 
        //Which page is this?
        $paged = !empty($_GET["paged"]) ? $_GET["paged"] : '';
        //Page Number
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }
 
 
        //How many pages do we have in total?
        $totalpages = ceil($totalitems / $perpage);
        //adjust the query to take pagination into account
        if (!empty($paged) && !empty($perpage)) {
            $offset = ($paged - 1) * $perpage;
            $query.=' LIMIT ' . (int) $offset . ',' . (int) $perpage;
        }
 
        /* -- Register the pagination -- */
        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ));
 
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
 
        $this->_column_headers = array($columns, $hidden, $sortable);
 
        $this->items = $wpdb->get_results($query);
    }
 
}
 
function tt_add_menu_items() {
    add_menu_page('Condidatures', 'List Condidatures', 'activate_plugins', 'list_condidatures', 'render_list_condidature');
}
 
add_action('admin_menu', 'tt_add_menu_items');
 
function render_list_condidature() {
 
    //Create an instance of our package class...
    $testListTable = new TT_Example_List_Table();
    //Fetch, prepare, sort, and filter our data...
    $testListTable->prepare_items();
    ?>
    <div class="wrap">
 
        <div id="icon-users" class="icon32"><br/></div>
        <h2>List Condidatures</h2>       
 
        <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
        <form id="movies-filter" method="get">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <!-- Now we can render the completed list table -->
    <?php $testListTable->display() ?>
        </form>
 
    </div>
    <?php }
?>