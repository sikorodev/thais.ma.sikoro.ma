<?php
/**
 * Template Name: Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>
<?php
            while ( have_posts() ) : the_post();
        ?>
<section>
    <div class="container">
        
            <h1 class="h1-title"><?php the_title(); ?></h1>
            <div class="visuel">
                <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
				<img src="<?php echo $url ?>" />
            </div> 
    </div>
</section>
<?php the_content(); ?>
<?php
            endwhile;
        ?>
<?php
get_footer();
