<footer class="footer" id="footer">
      <div class="footer-wrap">
        <div class="row expanded">
          <div class="large-3 medium-3 small-12 column">
			  <?php
                wp_nav_menu( array(
                    'theme_location'  => 'footerMenu',
                    'menu_class' => 'menu-footer',
                    'container' => false,
                    'fallback_cb' => false,
                ));
                
            ?>
          </div>
          <div class="large-3 medium-3 small-12 column">
            <?php
				wp_nav_menu( array(
					'theme_location'  => 'footerMenu2',
					'menu_class' => 'menu-footer',
					'container' => false,
					'fallback_cb' => false,
				));
				
			?>
          </div>
          <div class="large-3 medium-3 small-12 column">
            <div class="contact-info">
              <div><i class="c-fsik__icon--marker"></i><span><?php echo get_option( 'adressContact' ); ?></span></div>
              <div><i class="c-fsik__icon--tel"></i><span><?php echo get_option( 'telContact' ); ?></span></div>
              <div><i class="c-fsik__icon--fax"></i><span>+<?php echo get_option( 'faxContact' ); ?></span></div>
              <div><i class="c-fsik__icon--mail"></i><span><?php echo get_option( 'formContactDestinationEmail' ); ?></span></div>
            </div>
          </div>
          <?php
			$imgteenor = get_option( 'logoteenor' );
			$imgteenorsrc = wp_get_attachment_image_src( $imgteenor, 'thumbnail' );
			$tennor_img_slug = is_array( $imgteenorsrc );
			?>
          <div class="large-3 medium-3 small-12 column">
            <div class="groupe-mention"><span>Une société de</span><a href="http://www.tenorgroup.ma/" target="_blank"><img style="margin:auto;width:70px !important;height:auto;" src="<?php echo $imgteenorsrc[0] ?>"></a></div>
          </div>
        </div>
      </div>
      <div class="btm-copyright">
        <div class="footer-wrap">
         <?php
			$imgLogo = get_option( 'imageLogo' );
			$imgLogosrc = wp_get_attachment_image_src( $imgLogo, 'full' );
			$you_have_img_slug = is_array( $imgslugsrc );
			?>
          <div class="copyright-content"><a href="<?php echo get_home_url(); ?>"><img class="preload-me" src="<?php echo $imgLogosrc[0]; ?>" width="922" height="247" sizes="922px" alt="THAÏS"></a><span><?php echo get_option( 'textCopyRight' ); ?></span></div>
        </div>
      </div>
    </footer><a class="c-action__gototop" href="" title="Haut de page"></a>
<?php wp_footer(); ?>
<script type="text/javascript" async="async" data-main="<?php echo get_template_directory_uri() ?>/assets/scripts/main.js" src="<?php echo get_template_directory_uri() ?>/assets/scripts/vendor/require.js"></script>
<?php echo get_option( 'googleAnalytique' ); ?>

</body>
</html>