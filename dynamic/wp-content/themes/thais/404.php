<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header(); ?>

<section>
	<div class="container">
		<div class="not-found-404">
			<span class="title">
				404<em>Page introuvable !</em>
			</span>
			<a href="" class="btn-default-sec js-btn-pack pink">
				<span>Retour</span>
			</a>
		</div>
	</div>
</section>

<?php
get_footer();
