<?php
/**
 * Template Name: Page Resultats de Recherche
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>


<section class="listing">
    <div class="container">
        <?php
            while ( have_posts() ) : the_post();
                pathway(); 
                searchFilter();
        ?>

                <div class="rows listing-results js-result">
                    <div class="rows">
                        <!-- Search Result -->
                        <?php
                            $queriedObject = get_queried_object();

                            // Query Arguments
                            $args = array(
                                            'post_status' => 'publish',
                                            'post_type' => 'annonce',
                                            'posts_per_page' => 9,
                                            's' => $_GET['keyword'],
                                        );

                            // QUERY
                            $query = new WP_Query( $args );

                            if ( $query->have_posts() ) {

                                // The Loop
                                while ( $query->have_posts() ) {
                                    $query->the_post();

                                    // Build Annonce Excerpt
                                    buildAnnonceExcerpt(get_the_ID(), get_the_title(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true), get_permalink(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ), 4);
                                }
                                
                                //Restore original Post Data 
                                wp_reset_postdata(); 
                            } else {
                                echo 'Aucune annonce trouvée';
                            }
                        ?>
                    </div>
                </div>
            <?php
                endwhile;
            ?>
        <div class="load-more clear"><button class="js-loadmore loadmore btn-default-sec"><span>Charger plus de produits</span></button> </div>
    </div>
</section>

<?php
get_footer();
