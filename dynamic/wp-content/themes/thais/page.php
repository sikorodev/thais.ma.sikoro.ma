<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>
<main>
 <?php
            while ( have_posts() ) : the_post();
        ?>
        <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
		<section class="heading">
        <div class="hero-content">
          <div class="hero-inner">
            <hgroup>
              <h1><?php echo the_title(); ?></h1>
            </hgroup>
            <div class="c-breadcrumb">
              <div class="row expanded">
                <div class="columns">
                  <div class="label">Vous êtes ici  :</div>
                   <?php // Breadcrumb navigation
					 if (is_page() && !is_front_page() || is_single() || is_category()) {
					 echo '<ul class="c-breadcrumb__list">';
					 echo '<li><a title="Accueil" rel="nofollow" href="'.get_home_url().'">Accueil</a></li>';
					
					 if (is_page()) {
					 $ancestors = get_post_ancestors($post);
					
					 if ($ancestors) {
					 $ancestors = array_reverse($ancestors);
					
					 foreach ($ancestors as $crumb) {
					 echo '<li><a href="'.get_permalink($crumb).'">'.get_the_title($crumb).'</a></li>';
					 }
					 }
					 }
					 if (is_single()) {
					 $category = get_the_category();
					 echo '<li><a href="'.get_category_link($category[0]->cat_ID).'">'.$category[0]->cat_name.'</a></li>';
					 }
					
					 if (is_category()) {
					 $category = get_the_category();
					 echo '<li>'.$category[0]->cat_name.'</li>';
					 }
					
					 // Current page
					 if (is_page() || is_single()) {
					 echo '<li><span>'.get_the_title().'</span></li>';
					 }
					 echo '</ul>';
					 }
					?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hero-second-illus">
          <div class="hero-bg lazy swiper-lazy" data-src="<?php echo $url; ?>" data-original="<?php echo $url; ?>" style="background:url(<?php echo $url; ?>); background-size:cover; background-position:50%;"></div>
        </div>
      </section>
      <section class="page-content">
      	<?php the_content(); ?> 	
      </section>
	
             
<?php
            endwhile;
        ?>

</main>

<?php
get_footer();
