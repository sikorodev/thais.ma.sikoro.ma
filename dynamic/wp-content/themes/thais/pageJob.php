<?php
/**
 * Template Name: Page Carriére
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>

<main>
 <?php
            while ( have_posts() ) : the_post();
        ?>
        <?php $url = get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>
		<section class="heading">
        <div class="hero-content">
          <div class="hero-inner">
            <hgroup>
              <h1><?php echo the_title(); ?></h1>
            </hgroup>
            <div class="c-breadcrumb">
              <div class="row expanded">
                <div class="columns">
                  <div class="label">Vous êtes ici  :</div>
                  <ul class="c-breadcrumb__list">
                    <li><a href="<?php echo get_home_url(); ?>">Accueil</a></li>
                    <li><?php echo the_title(); ?></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="hero-second-illus">
          <div class="hero-bg" style="background:url(<?php echo $url; ?>); background-size:cover; background-position:center 100%;"></div>
        </div>
      </section>
      <?php
            endwhile;
        ?>
      <section class="page-content">
      	<div class="row">
          <div class="large-12 medium-12 small-12 column">
            <div class="job-wrapper">
              <ul class="job-lists">
              	<?php 
					$wp_query = new WP_Query(array( 
						'post_status' => 'any',
						'posts_per_page' => -1,
						'orderby' => 'date',
						'order' => 'ASC',
						'post_type' => "Carriere",
					));
				?>
                <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                	<?php if(get_post_meta(get_the_ID(),'_carriereAddMetaboxesinfoscarriere_isActive',true ) == true){ ?>
                	<li>
                    	<div class="job-item">
                        	<article>
                            	<div class="row">
                                	<div class="large-2 medium-2 small-12 column"><span class="label hide">Titre</span>
                                      <h2><?php the_title(); ?></h2>
                                    </div>
                                    <div class="large-2 medium-2 small-12 column"><span class="label hide">Ville</span><span class="city-name"><?php echo wp_strip_all_tags(get_the_term_list( get_the_ID(), 'villes', ' ', ' , ', ' ') ); ?></span></div>
                                    <div class="large-12 medium-12 small-12 column hide js-toggle-content">
                                      <div class="toggle-content"><span class="label hide">Mission :</span>
                                        <?php the_content(); ?>
                                      </div>
                                    </div>
                                    <div class="large-4 medium-4 small-12 column"><span class="label hide">profil</span><span class="profil-desc"><?php echo get_post_meta(get_the_ID(),'_carriereAddMetaboxesinfoscarriere_profile',true ); ?></span></div>
                                    <div class="large-4 medium-4 small-12 column">
                          <div class="cta"><a class="js-toggle-post" href=""><span>Plus de détails</span><span>Moins de détails</span></a><a href="<?php echo get_permalink(); ?>">Postuler maintenant</a></div>
                        </div>
                                    
                                </div>
                            </article>
                        </div>
                    </li>
                    <?php } ?>
                <?php endwhile; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="row expanded">
          <div class="large-12 medium-12 small-12">
            <div class="condidature-wraper">
              <div class="desc">
                <p>Rejoignez notre équipe</p>
              </div>
              <div class="cta"><a href="<?php echo get_page_link(425); ?>"><span>Candidature spontanée</span></a></div>
            </div>
          </div>
        </div>	
      </section>
</main>

<?php
get_footer();
