<?php
/**
 * Template Name: Home Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>
<div id="loader-body" class="">
	<div class="load-wrap"><div class="bg-load"></div></div>
</div>
<main>
<?php
            while ( have_posts() ) : the_post();
        ?>
        <?php the_content(); ?> 	
        
<?php
            endwhile;
        ?>
</main>
<?php
get_footer();

