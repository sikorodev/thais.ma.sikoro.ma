<?php
/**
 * The category template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header(); ?>
<section class="bondeau-decline" data-show="on-scroll">
	<div class="container_slide">
    					<img src="<?php echo get_template_directory_uri() ?>/assets/img/bandeau-notre-culture.jpg">
    </div>
</section>
<section class="section wrap_cat" data-show="on-scroll">
    <div class="container">
    	<h2>NOTRE ACTUALITÉ</h2>
        <?php
            while ( have_posts() ) : the_post();
        ?>
            <div class="item">
                <div class="text">
                	<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
                    <span class="date"><?php the_time('d/m/Y') ?></span>
                    <?php the_excerpt('',FALSE,''); ?>
                </div>
            </div>
             
        <?php
            endwhile;
        ?>
    </div>
</section>
<?php
$wp_queryEnUneLigne = new WP_Query(array( 
		'post_status' => 'any',
		'posts_per_page' => 30,
		'orderby' => 'date',
		'order' => 'ASC',
		'post_type' => "en_une_ligne",
	));
?>
<section class="section en_une_ligne" data-show="on-scroll">
	<div class="container">
    	<h3>En une ligne</h3>
        <div class="counter stat-popup">
        	<ul>
            <?php $i = 0; ?>
            <?php while ($wp_queryEnUneLigne -> have_posts()) : $wp_queryEnUneLigne -> the_post();  ?>
            <?php $i++; ?>
					<li>
                    	<em id="count_0<?php echo $i; ?>"><?php echo get_post_meta( get_the_ID(), '_en_une_ligneAddMetaboxesnombreen_une_ligne_nombreEnligneM', true ); ?></em><?php echo get_post_meta( get_the_ID(), '_en_une_ligneAddMetaboxesnombreen_une_ligne_uniteEnligneM', true ); ?>
                    	<span><?php the_title(); ?></span>
                    </li>

     		<?php endwhile;?>
            </ul>
        </div>
    </div>
</section>
<section class="section cibleP" data-show="on-scroll">
<div class="container">
<div class="grid">
<div class="grid-50">
<div class="text">
<h2>Votre cible est la notre</h2>
<p>Particuliers, Professions libérales, Tpe, PME PMI, Collectivités locales & Grand compte, vous vous apportons notre expertise.</p>
</div>
</div>
<div class="grid-50">
<img src="http://localhost/iso_sell_groupe/wp-content/uploads/2017/03/visu-cible.jpg" alt=""  />
</div>
</div>
</div>
</section>
<?php
get_footer();