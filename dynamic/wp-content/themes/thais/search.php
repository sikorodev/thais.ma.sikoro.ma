<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package thais
 */

get_header(); ?>
<section class="search">
    <div class="container">
    	<?php if ( have_posts() ) : ?>
       	 <h1 class="h1-title">Résultats de votre recherche</h1>
        <?php
            while ( have_posts() ) : the_post();
        ?>
            
            <div class="search-field">
              <h3><a href="<?php echo the_permalink(); ?>" title=""><?php the_title(); ?></a></h3>
              <?php the_excerpt('',FALSE,''); ?>
              <p class="link-more"><a href="<?php echo the_permalink(); ?>" title=""><?php echo the_permalink(); ?></a></p>
            </div>
        <?php
            endwhile;
			else :
			?>
            <h1 class="h1-title">Aucune Résultat Trouvé</h1>
			<?php
			endif;
        ?>
    </div>
</section>

<?php
get_footer();
