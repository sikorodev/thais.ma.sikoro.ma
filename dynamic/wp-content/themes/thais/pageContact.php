<?php
/**
 * Template Name: Page Contact
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package thais
 */

get_header();
?>

<section class="forms">
    <div class="container">
        <?php
            while ( have_posts() ) : the_post();
        ?>
            <div class="rows">
                <div class="form-def">
                    <div class="form-conainter">
                        <h2><?php the_title(); ?></h2>
                        <div class="form-content js-form-validate">
                            <form class="contact-form js-sendMail" name="contact-form" id="contact-form" action="" method="post">
                                <?php wp_nonce_field( 'sendMailAjax', 'sendMailAjax_nonce') ?>
                                <input type="hidden" name="msgSubject" value="Contact Message">
                                <fieldset>
                                    <div class="field">
                                        <input type="text" name="nom" id="nom" placeholder="Nom*" class="required">
                                    </div>
                                    <div class="field">
                                        <input type="text" name="prenom" id="prenom" placeholder="Prénom*" class="required">
                                    </div>
                                    <div class="field">
                                        <input type="email" name="mail" id="mail" placeholder="Adresse mail*" class="required email">
                                    </div>
                                    <div class="field">
                                        <input type="text" name="tel" id="tel" placeholder="Téléphone*" class="required tel">
                                    </div>
                                    <div class="field">
                                        <select name="service" id="service" class="required">
                                            <option value="Service 1" selected>Service 1</option>
                                            <option value="Service 2">Service 2</option>
                                            <option value="Service 3">Service 3</option>
                                            <option value="Service 4">Service 4</option>
                                            <option value="Service 5">Service 5</option>
                                        </select>
                                    </div>
                                    <div class="field">
                                        <textarea type="text" name="message" id="message" placeholder="" class="required defaultInvalid" placeholder="Message">MESSAGE</textarea>
                                    </div>
                                    <div class="field cta">
                                        <span class="txt-required">* Champs obligatoires</span>
                                        <button class="btn-default-sec -contact">
                                            <span>Envoyer</span>
                                        </button>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            endwhile;
        ?>
    </div>

</section>

<?php
get_footer();
