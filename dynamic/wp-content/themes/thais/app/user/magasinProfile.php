<?php

use framework\userFieldsBuilder as fieldBuilder;


add_action( 'admin_menu', 'magasinProfileAdminMenu' );

function magasinProfileAdminMenu() {
	add_menu_page( 'Magasin', 'Magasin', 'edit_others_pending_posts', 'magasinProfile', 'magasinProfile', 'dashicons-store', 6  );
}


function magasinProfileSettings(){
    $settings = array(
        'config' => array(
            'pageTitle'=>'Magasin',
        ),
        'sections' => array(
            array(
                'title' => '',
                'fields' => array(
                    array(
                        'type' => 'text',
                        'name' => 'companyName',
                        'value' => 'getIt',
                        'placeholder' => 'Nom de l\'Entreprise',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'image',
                        'name' => 'companyImage1',
                        'value' => 'getIt',
                        'placeholder' => 'Image de l\'Entreprise 1',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'image',
                        'name' => 'companyImage2',
                        'value' => 'getIt',
                        'placeholder' => 'Image de l\'Entreprise 2',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyType',
                        'value' => 'getIt',
                        'placeholder' => 'Type de l\'Entreprise',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyContactPerson',
                        'value' => 'getIt',
                        'placeholder' => 'Personne de Contact',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyContactEmail',
                        'value' => 'getIt',
                        'placeholder' => 'E-mail de Contact',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companySiteWeb',
                        'value' => 'getIt',
                        'placeholder' => 'Site Web',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyPhoneIndicatif',
                        'value' => 'getIt',
                        'placeholder' => 'Indicatif Telephone de l\'Entreprise',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyPhone',
                        'value' => 'getIt',
                        'placeholder' => 'Telephone de l\'Entreprise',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyAdressMagasin',
                        'value' => 'getIt',
                        'placeholder' => 'Adresse du Magasin',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyZipMagasin',
                        'value' => 'getIt',
                        'placeholder' => 'Code Postal du Magasin',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyVilleMagasin',
                        'value' => 'getIt',
                        'placeholder' => 'Ville du Magasin',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyPaysMagasin',
                        'value' => 'getIt',
                        'placeholder' => 'Pays du Magasin',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'checkbox',
                        'name' => 'companyAdressesPareille',
                        'value' => 'getIt',
                        'placeholder' => 'Adresse du siege et pareille que du magasin',
                        'class' => 'regular-text',
                    ),

                    array(
                        'type' => 'text',
                        'name' => 'companyAdressSiege',
                        'value' => 'getIt',
                        'placeholder' => 'Adresse du Siege',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyZipSiege',
                        'value' => 'getIt',
                        'placeholder' => 'Code Postal du Siege',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyVilleSiege',
                        'value' => 'getIt',
                        'placeholder' => 'Ville du Siege',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyPaysSiege',
                        'value' => 'getIt',
                        'placeholder' => 'Pays du Siege',
                        'class' => 'regular-text',
                    ),

                    array(
                        'type' => 'text',
                        'name' => 'companyNumRC',
                        'value' => 'getIt',
                        'placeholder' => 'Numero RC de l\'Entreprise',
                        'class' => 'regular-text',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'companyNumTVA',
                        'value' => 'getIt',
                        'placeholder' => 'Numero TVA de l\'Entreprise',
                        'class' => 'regular-text',
                    )
                ),
            ),
        ),
    );
    
    return $settings;
}


function magasinProfile() {

    $magasinProfileSettings = magasinProfileSettings();

    // Load fieldBuilder Class
    $fieldBuilder = new fieldBuilder;

    // Build Metabox
    $fieldBuilder->fieldsBuilder($magasinProfileSettings);
}