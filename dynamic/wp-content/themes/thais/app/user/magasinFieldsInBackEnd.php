<?php
if(current_user_can('editor') || current_user_can('administrator'))
{
	$magasinFields = array(
                        'Nom de l\'Entreprise'=>'companyName',
                        'Type de l\'Entreprise'=>'companyType',
                        'Personne de Contact'=>'companyContactPerson',
                        'Telephone de l\'Entreprise'=>'companyPhone',
                        'Adresse du Magasin'=>'companyAdressMagasin',
                        'Adresse du Siege'=>'companyAdressSiege',
                        'Numero RC de l\'Entreprise'=>'companyNumRC',
                        'Numero TVA de l\'Entreprise'=>'companyNumTVA',
						'Valide'=>'isValide',
                  );
}
else
{
	$magasinFields = array(
                        'Nom de l\'Entreprise'=>'companyName',
                        'Type de l\'Entreprise'=>'companyType',
                        'Personne de Contact'=>'companyContactPerson',
                        'Telephone de l\'Entreprise'=>'companyPhone',
                        'Adresse du Magasin'=>'companyAdressMagasin',
                        'Adresse du Siege'=>'companyAdressSiege',
                        'Numero RC de l\'Entreprise'=>'companyNumRC',
                        'Numero TVA de l\'Entreprise'=>'companyNumTVA',
                  );
}

add_action( 'show_user_profile', 'magasinFieldsInBackend' );
add_action( 'edit_user_profile', 'magasinFieldsInBackend' );

function magasinFieldsInBackend( $user ) {

  if(in_array("magasin", $user->roles)){
?>
    <h3>Magasin</h3>
    <table class="form-table">
    <?php
      GLOBAL $magasinFields;
      foreach ($magasinFields as $key => $value) {
        echo '<tr>';
          echo '<th><label for="'.$key.'">'.$key.'</label></th>';
          echo '<td>';
		  	if(current_user_can('editor') || current_user_can('administrator'))
			{
				if($value == "isValide") {
					$checkd ="";
					if(get_the_author_meta( $value, $user->ID ) == 1) $checkd = 'checked="checked"';
					echo '<input type="checkbox"  id="isValide" '.$checkd.' value="'.get_the_author_meta( $value, $user->ID ).'" /><br />';
					echo '<input type="hidden" name="'.$value.'" id="'.$key.'" class="regular-text js-is-valide" value="'.get_the_author_meta( $value, $user->ID ).'" /><br />';
				}
				else echo '<input type="text" name="'.$value.'" id="'.$key.'" class="regular-text" value="'.get_the_author_meta( $value, $user->ID ).'" /><br />';
			}
			else{
				echo '<input type="text" name="'.$value.'" id="'.$key.'" class="regular-text" value="'.get_the_author_meta( $value, $user->ID ).'" /><br />';
			}
		  	
        echo '</td>';
        echo '</tr>';
      }
    ?>

    </table>
<?php
  }
}

add_action( 'personal_options_update', 'magasinFieldsInBackendSave' );
add_action( 'edit_user_profile_update', 'magasinFieldsInBackendSave' );
function magasinFieldsInBackendSave( $user_id ) {
  GLOBAL $magasinFields;

  if ( current_user_can( 'edit_user', $user_id ) ) {
    foreach ($magasinFields as $key => $value) {
      update_user_meta( $user_id, $value, $_POST[$value] );
      $saved = true;
    }
  }

}