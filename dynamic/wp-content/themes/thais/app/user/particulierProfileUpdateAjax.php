<?php

add_action( 'wp_ajax_particulierProfileUpdate', 'particulierProfileUpdate' );

function particulierProfileUpdate(){

	// Check Nonce
	check_ajax_referer( 'particulierProfile', 'nonce' );

	global $wpdb; // this is how you get access to the database

	// Update User Metas
	wp_update_user(	array(
						'ID' => get_current_user_id(),
						'user_email' => $_POST['email'],
						'first_name' => $_POST['firstName'],
						'last_name'  => $_POST['lastName'],
						)
					);

	// Response 
	echo json_encode(array('status'=>true, 'message'=>'Votre profil a été mis à jour avec succès.'), true);

	wp_die(); // this is required to terminate immediately and return a proper response
}

