<?php

add_action( 'wp_ajax_nopriv_loginOAuth', 'loginOAuth' );

function updateUserMeta_sendMail_signOn_ajaxResponse_Oauth($userInfos, $redirectTo){
	// Update User Meta
	wp_update_user(	array(
							'ID' => $userInfos['id'],
							'first_name' => $userInfos['fname'],
							'last_name' => $userInfos['lname'],
							'role' => $userInfos['role'],
							'show_admin_bar_front' => false,
						));

	// Add oAuth User Meta
	add_user_meta( $userInfos['id'], 'oAuthId', 'itesiko_'.$userInfos['idApp'], true );
	add_user_meta( $userInfos['id'], 'oAuthEmail', $userInfos['email'], true );

	// Send Mail
	$headers[] = 'From: thais <noreply@thais.com>';
	$headers[] = 'Content-Type: text/html; charset=UTF-8\r\n';
	$mailContent .= 'Bonjour '.$userInfos['fname'].' '.$userInfos['lname'].',<br/><br/>';
	$mailContent .= 'Nous vous confirmons la création de votre compte sur le site thais.com<br/><br/>';
	$mailContent .= 'Toute l\'équipe d\'thais vous souhaite la bienvenue !';
	$mail = wp_mail( $userInfos['email'], 'Bienvenue sur thais !', $mailContent, $headers);

	// Signon User
	wp_set_auth_cookie( $userInfos['id'], true);

	// Ajax Response
	if($userInfos['role'] == 'magasin'){
		$result = redirectLoggedInUserAjax(array('magasin'), $redirectTo);
	} else {
		$result = redirectLoggedInUserAjax(array('client'), $redirectTo);
	}
	
	echo json_encode($result, true);
}

function loginOAuth(){
	//Security Check
	validateAjaxDataNotNull(array('role','idApp','fname','lname','email'));

	global $wpdb; // this is how you get access to the database

	// Set Variable
	$result=array();


	// Verify Identity : Get User By MetaData
	$user = get_users(array(
							'meta_query'  => array(
								'relation' => 'AND',
								array(
									'key'     => 'oAuthId',
									'value'   => 'itesiko_'.$_POST['idApp'],
									'compare' => '='
								),/* For Google Change Email
								array( 
									'key'     => 'oAuthEmail',
									'value'   => $_POST['email'],
									'compare' => '='
								)*/
							)
						));

	// Check if Redirect Exist
	$referrer = parse_url($_SERVER['HTTP_REFERER']);
	parse_str($referrer['query'], $referrerQuery);
	$redirectTo = $referrerQuery['redirect_to'];

	// If User Not Exist Register It
	if (empty($user)) {
		// Security : Check User Role (Public Information)
		if($_POST['role'] != 'magasin' && $_POST['role'] != 'client'){
			echo json_encode(array('status'=>false, 'message'=>'don\'t try :P') ,true);
			wp_die();
		}

		// Generate Password for New User
		$_POST['pass'] = mt_rand(999,99999999);
		$userInfos = $_POST;

		// Register User
		$user = wp_create_user( $_POST['fname'].'.'.$_POST['lname'], $_POST['pass'], $_POST['email'] );

		// If User Create Error
		if ( is_wp_error($user) ){
			// Get Error
			$errorMessage = array_keys($user->errors);

			// If Existing Username, Add number to username
			if($errorMessage[0] == 'existing_user_login'){
				$user = registerUserWithGeneratedUsername($userInfos);

				if(is_wp_error($user)){
					$errorMessage = array_keys($user->errors);
					$result = array('status'=>false, 'message'=>$errorMessage[0]);
				} else {
					// Update User Meta & Send Mail & Success Response
					$userInfos['id'] = $user;
					updateUserMeta_sendMail_signOn_ajaxResponse_Oauth($userInfos);
				}
			} else{
				$result = array('status'=>false, 'message'=>$errorMessage[0]);
			}

		} else {
			// Update User Meta & Send Mail & Success Response
			$userInfos['id'] = $user;
			updateUserMeta_sendMail_signOn_ajaxResponse_Oauth($userInfos);
		}
	} else {
		// Signon User
		wp_set_auth_cookie( $user[0]->ID, true);

		// Send Response
		echo json_encode(redirectLoggedInUserAjax($user[0]->roles, $redirectTo), true);
	}

	wp_die(); // this is required to terminate immediately and return a proper response
}