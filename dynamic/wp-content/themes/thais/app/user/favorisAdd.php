<?php

add_action( 'wp_ajax_addUserProductFavoris', 'addUserProductFavoris' );
add_action( 'wp_ajax_nopriv_addUserProductFavoris', 'addUserProductFavoris' );


function addUserProductFavoris(){

	global $wpdb; // this is how you get access to the database

	// If User Is Logged
	if(is_user_logged_in()){


		// Get Variables
		$userId = get_current_user_id();
		$productId = array($_POST['productId']);
		$userFavoris = get_user_meta($userId, 'favorisProducts', true);

		// Check if Favoris Products Exist
		if($userFavoris){
			// Decode Json
			$userFavoris = json_decode($userFavoris, true);

			// Check if Product already Exist
			if (in_array($_POST['productId'], $userFavoris)){
				// If exist return Message
				$result = array('status'=>false, 'message'=>'Le produit que vous voulez ajouté existe déjà dans votre favoris.');
			} else {
				// If Not Exist Added it and return Message
				array_push($userFavoris, $_POST['productId']);
				update_user_meta($userId, 'favorisProducts', json_encode($userFavoris));
				$result = array('status'=>true, 'message'=>'Produit ajouté à vos favoris avec succès.');
			}
		} else{
			// Add First Product to Favoris
			update_user_meta($userId, 'favorisProducts', json_encode($productId));
			$result = array('status'=>true, 'message'=>'Produit ajouté à vos favoris avec succès.');
		}
   
	}else{
		// Redirect To Login Page
		$loginUrl = add_query_arg( 'redirect_to', $_SERVER['HTTP_REFERER'], home_url('se-connecter') );
		$result = array('status'=>false,'message'=>'Vous devez être inscrit et connecté pour ajouter ce produit à vos favoris.', 'redirect'=>$loginUrl);
	}

	// Response 
	echo json_encode($result, true);

	wp_die(); // this is required to terminate immediately and return a proper response
}

