<?php

/**
 * Redirect the user to the custom login page instead of wp-login.php.
 */

add_action( 'login_form_login', 'redirectToCustomLogin' );

function redirectToCustomLogin($redirectTo=NULL) {

	if(empty($redirectTo)){
		$redirectTo = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
	}

	if ( is_user_logged_in() ) {
		redirectLoggedInUser( $redirectTo );
		exit;
	}

	// The rest are redirected to the login page
	$login_url = home_url( 'se-connecter' );
	
	if ( ! empty( $redirectTo ) ) {
		$login_url = add_query_arg( 'redirect_to', $redirectTo, $login_url );
	}

	wp_redirect( $login_url );
	exit;
}

function redirectLoggedInUserAjax( $userRole, $redirectTo) {
	if($redirectTo){
		$result = array('status'=>true, 'redirect'=>$redirectTo);
	} else if (in_array("administrator", $userRole)) {
		$result = array('status'=>true, 'redirect'=>admin_url('/'));
	}else if(in_array("magasin", $userRole)){
		$login_page = home_url( '/?page_id=607/' );
		//$result = array('status'=>true, 'redirect'=>admin_url('/admin.php?page=magasinProfile'));
		$result = array('status'=>true, 'redirect'=>$login_page);
	} else {
		$result = array('status'=>true, 'redirect'=>home_url( 'profile' ));
	}
	return $result;
}

function redirectLoggedInUser($redirectTo=NULL) {

	if($redirectTo){
		wp_redirect( $redirectTo );
		exit;
	} else{
    	$userRole = wp_get_current_user();
	    $userRole = $userRole->roles;

		if(in_array("administrator", $userRole)){
			wp_redirect( admin_url('/') );
			exit;
		} else if(in_array("magasin", $userRole)) {
			$login_page = home_url( '/?page_id=607/' );
			//wp_redirect( admin_url('/admin.php?page=magasinProfile') );
			wp_redirect( $login_page);
			exit;
		} else {
			wp_redirect( home_url( 'profile' ));
			exit;
		}
	}
}

function validateAjaxDataNotNull($fields){
	foreach ($fields as $field) {
		if(!$_POST[$field]){
			echo json_encode($result = array('status'=>false, 'message'=>'Form Error, Empty Field, Try again'));
			wp_die();
		}
	}
}