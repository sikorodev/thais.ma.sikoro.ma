<?php

add_action( 'wp_ajax_magasinProfileUpdate', 'magasinProfileUpdate' );

function magasinProfileUpdate(){

	// Check Nonce
	check_ajax_referer( 'userProfile', 'nonce' );


	// User Metas
	unset($_POST['action']);
	unset($_POST['nonce']);

	global $wpdb; // this is how you get access to the database

	// Update User Metas
	foreach ($_POST as $key => $value) {
		update_user_meta(get_current_user_id(), $key, $value);
	}

	// Response 
	echo json_encode(array('status'=>true, 'message'=>'Changes Updated'), true);

	wp_die(); // this is required to terminate immediately and return a proper response
}

