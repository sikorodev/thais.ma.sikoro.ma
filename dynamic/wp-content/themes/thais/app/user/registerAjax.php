<?php

function registerUserWithGeneratedUsername($userInfos){
	
	$user = wp_create_user( $userInfos['fname'].'.'.$userInfos['lname'].'.'.mt_rand(1,99), $userInfos['pass'], $userInfos['email'] );

	if(is_wp_error($user)){
		$errorMessage = array_keys($user->errors);
		if($errorMessage[0] == 'existing_user_login'){
			registerUserWithGeneratedUSername($userInfos);
		} else{
			return $user;
		}
	} else {
		return $user;
	}
}


function updateUserMeta_sendMail_signOn_ajaxResponse($userInfos){
	// Update Uer Meta
	wp_update_user(	array(
							'ID' => $userInfos['id'],
							'first_name' => $userInfos['fname'],
							'last_name' => $userInfos['lname'],
							'role' => $userInfos['role'],
						));

	// Send Mail
	$headers[] = 'From: thais <noreply@thais.com>';
	$headers[] = 'Content-Type: text/html; charset=UTF-8\r\n';
	$mailContent .= 'Bonjour '.$userInfos['fname'].' '.$userInfos['lname'].',<br/><br/>';
	$mailContent .= 'Nous vous confirmons la création de votre compte sur le site thais.com<br/><br/>';
	$mailContent .= 'Toute l\'équipe d\'thais vous souhaite la bienvenue !';
	$mail = wp_mail( $userInfos['email'], 'Bienvenue sur thais !', $mailContent, $headers);

	// Signon User
	wp_set_auth_cookie( $userInfos['id'], true);

	// Ajax Response
	if($userInfos['role'] == 'magasin'){
		// Add Magasin User Meta
		$userMeta = array(	'companyName'=>$userInfos['companyName'],
							'companyType'=>$userInfos['companyType'],
							'companyContactPerson'=>$userInfos['companyContactPerson'],
							'companyPhone'=>$userInfos['companyPhone'],
							'companyAdressMagasin'=>$userInfos['companyAdressMagasin'],
							'companyAdressSiege'=>$userInfos['companyAdressSiege'],
							'companyNumRC'=>$userInfos['companyNumRC'],
							'companyNumTVA'=>$userInfos['companyNumTVA'],
						);
		foreach ($userMeta as $key => $value) {
			add_user_meta( $userInfos['id'], $key, $value, true );
		}
		
		$result = redirectLoggedInUserAjax(array('magasin'));
	} else {
		$result = redirectLoggedInUserAjax(array('client'));
	}
	
	echo json_encode($result, true);
}



add_action( 'wp_ajax_nopriv_registerCustom', 'registerCustom' );

function registerCustom(){
	//Security Check
	validateAjaxDataNotNull(array('fname','lname','email','pass','role', 'nonce'));

	// Security : Check User Role (Public Information)
	if($_POST['role'] != 'magasin' && $_POST['role'] != 'client'){
		echo json_encode(array('status'=>false, 'message'=>'don\'t try :P') ,true);
		wp_die();
	}

	// Check Nonce
	check_ajax_referer( 'registerAjax', 'nonce' );

	// User Infos
	$userInfos = array(	'fname'    => $_POST['fname'],
						'lname'	   => $_POST['lname'],
						'email'    => $_POST['email'],
						'password' => $_POST['pass'],
						'role'	   => $_POST['role'],
						'companyName'  => $_POST['companyName'],
						'companyType'  => $_POST['companyType'],
						'companyContactPerson'  => $_POST['companyContactPerson'],
						'companyPhone'  => $_POST['companyPhone'],
						'companyAdressMagasin'  => $_POST['companyAdressMagasin'],
						'companyAdressSiege'  => $_POST['companyAdressSiege'],
						'companyNumRC'  => $_POST['companyNumRC'],
						'companyNumTVA'  => $_POST['companyNumTVA'],
					);

	global $wpdb; // this is how you get access to the database

	// Create User
	$user = wp_create_user( $userInfos['fname'].'.'.$userInfos['lname'], $userInfos['password'], $userInfos['email'] );
	
	// If User Create Error
	if ( is_wp_error($user) ){
		// Get Error
		$errorMessage = array_keys($user->errors);
		// If Existing Username, Add number to username
		if($errorMessage[0] == 'existing_user_login'){
			$user = registerUserWithGeneratedUsername($userInfos);

			if(is_wp_error($user)){
				$errorMessage = array_keys($user->errors);
				$result = array('status'=>false, 'message'=>$errorMessage[0]);
			} else {
				// Update User Meta & Send Mail & Success Response
				$userInfos['id'] = $user;
				updateUserMeta_sendMail_signOn_ajaxResponse($userInfos);
			}
		} else{
			$result = array('status'=>false, 'message'=>$errorMessage[0]);
		}

	} else {
		// Update User Meta & Send Mail & Success Response
		$userInfos['id'] = $user;
		updateUserMeta_sendMail_signOn_ajaxResponse($userInfos);
	}

	echo json_encode($result ,true);

	wp_die(); // this is required to terminate immediately and return a proper response
}

