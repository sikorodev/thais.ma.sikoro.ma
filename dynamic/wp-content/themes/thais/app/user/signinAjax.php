<?php

add_action( 'wp_ajax_nopriv_signinCustom', 'signinCustom' );

function signinCustom(){
	//Security Check
	validateAjaxDataNotNull(array('username','password','nonce'));

	// Check Nonce
	check_ajax_referer( 'loginAjax', 'nonce' );

	global $wpdb; // this is how you get access to the database

	// Set Variable
	$result=array();

	// Check if Redirect Exist
	$referrer = parse_url($_SERVER['HTTP_REFERER']);
	parse_str($referrer['query'], $referrerQuery);
	$redirectTo = $referrerQuery['redirect_to'];

	// Sign In
	$creds = array(
					'user_login'=>$_POST['username'],
					'user_password'=>$_POST['password'],
					'remember'=>true,
				);
	$user = wp_signon( $creds, false );

	// Send Result
	if ( is_wp_error($user) ){
		$result = array('status'=>false, 'message'=>'Votre nom d\'utilisateur et votre mot de passe ne correspondent pas');
	} else {
		$result = redirectLoggedInUserAjax($user->roles, $redirectTo);
	}

	echo json_encode($result, true);

	wp_die(); // this is required to terminate immediately and return a proper response
}