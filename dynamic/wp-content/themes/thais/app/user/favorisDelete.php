<?php

add_action( 'wp_ajax_deleteUserProductFavoris', 'deleteUserProductFavoris' );

function deleteUserProductFavoris(){

	global $wpdb; // this is how you get access to the database

	// Get Variables
	$userId = get_current_user_id();
	$productId = array($_POST['productId']);
	$userFavoris = get_user_meta($userId, 'favorisProducts', true);

	// Decode Json
	$userFavoris = json_decode($userFavoris, true);

	// Delete Items from Favofis List
	$userFavorisNew = array_diff($userFavoris, $_POST['favorisIds']);

	// Update Database
	update_user_meta($userId, 'favorisProducts', json_encode($userFavorisNew));

	$result = array('status'=>true, 'message'=>'Le Produit a été supprimé avec succès de votre list de favoris');

	// Response 
	echo json_encode($result, true);

	wp_die(); // this is required to terminate immediately and return a proper response
}

