<?php

add_action( 'wp_ajax_clearUserProductFavoris', 'clearUserProductFavoris' );

function clearUserProductFavoris(){

	// Check Nonce
	check_ajax_referer( 'userFavoris', 'nonce' );

	global $wpdb; // this is how you get access to the database

	// Get Variables
	$userId = get_current_user_id();
	$productId = array($_POST['productId']);
	$userFavoris = get_user_meta($userId, 'favorisProducts', true);

	// Clear Product to Favoris
	delete_user_meta($userId, 'favorisProducts');
	$result = array('status'=>true, 'message'=>'Favoris List Cleared');

	// Response 
	echo json_encode($result, true);

	wp_die(); // this is required to terminate immediately and return a proper response
}

