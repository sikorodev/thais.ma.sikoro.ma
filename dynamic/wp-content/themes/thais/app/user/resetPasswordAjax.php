<?php

add_action( 'wp_ajax_nopriv_resetPasswordCustom', 'resetPasswordCustom' );

function resetPasswordCustom(){
	//Security Check
	validateAjaxDataNotNull(array('email','nonce'));

	// Check Nonce
	check_ajax_referer( 'resetPasswordAjax', 'nonce' );

	global $wpdb; // this is how you get access to the database

	// Declare Result Array
	$result = array();
	
	// Get user by eMail
	$user = get_user_by_email($_POST['email']);

	// If user Exist Do Statement
	if(!$user){
		$result = array('status'=>false, 'message'=>'l\'email entré ne correspond à aucun compte');
	} else {
		// Send Mail to User With New Password
		$passwordNew = mt_rand(999,99999999); // Generate New Password

		$headers[] = 'From: thais <noreply@thais.com>';
		$headers[] = 'Content-Type: text/html; charset=UTF-8\r\n';

		$mailContent .= 'Bonjour '.get_user_meta($user->ID, 'first_name', true).' '.get_user_meta($user->ID, 'last_name', true).',<br/><br/>';
		$mailContent .= 'Vous venez de demander un nouveau mot de passe pour accéder à votre compte sur thais.com :<br/><br/>';
		$mailContent .= 'Nouveau mot de passe : '.$passwordNew.'<br/><br/>';
		$mailContent .= 'L\'équipe thais.com';
		
		$mail = wp_mail( $user->data->user_email, 'Votre nouveau mot de passe thais', $mailContent, $headers); // Send Mail
		
		// If mail is Sent Change User Password
		if($mail){
			wp_set_password( $passwordNew, $user->ID);
			
			$result = array('status'=>true, 'message'=>'Un mail vous a été envoyé avec votre mot de passe');
		} else {
			$result = array('status'=>false, 'message'=>'Nous n\'avons pas pu modifier votre compte, réessayer ultérieurement!');
		}	
	}

	// Send Result
	echo json_encode($result, true);

	wp_die(); // this is required to terminate immediately and return a proper response
}