<?php

function buildAnnonceExcerpt($id, $title, $description, $url, $priceBase, $priceBrade, $img, $grid=3, $class=NULL ){

	// Get Variables
	$img = json_decode($img, true);
	$img = wp_get_attachment_image_src($img[0][0]['value'], 'annonceS' );
	$title = get_limitStringChars($title, 20);
	$description = get_limitStringChars($description, 55);

	$remiseType = get_post_meta($id,'_annonceAddMetaboxesPrix_prixRemiseType',true);
	if($remiseType){
		if($remiseType == 'percent'){
			$priceNew = number_format((float)(int)$priceBase - ($priceBase * get_post_meta($id,'_annonceAddMetaboxesPrix_prixpercent',true) / 100), 2, '.', '');
		}else if($remiseType == 'price'){
			$priceNew = number_format((float)$priceBrade, 2, '.', '');
		}
	}

	$isFavoris = '';

	// Check if Logged User (Favoris)
	if(is_user_logged_in()){
		//Get User Favoris
		$userId = get_current_user_id();
		$userFavoris = get_user_meta($userId, 'favorisProducts', true);

		// Check is user have Favoris
		if($userFavoris){
			// Decode Json
			$userFavoris = json_decode($userFavoris, true);

			// Check if This Product exist in user Favoris List
			if (in_array($id, $userFavoris)){
				$isFavoris = 'inFavoris';
			}
		}

	}

	// Build
	echo '<div class="gr-'.$grid.' gr-12@mobile">';
		echo '<div class="bloc-offer js-fav js-cta-href '.$isFavoris.' '.$class.'"  id="'.$id.'">';
	        //echo '<a href="'.$url.'">';
	        echo '<a href="#">';
	            echo '<div class="owl-carousel owl-theme">';
	                echo '<div class="item"><img src="'.$img[0].'" alt=""></div>';
	            echo '</div>';
	        echo '</a>';

	        if($remiseType && $remiseType!=='none'){
	        	echo '<span class="priceOld">'.$priceBase.' €</span>';
	        	echo '<span class="price">'.$priceNew.' €</span>';
	    	} else {
	        	echo '<span class="price">'.$priceBase.' €</span>';
	    	}

	        echo '<h2><a href="">'.$title.'</a></h2>';
	        echo '<p>'.$description.'</p>';
	        echo '<div class="cta-offer"><div><a href="'.$url.'" class="cta-more"><span>Détails</span></a></div>';
	        
	        echo '<a href="#" class="cta-add-favoris '.$isFavoris.'"><i class="fa fa-heart-o"></i></a>';

	        echo '</div>';
	    echo '</div>';
	echo '</div>';
}

function buildAnnonceHome($id, $title, $url, $priceBase, $priceBrade, $imgs ){

	// Get Variables
	$imgs = json_decode($imgs, true);
	$imgsUrls = array();
	if($imgs){
		foreach ($imgs as $img) {
			$imgsUrls[] = wp_get_attachment_image_src($img[0]['value'], 'annonceM' );
		}
	}
	$title = get_limitStringChars($title, 22);
	
	$remiseType = get_post_meta($id,'_annonceAddMetaboxesPrix_prixRemiseType',true);
	if($remiseType){
		if($remiseType == 'percent'){
			$priceNew = number_format((float)(int)$priceBase - ($priceBase * get_post_meta($id,'_annonceAddMetaboxesPrix_prixpercent',true) / 100), 2, '.', '');
		}else if($remiseType == 'price'){
			$priceNew = number_format((float)$priceBrade, 2, '.', '');
		}
	}



	// Build
	echo '<div class="gr-4 gr-12@mobile">';
        echo '<div class="bloc-offer mea highlight-lightgray">';
            echo '<div class="owl-carousel owl-theme">';
            	$imgsUrlsCount = count($imgsUrls) - 1;
				$ctr = 0;
            	for ($imgsUrlsCount; $imgsUrlsCount >= 0 ; $imgsUrlsCount--) {
            		if($ctr < 3){
	            		echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[$imgsUrlsCount][0].'" alt=""></a></div>';
	            	}
	            	$ctr++;
            	}
            echo '</div>';
            echo '<h2><a href="'.$url.'">'.$title.'</a></h2>';
           	if($remiseType && $remiseType!=='none'){
	        	echo '<span class="priceOld">'.$priceBase.' €</span>';
	        	echo '<span class="price">'.$priceNew.' €</span>';
	    	} else {
	        	echo '<span class="price">'.$priceBase.' €</span>';
	    	}
        echo '</div>';
    echo '</div>';

}
function buildAnnonceHome_v2($latestPost,$firstIndex,$lastIndex,$itemCount){

	// Get Variables
	//$latestAnnonces[$i]['id'], $latestAnnonces[$i]['title'], $latestAnnonces[$i]['link'], $latestAnnonces[$i]['prixBase'], $latestAnnonces[$i]['prixBrade'], $latestAnnonces[$i]['photos']
	$j=0;
	$i=0;
	foreach($latestPost as $latest)
	{
		
		$title = $latest["title"];
		$imgs = $latest["photos"];
		$id = $latest["id"];
		$priceBase = $latest["prixBase"];
		$priceBrade = $latest["prixBrade"];
		$url = $latest["link"];
		$imgs = json_decode($imgs, true);
		$imgsUrls = array();
		if($imgs){
			foreach ($imgs as $img) {
				$imgsUrls[] = wp_get_attachment_image_src($img[0]['value'], 'annonceM' );
			}
		}
		$title = get_limitStringChars($title, 22);
	
		$remiseType = get_post_meta($id,'_annonceAddMetaboxesPrix_prixRemiseType',true);
		if($remiseType){
			if($remiseType == 'percent'){
				$priceNew = number_format((float)(int)$priceBase - ($priceBase * get_post_meta($id,'_annonceAddMetaboxesPrix_prixpercent',true) / 100), 2, '.', '');
			}else if($remiseType == 'price'){
				$priceNew = number_format((float)$priceBrade, 2, '.', '');
			}
		}
		// Build
		if($itemCount > 1)
		 {
			 
			if(count($latestPost) > 6 && ($i+1) <= $lastIndex)
				{
					$j++;
					if($j == 1)
					{
						echo '<div class="gr-4 gr-12@mobile">';
						echo '<div class="bloc-offer mea highlight-lightgray">';
						echo '<div class="owl-carousel owl-theme" data-auto="true">';
					}
					if($j < 4)
					
					{
						echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[0][0].'" alt=""></a>';
						echo '<div class="text-item"><h2><a href="'.$url.'">'.$title.'</a></h2>';
						if($remiseType && $remiseType!=='none'){
							echo '<span class="priceOld">'.$priceBase.' €</span>';
							echo '<span class="price">'.$priceNew.' €</span>';
						} else {
							echo '<span class="price">'.$priceBase.' €</span>';
						}
						echo '</div></div>';
						
						if($j == 3 || ($i+1) == $lastIndex) {
							echo '</div>';
							echo '</div>';
							echo '</div>';
							$j = 0;
						}
					}
				}
				elseif(count($latestPost) > 4 && count($latestPost) <= 6)
				{
					$j++;
					if($j == 1)
					{
						echo '<div class="gr-4 gr-12@mobile">';
						echo '<div class="bloc-offer mea highlight-lightgray">';
						echo '<div class="owl-carousel owl-theme" data-auto="true">';
					}
					if($j < 3)
					
					{
						echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[0][0].'" alt=""></a>';
						echo '<div class="text-item"><h2><a href="'.$url.'">'.$title.'</a></h2>';
						if($remiseType && $remiseType!=='none'){
							echo '<span class="priceOld">'.$priceBase.' €</span>';
							echo '<span class="price">'.$priceNew.' €</span>';
						} else {
							echo '<span class="price">'.$priceBase.' €</span>';
						}
						echo '</div></div>';
						
						if($j == 2 || $i == count($latestPost)) {
							echo '</div>';
							echo '</div>';
							echo '</div>';
							$j = 0;
						}
					}
				}
				elseif(count($latestPost) <= 4)
				{
					$j++;
					if(count($latestPost) < 4)
					{
						echo '<div class="gr-4 gr-12@mobile">';
						echo '<div class="bloc-offer mea highlight-lightgray">';
						echo '<div class="owl-carousel owl-theme" data-auto="true">';
						
						echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[0][0].'" alt=""></a>';
						echo '<div class="text-item"><h2><a href="'.$url.'">'.$title.'</a></h2>';
						if($remiseType && $remiseType!=='none'){
							echo '<span class="priceOld">'.$priceBase.' €</span>';
							echo '<span class="price">'.$priceNew.' €</span>';
						} else {
							echo '<span class="price">'.$priceBase.' €</span>';
						}
						echo '</div></div>';
						echo '</div>';
						echo '</div>';
						echo '</div>';
					}
					else{
						
						if($j >= 3)
						{
							if($j == 3)
							{
								echo '<div class="gr-4 gr-12@mobile">';
								echo '<div class="bloc-offer mea highlight-lightgray">';
								echo '<div class="owl-carousel owl-theme" data-auto="true">';
							}
							echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[0][0].'" alt=""></a>';
							echo '<div class="text-item"><h2><a href="'.$url.'">'.$title.'</a></h2>';
							if($remiseType && $remiseType!=='none'){
								echo '<span class="priceOld">'.$priceBase.' €</span>';
								echo '<span class="price">'.$priceNew.' €</span>';
							} else {
								echo '<span class="price">'.$priceBase.' €</span>';
							}
							echo '</div></div>';
							
							if($i == count($latestPost)) {
								echo '</div>';
								echo '</div>';
								echo '</div>';
								$j = 0;
							}
						}
						else{
							echo '<div class="gr-4 gr-12@mobile">';
							echo '<div class="bloc-offer mea highlight-lightgray">';
							echo '<div class="owl-carousel owl-theme" data-auto="true">';
							
							echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[0][0].'" alt=""></a>';
							echo '<div class="text-item"><h2><a href="'.$url.'">'.$title.'</a></h2>';
							if($remiseType && $remiseType!=='none'){
								echo '<span class="priceOld">'.$priceBase.' €</span>';
								echo '<span class="price">'.$priceNew.' €</span>';
							} else {
								echo '<span class="price">'.$priceBase.' €</span>';
							}
							echo '</div></div>';
							
							echo '</div>';
							echo '</div>';
							echo '</div>';
						}
					}
				} 
		 }
		 else{
			 
			 if($i >= $firstIndex && $i <= $lastIndex)
			 {
				 if(count($latestPost) < $firstIndex) $firstIndex = count($latestPost);
				 $j++;
				if($j == 1)
				{
					echo '<div class="gr-4 gr-12@mobile">';
					echo '<div class="bloc-offer mea highlight-lightgray">';
					echo '<div class="owl-carousel owl-theme" data-auto="true">';
				}
				if($j < 4)
				{
					echo '<div class="item"><a href="'.$url.'"><img src="'.$imgsUrls[0][0].'" alt=""></a>';
					echo '<div class="text-item"><h2><a href="'.$url.'">'.$title.'</a></h2>';
					if($remiseType && $remiseType!=='none'){
						echo '<span class="priceOld">'.$priceBase.' €</span>';
						echo '<span class="price">'.$priceNew.' €</span>';
					} else {
						echo '<span class="price">'.$priceBase.' €</span>';
					}
					echo '</div></div>';
					
					if($j == 3 || $i == $lastIndex) {
						echo '</div>';
						echo '</div>';
						echo '</div>';
						$j = 0;
					}
				}
			 }
			 	
		 }
		$i++;
	}
	

}

function get_latestPosts($postType, $count){

	// Query Arguments
	$args = array(
					'post_status' => 'publish',
					'post_type' => $postType,
					'posts_per_page' => $count,
				);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		$posts = array();

		// The Loop
		while ( $query->have_posts() ) {
			$query->the_post();

			$post = array(
					'id' => get_the_ID(),
					'title' => get_the_title(),
					'description' => get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true),
					'link' => get_permalink(),
					'prixBase' =>  get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ),
					'prixBrade' => get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ),
					'photos' => get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true )
				);
			$posts[] = $post;
		}
		
		return $posts;
	}
}


function get_postById($id){

	if(empty($id)){
		return false;
	}

	// Query Arguments
	$args = array(
					'post_status' => 'publish',
					'post_type' =>  'any',
					'p' => (int)$id,
				);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {
		// The Loop
		while ( $query->have_posts() ) {
			$query->the_post();

			$post = array(
					'id' => get_the_ID(),
					'title' => get_the_title(),
					'description' => get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true),
					'link' => get_permalink(),
					'prixBase' =>  get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ),
					'prixBrade' => get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ),
					'photos' => get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ),
					'thumbnail' => get_the_post_thumbnail_url(get_the_ID(), 'annonceThumb')
				);
		}

		return $post;
	} else{
		return NULL;
	}
}





function buildAnnonceHome_v3($ids){


	if(!empty($ids)){

	// Build ?>

    <div class="gr-4 gr-12@mobile">
        <div class="bloc-offer">
            <div class="owl-carousel owl-theme highlight-pink items-case">
			    








	<?php



		for ($i=0; $i < 3 ; $i++) {
			$id = $ids[$i];

			$annonce = get_postById($id);



			if($annonce){
				// Get Variables
				$imgs = json_decode($annonce['photos'], true);
				$imgsUrls = array();
				if($imgs){
					foreach ($imgs as $img) {
						$imgsUrls[] = wp_get_attachment_image_src($img[0]['value'], 'annonceM' );
					}
				}

				$title = get_limitStringChars($annonce['title'], 22);
				
				$remiseType = get_post_meta($id,'_annonceAddMetaboxesPrix_prixRemiseType',true);


				if($remiseType){
					if($remiseType == 'percent'){
						$priceNew = number_format((float)(int)$annonce['prixBase'] - ($annonce['prixBase'] * get_post_meta($id,'_annonceAddMetaboxesPrix_prixpercent',true) / 100), 2, '.', '');
					}else if($remiseType == 'price'){
						$priceNew = number_format((float)$annonce['prixBrade'], 2, '.', '');
					}
				}



		        echo '<div class="item"><a href="'.$annonce['link'].'"><img src="'.$imgsUrls[count($imgsUrls)-1][0].'" alt=""></a>';
	            echo '<h2><a href="'.$annonce['link'].'">'.$title.'</a></h2>';

	           	if($remiseType && $remiseType!=='none'){
		        	echo '<span class="priceOld">'.$annonce['prixBase'].' €</span>';
		        	echo '<span class="price">'.$priceNew.' €</span>';
		    	} else {
		        	echo '<span class="price">'.$annonce['prixBase'].' €</span>';
		    	}
	            echo '</div>';

        	}





		}

	?>




            </div>
        </div>
    </div>
<?php
	}
}