<?php

add_action( 'wp_ajax_sendMailAjax', 'sendMailAjax' );
add_action( 'wp_ajax_nopriv_sendMailAjax', 'sendMailAjax' );

function sendMailAjax(){
	// Get Variables
	$formData = json_decode(stripslashes($_POST['data']), true);
	
	// Check Nonce
	check_ajax_referer( 'sendMailAjax', 'nonce');

	//Security Check
	if(!$formData['nom'] || !$formData['prenom'] || !$formData['mail']){
		echo json_encode($result = array('status'=>false, 'message'=>'Form Error, Empty Field, Try again'));
		wp_die();
	}

	// Write Mail
	$mailContent = NULL;
	foreach (array('sendMailAjax_nonce','_wp_http_referer') as $value) {
		unset($formData[$value]);
	}

	foreach ($formData as $key => $value) {
		$mailContent .= $key.' : '.$value.'<br/><br/>';
	}

	// Send Mail
	global $wpdb; // this is how you get access to the database
	$headers[] = 'From: thais <email@exemple.com>';
	$headers[] = 'Content-Type: text/html; charset=ISO-8859-1\r\n';
	$mail = wp_mail( get_option('formContactDestinationEmail'), $formData['msgSubject'], $mailContent, $headers);
		
	if($mail){
		$result = array('status'=>true, 'message'=>'Votre message a bien été envoyé,! L\'équipe thais traitera votre demande sous 48 heures dès réception de votre e-mail ');
	} else {
		$result = array('status'=>false, 'message'=>'Mail not sent try again');
	}

	echo json_encode($result, true);

	wp_die(); // this is required to terminate immediately and return a proper response
}