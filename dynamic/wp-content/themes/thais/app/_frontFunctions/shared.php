<?php

function termLink($id,$taxonomy){
	$term = get_term($id, $taxonomy);
	if(!is_wp_error($term)){
		echo '<a href="'.get_term_link($id, $taxonomy).'">'.$term->name.'</a>';
	}
}

function get_limitStringChars($str, $charsLimit){
	return (strlen($str) > $charsLimit) ? mb_strcut($str, 0, $charsLimit-4).' ...' : $str;
}

function the_replaceNullString($string=NULL, $replace='--'){
	if(!$string){
		echo $replace;
	}else{
		echo $string;
	}
}