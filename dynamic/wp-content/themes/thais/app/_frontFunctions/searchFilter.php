<?php

function termChildrenSearchFilter($id, $taxonomy, $selected=null){

    $termChildren = get_term_children( $id, $taxonomy );
    $ctr = 0;

    echo '<ul>';
    foreach ($termChildren as $termId) {

        $checked = ($termId == $selected) ? 'checked' : '';

        $termName = get_term($termId, $taxonomy);
        $termName = $termName->name;
        echo '<li><input type="checkbox" name="subcat" id="subcat-'.$termId.'-'.$ctr.'" value="'.$termId.'" '.$checked.'><label for="subcat-'.$termId.'-'.$ctr.'"><span class="skinInput"></span>'.$termName.'</label></li>';
        $ctr++;
    }
    echo '</ul>';

}

function searchFilter(){	

	// Get Current Category & Category Parent
	$currentTerm = get_queried_object();
	$currentTermId = $currentTerm->term_id;

    //
    if($currentTermId){
        $currentTermName = $currentTerm->name;
        $currentTermTaxonomy = $currentTerm->taxonomy;
        $currentTermParentId = $currentTerm->parent;

        // If Current Category have Parent --> Get Category Parent 
        if($currentTermParentId){
            $currentTermParent = get_term($currentTermParentId, $currentTermTaxonomy);
            $currentTermParentName = $currentTermParent->name;
        }

        // Get All Categories Parents
        $categoriesParents = array();
        $categories = get_terms(array('taxonomy' => $currentTermTaxonomy,'hide_empty' => false,'parent' => 0));
        foreach ($categories as $category) {
            $categoriesParents[$category->term_id] = $category->name;
        }

        // Delete current Category Parent
        if($currentTermParentId){
            unset($categoriesParents[$currentTermParentId]);
        } else {
            unset($categoriesParents[$currentTermId]);
        }
    }
	
	 
?>

<div class="finder-filter">
    <form action="" method="get" name="formrech" id="formrech">
        <input type="hidden" id="offset" name="offset" value="0">
        <h2><i class="fa fa-bars" aria-hidden="true"></i> Fitrer par</h2>
        <div class="form-content">
            <fieldset><div class="field">
                    <h3><i></i><span>Ordre</span></h3>
                    <div class="radiobox js-ordre-filtre">
                        <div><input type="radio" name="ordre" value="asc" checked id="fld_asc"><label for="fld_asc">Croissant</label><div class="check"></div></div>
                        <div><input type="radio" name="ordre" value="desc" id="fld_desc"><label for="fld_desc">Décroissant</label><div class="check"></div></div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <h3><i></i><span>Produit</span></h3>
                <div class="field search-by-keyword">
                    <input type="text" name="keyword" value="<?php echo $_GET['keyword']; ?>" placeholder="Chercher un produit">
                    <button class="cta-ico-search" type="button"><i class="fa fa-search"></i></button>
                </div>
            </fieldset>
            <fieldset>
                <h3><i></i><span>Categories</span></h3>
                <div class="field">
                    <?php 
                        if($currentTermId){
                    ?>
                            <div class="box-select">
                                <ul>
                                    <li class="selected js-cta-select" id="item-cat-0">

                                        <!-- Active Category -->
                                        <div>
                                            <?php
                                            if($currentTermParentId){
                                                $termId = $currentTermParentId;
                                                $termName = $currentTermParentName;
                                            } else {
                                                $termId = $currentTermId;
                                                $termName = $currentTermName;
                                            }
                                            echo '<input type="radio" name="cat" checked value="'.$termId.'">';
                                            echo '<a href="">';
                                                echo '<span>'.$termName.'</span>';
                                                echo '<i> > </i>';
                                            echo '</a>';
                                            ?>
                                        </div>
                                        <!---->


                                        <!-- Listing Others Category -->
                                        <ul>
                                            <?php
                                                echo '<li id="noMatch"><input type="radio" name="cat" value="">Toutes Categories</li>';
                                                $ctr = 1;
                                                foreach ($categoriesParents as $categoryId => $categoryName){
                                                    echo '<li id="item-cat-'.$ctr.'"><input type="radio" name="cat" value="'.$categoryId.'">'.$categoryName.'</li>';
                                                    $ctr++;
                                                }
                                            ?>
                                        </ul>
                                        <!---->
                                    </li>
                                </ul>
                            </div>
                            <div class="js-from-cat sub-cat checkbox-style">
                                <?php
                                    

                                    // Listing Sub Categories
                                    if($currentTermParentId){
                                        // Active Category -> Sub Categories
                                        termChildrenSearchFilter($currentTermParentId, $currentTermTaxonomy, $currentTermId);

                                        // Others Categories -> Sub Categories
                                        foreach ($categoriesParents as $categoryId => $categoryName) {
                                            termChildrenSearchFilter($categoryId, $currentTermTaxonomy);
                                        }

                                    } else {
                                        // Active Category -> Sub Categories
                                        termChildrenSearchFilter($currentTermId, $currentTermTaxonomy);

                                        // Others Categories -> Sub Categories
                                        foreach ($categoriesParents as $categoryId => $categoryName) {
                                            termChildrenSearchFilter($categoryId, $currentTermTaxonomy);
                                        }
                                    }

                                    
                                ?>
                            </div>
                    <?php
                        } else {
                    ?>
                            <div class="box-select">
                                <ul>
                                    <li class="selected js-cta-select" id="noMatch">

                                        <!-- Active Category (All categories) -->
                                        <div>
                                            <input type="radio" name="cat" checked value="">
                                            <a href="">
                                                <span>Toutes Categories</span>
                                                <i> > </i>
                                            </a>
                                        </div>
                                        <!---->


                                        <!-- Listing All Categories -->
                                        <ul>
                                            <?php
                                                $ctr = 0;
                                                $categories = get_terms(array('taxonomy' => 'category','hide_empty' => false,'parent' => 0));
                                                foreach ($categories as $category) {
                                                    echo '<li id="item-cat-'.$ctr.'"><input type="radio" name="cat" value="'.$category->term_id.'">'.$category->name.'</li>';
                                                    $ctr++;
                                                }
                                            ?>
                                        </ul>
                                        <!---->
                                    </li>
                                </ul>
                            </div>
                            <div class="js-from-cat sub-cat checkbox-style">
                                <?php
                                    

                                    // Listing Sub Categories
                                        //var_dump($categories);
                                        //die();
                                        foreach ($categories as $categorie) {
                                            termChildrenSearchFilter($categorie->term_id, 'category');
                                        }
                                    
                                    
                                    
                                ?>
                            </div>
                    <?php
                        }
                    ?>






                </div>
            </fieldset>
            <fieldset>
                <h3><i></i><span>Marques</span></h3>
                <div class="field">
                    <div class="checkbox-style">
                    	
                        <div class="field search-by-keyword js-marques-search">
                            <select id="tokenize_ajax" class="tokenize-sample" multiple="multiple"></select>
                            <button class="cta-ico-search" type="button"><i class="fa fa-search"></i></button>
                        </div>
                        <!--
                        <ul>
                            <?php
                                $ctr = 0;
                                $marques = get_terms( array('taxonomy' => 'brand','hide_empty' => false));
                                foreach ($marques as $marque) {
                                    echo '<li><input type="checkbox" name="marques" id="marques-'.$ctr.'" value="'.$marque->term_id.'"><label for="marques-'.$ctr.'"><span class="skinInput"></span>'.$marque->name.'</label></li>';
                                    $ctr++;
                                }
                            ?>
                        </ul>-->
                    </div>
                </div>
            </fieldset>
            

            <fieldset>
                <h3><i></i><span>Magasins</span></h3>
                <div class="field">
                    <div class="checkbox-style">
                        <ul>
                            <?php
                                $ctr = 0;
                                $magasins = get_users(array('role'=>'magasin'));
                                foreach ($magasins as $magasin) {
                                    echo '<li><input type="checkbox" name="magasin" id="magasin-'.$ctr.'" value="'.$magasin->ID.'"><label for="magasin-'.$ctr.'"><span class="skinInput"></span>'.get_user_meta($magasin->ID, 'companyName', true).'</label></li>';
                                    $ctr++;
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </fieldset>


            <fieldset>
                <h3><i></i><span>Couleurs</span></h3>
                <div class="field">
                    <div class="checkbox-style">
                        <ul>
                            <?php
                                $ctr = 0;
                                $colors = get_terms( array('taxonomy' => 'coloris','hide_empty' => false));
                                foreach ($colors as $color) {
                                    echo '<li><input type="checkbox" name="color" id="color-'.$ctr.'" value="'.$color->term_id.'"><label for="color-'.$ctr.'"><span class="skinInput"></span>'.$color->name.'</label></li>';
                                    $ctr++;
                                }
                            ?>
                        </ul>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <h3><i></i><span>Prix</span></h3>
                <div class="field">
                    <span class="range"><em class="js-range-min"> 50 €</em> - <em class="js-range-max">  10 000 €</em></span>
                    <div id="slider-snap" data-devis="€" data-start-from="50" data-start-to="10000" data-range-from="50" data-range-to="10000" data-range-step="50" data-idform="formrech"></div>
                    <input data-val="true" id="priceMin" name="priceMin" type="hidden" value="50" >
                    <input data-val="true" id="priceMax" name="prixMax" type="hidden" value="10000">
                    <span class="statique-range"><em> 50 €</em> <em>  10 000 €</em></span>
                </div>
            </fieldset>
            <fieldset>
                <h3><i></i><span>Materiaux</span></h3>
                <div class="field">
                    <div class="checkbox-style">
                        <ul class="checked">
                        <?php
                        		$ctr = 0;
                        		$finitions = get_terms( array('taxonomy' => 'finition','hide_empty' => false));
                        		foreach ($finitions as $finition) {
                        			echo '<li><input type="checkbox" name="finition" id="materials-'.$ctr.'" value="'.$finition->term_id.'"><label for="materials-'.$ctr.'"> <span class="skinInput"></span>'.$finition->name.'</label></li>';
                        			$ctr++;
                        		}
                        	?>
                        </ul>
                    </div>
                </div>
            </fieldset>
        </div>
    </form>
</div>

<?php
}