<?php


//$parentsTermsIds = array(1,8,9,10,11,12,13,14,15);
function megaMenu($parentsTermsIds, $taxonomyName, $callBack=NULL){
	
	// get current page taxonomy id or parent id
	$currentPageInfos = get_queried_object();
	$currentPageTaxonomyId = NULL;
	if($currentPageInfos){
		if($currentPageInfos->taxonomy){
			if($currentPageInfos->parent){
				$currentPageTaxonomyId = $currentPageInfos->parent;
			} else {
				$currentPageTaxonomyId = $currentPageInfos->term_taxonomy_id;
			}
		}
	}

	echo '<ul>';
	foreach ($parentsTermsIds as $parentTermId) {
		// Get Term Children
		$termChildren = get_term_children($parentTermId, $taxonomyName);

		// Current Category Page
		$currentCategoryClass = NULL;
		if($currentPageTaxonomyId == $parentTermId){
			$currentCategoryClass = 'current';
		}

		// Build MegaMenu
		echo '<li class="'.$currentCategoryClass.'">';
			termLink($parentTermId,$taxonomyName);
			if(!empty($termChildren)){
				echo '<div class="smenu mega"><div class="container">';
					echo '<div class="gr-12 gr-12@tablet gr-12@mobile"><ul>';
						// Sous Menu
						foreach ($termChildren as $termChild) {
							echo '<li>';
								termLink($termChild,$taxonomyName);
							echo '</li>';
						}
					echo '</ul></div>';
					//echo '<div class="gr-4 gr-12@tablet gr-12@mobile"><div class="bloc-offer megamenu"><img src="'.get_template_directory_uri().'/uploads/thumbs/thumb-smenu-01.jpg" alt=""><h2><a href=""> Chambre a coucher</a></h2><span class="price">3500.00 €</span></div></div><div class="gr-4 gr-12@tablet gr-12@mobile"><div class="bloc-offer megamenu"><img src="'.get_template_directory_uri().'/uploads/thumbs/thumb-smenu-02.jpg" alt=""><h2><a href=""> Meuble TV Chic</a></h2><span class="price">700.00 €</span></div></div>';
				echo '</div></div>';
			}
		echo '</li>';
	}
	$currentCategoryClass = (!is_page('inspirations'))?:'current';
	echo '<li class="'.$currentCategoryClass.'"><a href="'.esc_url( home_url("inspirations")).'"><i>Inspirations</i></a></li>';
	echo '</ul>';

}