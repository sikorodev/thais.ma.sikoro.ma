<?php

function get_magasinLink($id, $class=NULL, $anchor=NULL){
	if(!$anchor){
		$anchor = get_user_meta($id, 'companyName', true);
	};
	return '<a href="'.get_author_posts_url($id).'" class="'.$class.'">'.$anchor.'</a>';
}


function get_magasinBrands($id){
	// Set Args
	$args = array(
					'post_status' => 'publish',
					'author' => $id,
					'post_type' => 'annonce',
				);

	// Query
	$query = new WP_Query($args);

	// Get Posts
	if ( $query->have_posts() ) {
		// Brands Id Variable
		$magasinBrands = array('Marque-5');

		// The Loop
		while ( $query->have_posts() ) {
			$query->the_post();

			// Post Brand
			$postBrand = get_term(get_post_meta( get_the_id(), '_annonceAddMetaboxesDescription_marque', true ),'brand')->name;
			$member_terms = get_the_terms(get_the_ID(),'brand'); 

			// Add this Brand to Brands List if not exist yet
			if($member_terms){
				//loop through each term 
				foreach($member_terms as $term){
					//collect term information and display it
					$term_name = $term->name;
					if(!in_array($postBrand, $magasinBrands)){
						$magasinBrands[]=$term_name;
					}
				}
			}
		}
		
		//Restore original Post Data 
		wp_reset_postdata();

		return array_unique($magasinBrands);
	}
}


function the_magasinProducts($id){
	// Set Args
	$args = array(
					'post_status' => 'publish',
					'author' => $id,
					'post_type' => 'annonce',
					'posts_per_page' => 4
				);

	// Query
	$query = new WP_Query($args);

	// Get Posts
	if ( $query->have_posts() ) {

		// The Loop
		while ( $query->have_posts() ) {
			$query->the_post();

			// Build Annonce Excerpt
			buildAnnonceExcerpt(get_the_ID(), get_the_title(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true), get_permalink(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ), 3);

		}
		
		//Restore original Post Data 
		wp_reset_postdata();

		return $magasinBrands;
	}
}