<?php

function relatedPosts($taxonomy, $count){
	// Get Post Data
	$postId = get_the_ID();
	$postTerms = get_the_terms(get_the_ID(), $taxonomy);
	$postType = get_post_type();

	// Set Variables
	$terms = array();
	if($postTerms){
		foreach ($postTerms as $postTerm) {		
			$terms[] = $postTerm->term_id;
		}
	}

	// Get Related Posts
	$args = array(
				'post_status' => 'publish',
				'post_type' => $postType,
				'posts_per_page' => $count,
				'post__not_in' => array($postId),
				'tax_query' => array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'term_id',
						'terms'    => $terms,
					),
				)
			);

	// Send the Query
	$query = new WP_Query( $args );

	// Do statements if we found Posts
	if ( $query->have_posts() ) {

		// The Loop
		while ( $query->have_posts() ) {
			$query->the_post();

			// Build Annonce Excerpt
			buildAnnonceExcerpt(get_the_ID(), get_the_title(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true), get_permalink(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ));

		}
				
		//Restore original Post Data 
		wp_reset_postdata();
	} else {
		// Do statements if we don't found Posts
		$args = array(
				'post_status' => 'publish',
				'post_type' => $postType,
				'posts_per_page' => $count,
				'post__not_in' => array($postId)
			);

		// Send the Query
		$query = new WP_Query( $args );

		// Build Annonces
		if ( $query->have_posts() ) {

			// The Loop
			while ( $query->have_posts() ) {
				$query->the_post();

				// Build Annonce Excerpt
				buildAnnonceExcerpt(get_the_ID(), get_the_title(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true), get_permalink(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ));
			}
					
			//Restore original Post Data 
			wp_reset_postdata();
		}
	}
}
function relatedPostsCount($taxonomy, $count){
	// Get Post Data
	$postId = get_the_ID();
	$postTerms = get_the_terms(get_the_ID(), $taxonomy);
	$postType = get_post_type();

	// Set Variables
	$terms = array();
	if($postTerms){
		foreach ($postTerms as $postTerm) {		
			$terms[] = $postTerm->term_id;
		}
	}

	// Get Related Posts
	$args = array(
				'post_status' => 'publish',
				'post_type' => $postType,
				'posts_per_page' => $count,
				'post__not_in' => array($postId),
				'tax_query' => array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'term_id',
						'terms'    => $terms,
					),
				)
			);

	// Send the Query
	$query = new WP_Query( $args );

	// Do statements if we found Posts
	if ( $query->have_posts() ) {

	
	} else {
		// Do statements if we don't found Posts
		$args = array(
				'post_status' => 'publish',
				'post_type' => $postType,
				'posts_per_page' => $count,
				'post__not_in' => array($postId)
			);

		// Send the Query
		$query = new WP_Query( $args );

		
	}
	return $query->post_count;
}