<?php

// 
function pathway(){
	// Pathway for Single Post
	if(is_single()){
		// Statements for each PostType
		switch (get_post_type()) {
			case 'annonce':
				// Get Post Term & Term Parent
				$terms = get_the_terms(get_the_ID(), 'category');
				$term = get_term($terms[0]->term_id, $terms[0]->taxonomy);
				$termId = $term->term_id;
				$termTaxonomy = $term->taxonomy;
				$termParentId = $term->parent;

				// Build Pathway
				echo '<div class="breadcrumb">';
					echo '<a href="'.esc_url( home_url("/")).'">thais</a>';

					// Post Term Parent
					if($termParentId != '0'){
						echo '<span>></span>';
						termLink($termParentId,$termTaxonomy);
					}

					// Post Term
					echo '<span>></span>';
					termLink($termId,$termTaxonomy);

					// Post Title
					echo '<span>></span>';
					the_title();
				echo '</div>';
				break;

			default:
				break;
		}
	} else if(is_archive()) { // Pathway for Archive
		// Get Term & Term Parent

		$term = get_queried_object();
		$termId = $term->term_id;
		$termName = $term->name;
		$termTaxonomy = $term->taxonomy;
		$termParentId = $term->parent;		

		// Build Pathway
		echo '<div class="breadcrumb">';
			echo '<a href="'.esc_url( home_url("/")).'">thais</a>';
			// For Inspiration Taxonomy
			if($termTaxonomy == 'inspiration'){
				echo '<span>></span>';
				echo '<a href="'.esc_url( home_url("inspirations")).'">Inspirations</a>';
			}
			// Term Parent
			if($termParentId != '0'){
				echo '<span>></span>';
				termLink($termParentId,$termTaxonomy);
				
				// Term
				echo '<span>></span>';
				echo $termName;
			} else {
				// Term
				echo '<span>></span>';
				echo $termName;
			}
		echo '</div>';
	} else if(is_page()){
		// Build Pathway
		echo '<div class="breadcrumb">';
			echo '<a href="'.esc_url( home_url("/")).'">thais</a>';

			// Page Name
			echo '<span>></span>';
			echo get_the_title();

			// Page Recherche Keyword
			if(is_page('recherche')){
				if($_GET['keyword']){
					echo ' : '.$_GET['keyword'];
				} else {
					echo ' : Tous les produits';
				}
			}
		echo '</div>';
	}

}

