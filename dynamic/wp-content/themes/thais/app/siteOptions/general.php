<?php

use framework\themeOptionsFieldsBuilder as fieldBuilder;

if ( is_admin() ){ // admin actions
  add_action( 'admin_menu', 'siteOptionsMenu' );
  add_action( 'admin_init', 'registerSiteOptionsSettings' );
}


function siteOptionsMenu() {
    add_menu_page('General Options', 'General Options', 'administrator', __FILE__, 'siteOptionsSettingsPage' , 'dashicons-admin-settings', 1 );
}


function siteOptionsSettings(){
    $settings = array(
        'config' => array(
            'pageTitle'=>'Configuration Generale de la Plateforme thais',
            'settingsFields' => 'thaisGeneralOptions'
        ),
        'sections' => array(
            array(
                'title' => 'Administration Preferences',
                'fields' => array(
                    array(
                        'type' => 'text',
                        'name' => 'formContactDestinationEmail',
                        'value' => 'getIt',
                        'placeholder' => 'Administration E-mail',
                        'class' => 'regular-text',
                    ),
                ),
            ),





			array(
                'title' => 'Header',
                'fields' => array(
					array(
                        'type' => 'image',
                        'name' => 'imageLogo',
						'placeholder' => 'Logo',
                        'value' => 'getIt',
                        'class' => '',
                    ),
					array(
                        'type' => 'textarea',
                        'name' => 'googleAnalytique',
						'placeholder' => 'Code Google analytique',
                        'value' => 'getIt',
                        'class' => 'large-text',
                    )
                ),
            ),
			
			array(
                'title' => 'coordonnées',
                'fields' => array(
					array(
                        'type' => 'text',
                        'name' => 'titleadressContact',
                        'value' => 'getIt',
                        'placeholder' => 'Title adress',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'adressContact',
                        'value' => 'getIt',
                        'placeholder' => 'Adresse',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'payContact',
                        'value' => 'getIt',
                        'placeholder' => 'Pays',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'telContact',
                        'value' => 'getIt',
                        'placeholder' => 'Téléphone',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'faxContact',
                        'value' => 'getIt',
                        'placeholder' => 'Fax',
                        'class' => 'regular-text',
                    )
                ),
            ),
			
			array(
                'title' => 'Footer',
                'fields' => array(
					array(
                        'type' => 'image',
                        'name' => 'logofooter',
						'placeholder' => 'Logo Footer',
                        'value' => 'getIt',
                        'class' => '',
                    ),
					array(
                        'type' => 'image',
                        'name' => 'logoteenor',
						'placeholder' => 'Logo Teenor',
                        'value' => 'getIt',
                        'class' => '',
                    ),
                    array(
                        'type' => 'text',
                        'name' => 'textSocialFooter',
                        'value' => 'getIt',
                        'placeholder' => 'Social Media Titre',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'linkFacebock',
                        'value' => 'getIt',
                        'placeholder' => 'Lien Facebook',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'linkTwitter',
                        'value' => 'getIt',
                        'placeholder' => 'Lien Twitter',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'linkLinkedin',
                        'value' => 'getIt',
                        'placeholder' => 'Lien Linkedin',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'linkInstagram',
                        'value' => 'getIt',
                        'placeholder' => 'Lien Instagram',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'linkGoogle',
                        'value' => 'getIt',
                        'placeholder' => 'Lien Google',
                        'class' => 'regular-text',
                    ),
					array(
                        'type' => 'text',
                        'name' => 'textCopyRight',
                        'value' => 'getIt',
                        'placeholder' => 'Text CopyRight',
                        'class' => 'regular-text',
                    )
                ),
            ),
        ),
    );
    
    return $settings;
}


function registerSiteOptionsSettings() {
    $fields = siteOptionsSettings();
    foreach ($fields['sections'] as $section) {
        foreach ($section['fields'] as $field) {
            register_setting( $fields['config']['settingsFields'], $field['name'] );
        }
    }
}


function siteOptionsSettingsPage() {
    $siteOptionsSettings = siteOptionsSettings();

    // Load fieldBuilder Class
    $fieldBuilder = new fieldBuilder;

    // Build Metabox
    $fieldBuilder->fieldsBuilder($siteOptionsSettings);
}