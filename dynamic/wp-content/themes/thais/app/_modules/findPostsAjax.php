<?php

add_action( 'wp_ajax_findPosts', 'findPosts' );

function findPosts() {
	global $wpdb; // this is how you get access to the database

	// Get Variables From Post
	$filter = $_POST['filter'];

	$postsperpage = $filter['posts_per_page'];
	$offset = $_POST['offset'];
	$keyword = $_POST['keyword'];

	// Search Posts
	if($filter['posttype']){
		$posttype = $filter['posttype'];
		// Create Result Variable to return
		$result=array(
					'config'=>array(
									'post_type' => $posttype,
									'search_term' => $keyword,
									),
					'posts' => array(),
				);

		// Query Arguments
		$args = array('post_status' => array('publish','future') );
		(!$posttype ?: $args['post_type'] = $posttype);
		(!$postsperpage ?: $args['posts_per_page'] = $postsperpage);
		(!$offset ?: $args['offset'] = $offset);
		(!$keyword ?: $args['s'] = $keyword);

		//var_dump($arg);


		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {

			// The Loop
			while ( $query->have_posts() ) {
				$query->the_post();

				/*$thumbnail = json_decode(get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos', true), true);
				$thumbnail = wp_get_attachment_image_src($thumbnail[0][0]['value'], 'annonceM' );
				$thumbnail = $thumbnail[0];*/
				
				$thumb_id = get_post_thumbnail_id();
				$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
				$thumbnail = $thumb_url[0];




				$post = array(
						'id' => get_the_id(),
						'title' => get_the_title(),
						'thumbnail' => $thumbnail ,

					);
				array_push($result['posts'], $post);
			}
			
			echo json_encode($result, true);
			
			//Restore original Post Data 
			wp_reset_postdata();
		}
	} else if($filter['taxonomy']){
		//var_dump($offset);die();
		
		$taxonomy = $filter['taxonomy'];

		// Create Result Variable to return
		$result = array(
					'config'=>array(
									'taxonomy' => $taxonomy,
									'search_term' => $keyword,
									),
					'posts' => array(),
				);

		// Get terms


		$args = array(
						'taxonomy' => $taxonomy,
						'hide_empty' => false,
						'orderby' => 'term_id',
						'order' => 'DESC'
					);

		(!$postsperpage ?: $args['number'] = $postsperpage);
		(!$offset ?: $args['offset'] = $offset);
		(!$keyword ?: $args['search'] = $keyword);

		$terms = get_terms($args);

		if(!empty($terms)){
			foreach ($terms as $term) {

				$termThumbnail = get_term_meta( $term->term_id, 'thumbnail', true );
				$termThumbnail = wp_get_attachment_image_src($termThumbnail);
				$termThumbnail = $termThumbnail[0];

				$post = array(
						'id' => $term->term_id,
						'title' => $term->name,
						'thumbnail' => $termThumbnail ,
					);
				array_push($result['posts'], $post);
			}
			
			echo json_encode($result, true);

			//Restore original Post Data 
			wp_reset_postdata();
		}

	}


	wp_die(); // this is required to terminate immediately and return a proper response
}