<?php

namespace framework;

class themeOptionsFieldsBuilder{

	function __construct(){
		$this->postId = get_the_ID();
		wp_enqueue_media();
		}

	private function getFieldValue($value, $name) {
		if($value == 'getIt')
			return $value = get_option($name);
		else
			return $value;
	}

	public function geTaxonomyList($taxonomy){
		$args = array(
						'taxonomy' => $taxonomy,
						'hide_empty' => false,
						'fields' => 'id=>name'
					);

		$taxonomyList = get_terms($args);
		return $taxonomyList;
	}


	public function text($name, $value, $placeholder, $class){

		// Get Field Value
		$value = $this->getFieldValue($value, $name);

		// Build Field
		echo'<tr>';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo '<input id="'.$name.'" type="text" name="'.$name.'" value="'.$value.'" placeholder="'.$placeholder.'" class="'.$class.'" />';
			echo '</td>';
		echo '</tr>';
	}
	public function image($name, $value, $placeholder, $class){

		// Get Field Value
		$value = $this->getFieldValue($value, $name);
		$upload_link = esc_url( get_upload_iframe_src( 'image', $this->postId ) );
		$your_img_src = wp_get_attachment_image_src( $value, 'full' );
		$you_have_img = is_array( $your_img_src );

		// Build Field
		echo'<tr>';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo '<div class="itemPreview">';
				echo '<input sptype="'.__FUNCTION__.'" class="idImg" name="'.$name.'" type="hidden" value="'.$value.'" />';
				if ( $you_have_img ) :
					echo '<img src="'.$your_img_src[0].'" alt="" class="previewImg" />';
				endif;
				echo '<a class="uploadImg '.(($you_have_img) ? "hidden" : null).'" href="'.$upload_link.'">Set image</a>';
				echo '<a class="deleteImg '.(($you_have_img) ? null : "hidden").'" href="#">Remove image</a>';
				echo '</div>';
			echo '</td>';
		echo '</tr>';
	}

	public function textarea($name, $value, $placeholder, $class){
		
		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Build Field
		echo'<tr>';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo '<textarea style="max-width:400px;height:200px;" id="'.$name.'" name="'.$name.'" placeholder="'.$placeholder.'" class="'.$class.'">'.$value.'</textarea>';
			echo '</td>';
		echo '</tr>';
	}

	public function editor($name, $value){

		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Build Editor
		echo'<tr>';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				wp_editor( $value, strtolower($name), array('textarea_name'=>$name,'textarea_rows'=>10,));
			echo '</td>';
		echo '</tr>';				
	}
	
	

	public function select($name, $data, $value, $placeholder, $class){
		/**
		 * $data schema = array('type' =>'taxonomy', 'name'=>'', 'data'=array();
		 */

		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Get Field Options Data
		if($data['type']=='taxonomy'){
			$fieldOptionsData = $this->geTaxonomyList($data['name']);
		} elseif($data['type']=='array') {
			$fieldOptionsData = $data['data'];
		}

		// Select none option
		$selected = (!array_key_exists($value, $fieldOptionsData)) ? 'selected' : null ;

		// Build Field
		echo'<tr>';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo'<select id="'.$name.'" sptype="'.__FUNCTION__.'" name="_'.$name.'" class="'.$class.'">';
					echo '<option value="" disabled '.$selected.'>'.$placeholder.'</option>';
					if($selected)
						foreach ($fieldOptionsData as $optionKey => $optionData) {
							echo '<option value="'.$optionKey.'">'.$optionData.'</option>';
						}
					else
						foreach ($fieldOptionsData as $optionKey => $optionData) {
							echo '<option value="'.$optionKey.'" '.(($value == $optionKey) ? "selected" : null).'>'.$optionData.'</option>';
						}
				echo'</select>';
			echo '</td>';
		echo '</tr>';
	}
	public function selectPages($name, $data, $value, $placeholder, $class){
		/**
		 * $data schema = array('type' =>'taxonomy', 'name'=>'', 'data'=array();
		 */

		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Select none option
		$selected = null ;

		// Build Field
		echo'<tr>';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo'<select id="'.$name.'"  name="'.$name.'" class="'.$class.'">';
					echo '<option value="">'.$placeholder.'</option>';
		  			$pages = get_pages(); 
					  foreach ( $pages as $page ) {
						$option = '<option value="' .$page->ID. '" '.(($value == $page->ID) ? "selected" : null).'>';
						$option .= $page->post_title;
						$option .= '</option>';
						echo $option;
					  }
				echo'</select>';
			echo '</td>';
		echo '</tr>';
	}
	

	public function posts($name, $value, $filter, $description){

		// Get Field Value
		$value = $this->getFieldValue($value, $name);
		
		$valueCheck = get_option("LatestNewsHome");

		// Build Module
		echo '<div class="posts">';
			echo '<p>'.$description.'</p>';
			echo '<div class="postsChosen">';
				$checked = '';
				if($valueCheck == "1") $checked = 'checked="checked"';
				echo '<div class="latestPostHome"><input type="checkbox" id="latestPostHome" name="LatestNewsHome" '.$checked.' value="1"> <label for="latestPostHome">les 4 derniers Articles</label></div>';
				echo '<input class="postsChosen_data" name="'.$name.'" type="hidden" value="'.htmlspecialchars($value).'" spFilter="'.htmlspecialchars(json_encode($filter)).'" />';
				if($value == '' || $value =='[]'){
					echo '<div class="postsChosen_list"></div>';
				}else{
					echo '<div class="postsChosen_list">';
						$value = json_decode($value, true);

						if($filter['taxonomy']){
							foreach ($value as $id) {
								$term = get_term( $id, $filter['taxonomy']);
								$termThumbnail = get_term_meta( $id, 'thumbnail', true );
								$termThumbnail = wp_get_attachment_image_src($termThumbnail);
								$termThumbnail = $termThumbnail[0];
								
								$url = get_the_post_thumbnail_url($id, 'full');

								echo '<article id="'.$id.'"><img src="'.$url.'"><h1>'.$term->name.'</h1><button class="remove">Remove</button></article>';
							}
						} else if($filter['posttype']){
							foreach ($value as $postId) {
								$post = get_post($postId);

								$thumbnail = json_decode(get_post_meta($postId,'_annonceAddMetaboxesPhotos_photos', true), true);
								$thumbnail = wp_get_attachment_image_src($thumbnail[0][0]['value']);
								$thumbnail = $thumbnail[0];

								$url = get_the_post_thumbnail_url($postId, 'full');

								echo '<article  id="'.$postId.'"><img src="'.$url.'"><h1>'.$post->post_title.'</h1><button class="remove">Remove</button></article>';
							}
						}


					echo '</div>';
				}
				echo '<button class="addPost">+ Add Posts</button>';
			echo '</div>';

			// Posts Page Search List
			echo '<div class="postsSearchSection">';
				echo '<div class="postsSearchSection_box">';
					echo '<div class="postsSearchSection_boxHead">Chercher Une Article</div>';

					echo '<div class="postsSearchSection_boxContent">';

						echo '<div class="postsSearchSection_searchForm">';
							echo '<input type="text" name="postsSearchSection_searchKeyword" class="postsSearchSection_searchKeyword">';
							echo '<button>Chercher</button>';
						echo '</div>';

						echo '<h1 class="postsSearchSection_searchTitle"></h1>';
							
						echo '<div class="postsSearchSection_searchResult"></div>';

						echo '<div class="postsSearchSection_loadMore"><button>Load More</button></div>';
					echo '</div>';

					echo '<div class="postsSearchSection_boxFooter"><button class="done">Terminer</button></div>';
				echo '</div>';
			echo '</div>';
		echo '</div>';
	}

	public function fieldsBuilder($fields){
		/*
		if('value' => 'getIt') get stored field value

		$Schema = array(
			'config' => array(
				'pageTitle'=>'Configuration Generale de la Plateforme thais',
				'settingsFields' => 'thaisGeneralOptions'
			),
			'sections' => array(
				array(
					'title' => 'Vous Servir',
					'fields' => array(
						array(
							'type' => 'text',
							'name' => 'servirServiceProduit',
							'value' => 'getIt',
							'placeholder' => 'Service Produit',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'servirServiceClient',
							'value' => 'getIt',
							'placeholder' => 'Service Client',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'servirServiceValisation',
							'value' => 'getIt',
							'placeholder' => 'Service Valisation',
							'class' => 'regular-text',
						)
					),
				),
				array(
					'title' => 'Newsletter',
					'fields' => array(
						array(
							'type' => 'text',
							'name' => 'newsletterTitle',
							'value' => 'getIt',
							'placeholder' => 'Section Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'newsletterSubTitle',
							'value' => 'getIt',
							'placeholder' => 'Section Sous Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'newsletterButtonText',
							'value' => 'getIt',
							'placeholder' => 'Section C2A Texte',
							'class' => 'regular-text',
						)
					),
				),
				array(
					'title' => 'Footer',
					'fields' => array(
						array(
							'type' => 'textarea',
							'name' => 'footerSiteDescription',
							'value' => 'getIt',
							'placeholder' => 'Site Description',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'footerFollowUsTitle',
							'value' => 'getIt',
							'placeholder' => 'Social Media Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'footerFollowUsSubTitle',
							'value' => 'getIt',
							'placeholder' => 'Social Media Sous Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'textarea',
							'name' => 'footerSiteCopyright',
							'value' => 'getIt',
							'placeholder' => 'Site Copyright',
							'class' => 'regular-text',
						)
					),
				),
			),
		);
		*/

		// Build Theme Options Page
		echo '<div class="wrap">';
			echo '<h1>'.$fields['config']['pageTitle'].'</h1>';
			echo '<hr>';
			echo '<form method="post" action="options.php">';
			settings_fields($fields['config']['settingsFields']);
			do_settings_sections($fields['config']['settingsFields']);
			foreach ($fields['sections'] as $section) {
				echo '<h2>'.$section['title'].'</h2>';
				echo '<table class="form-table">';
					foreach ($section['fields'] as $field) {
						switch ($field['type']) {
							case "text":
								$this->text($field['name'], $field['value'], $field['placeholder'], $field['class']);
								break;
							case "image":
								$this->image($field['name'], $field['value'], $field['placeholder'], $field['class']);
								break;
							case "checkbox":
								$this->checkbox($field['name'], $field['value'], $field['placeholder'], $field['class']);
								break;	
							case "textarea":
								$this->textarea($field['name'], $field['value'], $field['placeholder'], $field['class']);
								break;
							case "select":
								$this->select($field['name'], $field['data'], $field['value'], $field['placeholder'], $field['class']);
								break;
							case "selectPages":
								$this->selectPages($field['name'], $field['data'], $field['value'], $field['placeholder'], $field['class']);
								break;	
							case "editor":
								$this->editor($field['name'], $field['value']);
								break;
							case "posts":
								$this->posts($field['name'], $field['value'], $field['filter'], $field['description']);
								break;
							default:
								echo '<p><u>'.$field['type'].'</u> is invalid field.</p>';
						}
					}
				echo '</table>';
				echo '<hr>';
			}
			submit_button();
			echo '</form>';
		echo '</div>';
	}

}