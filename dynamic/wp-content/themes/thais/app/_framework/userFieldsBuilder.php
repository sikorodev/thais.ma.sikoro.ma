<?php

namespace framework;

class userFieldsBuilder{

	public $userId;

	function __construct(){
		$this->userId = get_current_user_id();
		wp_enqueue_media();
	}

	private function getFieldValue($value, $name) {
		if($value == 'getIt')
			return $value = get_user_meta($this->userId, $name, true);
		else
			return $value;
	}

	public function text($name, $value, $placeholder, $class){

		// Get Field Value
		$value = $this->getFieldValue($value, $name);

		// Build Field
		echo'<tr class="'.$name.'">';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo '<input id="'.$name.'" type="text" name="'.$name.'" value="'.$value.'" placeholder="'.$placeholder.'" class="'.$class.'" />';
			echo '</td>';
		echo '</tr>';
	}

	public function checkbox($name, $value, $placeholder){

		// Get Field Value
		$value = $this->getFieldValue($value, $name);

		$checked = NULL;
		if($value=='true'){
			$checked = 'checked';
		}

		// Build Field
		echo'<tr class="'.$name.'">';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
				echo '<input type="checkbox" id="'.$name.'" name="'.$name.'" value="" '.$checked.'>';


			echo '</td>';
		echo '</tr>';
	}

	

	

	public function image($name, $value, $placeholder){
		
		// Get Field Data
		$value = $this->getFieldValue($value, $name);
		
		// Image Library Init
		$upload_link = esc_url( get_upload_iframe_src( 'image', $this->postId ) );
		$your_img_src = wp_get_attachment_image_src( $value, 'full' );
		$you_have_img = is_array( $your_img_src );

		// Build Field
		echo'<tr class="'.$name.'">';
			echo '<th scope="row"><label for="'.$name.'">'.$placeholder.'</label></th>';
			echo '<td>';
			echo '<input class="idImg" id="'.$name.'" name="'.$name.'" type="hidden" value="'.$value.'" />';
				// Show Image if exist
				if ( $you_have_img ) :
					echo '<img src="'.$your_img_src[0].'" alt="" class="previewImg" />';
				endif;

				// Add & Remove image
				echo '<a class="uploadImg '.(($you_have_img) ? "hidden" : null).'" href="'.$upload_link.'">Set image</a>';
				echo '<a class="deleteImg '.(($you_have_img) ? null : "hidden").'" href="#">Remove image</a>';
			echo '</td>';
		echo'</tr>';
	}

	

	public function fieldsBuilder($fields){
		/*
		if('value' => 'getIt') get stored field value

		$Schema = array(
			'config' => array(
				'pageTitle'=>'Configuration Generale de la Plateforme thais',
				'settingsFields' => 'thaisGeneralOptions'
			),
			'sections' => array(
				array(
					'title' => 'Vous Servir',
					'fields' => array(
						array(
							'type' => 'text',
							'name' => 'servirServiceProduit',
							'value' => 'getIt',
							'placeholder' => 'Service Produit',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'servirServiceClient',
							'value' => 'getIt',
							'placeholder' => 'Service Client',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'servirServiceValisation',
							'value' => 'getIt',
							'placeholder' => 'Service Valisation',
							'class' => 'regular-text',
						)
					),
				),
				array(
					'title' => 'Newsletter',
					'fields' => array(
						array(
							'type' => 'text',
							'name' => 'newsletterTitle',
							'value' => 'getIt',
							'placeholder' => 'Section Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'newsletterSubTitle',
							'value' => 'getIt',
							'placeholder' => 'Section Sous Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'newsletterButtonText',
							'value' => 'getIt',
							'placeholder' => 'Section C2A Texte',
							'class' => 'regular-text',
						)
					),
				),
				array(
					'title' => 'Footer',
					'fields' => array(
						array(
							'type' => 'textarea',
							'name' => 'footerSiteDescription',
							'value' => 'getIt',
							'placeholder' => 'Site Description',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'footerFollowUsTitle',
							'value' => 'getIt',
							'placeholder' => 'Social Media Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'text',
							'name' => 'footerFollowUsSubTitle',
							'value' => 'getIt',
							'placeholder' => 'Social Media Sous Titre',
							'class' => 'regular-text',
						),
						array(
							'type' => 'textarea',
							'name' => 'footerSiteCopyright',
							'value' => 'getIt',
							'placeholder' => 'Site Copyright',
							'class' => 'regular-text',
						)
					),
				),
			),
		);
		*/

		// Build Theme Options Page
		echo '<div class="wrap">';
			echo '<h1>'.$fields['config']['pageTitle'].'</h1>';
			echo '<hr>';
			echo '<form method="post" action="" id="profileMagasin">';
				wp_nonce_field( 'userProfile', 'userProfile_nonce');
				foreach ($fields['sections'] as $section) {
					echo '<h2>'.$section['title'].'</h2>';
					echo '<table class="form-table">';
						foreach ($section['fields'] as $field) {
							switch ($field['type']) {
								case "text":
									$this->text($field['name'], $field['value'], $field['placeholder'], $field['class']);
									break;
								case "image":
									$this->image($field['name'], $field['value'], $field['placeholder']);
									break;
								case "checkbox":
									$this->checkbox($field['name'], $field['value'], $field['placeholder']);
								default:
									echo '<p><u>'.$field['type'].'</u> is invalid field.</p>';
							}
						}
					echo '</table>';
					echo '<hr>';
				}
				submit_button();
			echo '</form>';
		echo '</div>';
	}

}