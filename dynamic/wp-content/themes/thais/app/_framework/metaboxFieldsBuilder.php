<?php

namespace framework;

class metaboxFieldsBuilder{

	public $postId;

	function __construct(){
		$this->postId = get_the_ID();
		wp_enqueue_media();
	}

	private function getFieldValue($value, $name) {
		if($value == 'getIt')
			return $value = get_post_meta( $this->postId, '_'.$name, true );
		else
			return $value;
	}

	public function getTaxonomyFieldValue($taxonomyName, $term, $name){
		$termID = $term->term_id;

		$termMeta = get_option( $taxonomyName.'_'.$termID );

		//var_dump($termMeta);

		$customfield= $termMeta[$name];

		//var_dump($customfield);
		die();
	}

	public function geTaxonomyList($taxonomy){
		$args = array(
						'taxonomy' => $taxonomy,
						'hide_empty' => false,
						'fields' => 'id=>name'
					);

		$taxonomyList = get_terms($args);
		return $taxonomyList;
	}

	public function text($tag, $name, $value, $placeholder, $class){

		// Get Field Value
		$value = $this->getFieldValue($value, $name);

		// Build Field
		echo ($tag)?'<'.$tag.'>':'';
		echo '<input sptype="'.__FUNCTION__.'" type="text" name="_'.$name.'" value="'.$value.'" placeholder="'.$placeholder.'" class="'.$class.'" />';
		echo ($tag)?'</'.$tag.'>':'';
	}

	public function textarea($tag, $name, $value, $placeholder, $class){
		
		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Build Field
		echo ($tag)?'<'.$tag.'>':'';
		echo "<textarea sptype='".__FUNCTION__."' name='_".$name."' placeholder='".$placeholder."' class='".$class."'>".$value."</textarea>";
		echo ($tag)?'</'.$tag.'>':'';
	}

	public function editor($name, $value){

		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Build Editor
		wp_editor( $value, strtolower($name), array('textarea_name'=>'_'.$name,'textarea_rows'=>10,));
	}

	public function select($tag, $name, $data, $value, $placeholder, $class){
		/**
		 * $data schema = array('type' =>'taxonomy', 'name'=>'', 'data'=array();
		 */

		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Get Field Options Data
		if($data['type']=='taxonomy'){
			$fieldOptionsData = $this->geTaxonomyList($data['name']);
		} elseif($data['type']=='array') {
			$fieldOptionsData = $data['data'];
		}

		// Select none option
		$selected = (!array_key_exists($value, $fieldOptionsData)) ? 'selected' : null ;

		// Build Field
		echo ($tag)?'<'.$tag.'>':'';
		echo'<select sptype="'.__FUNCTION__.'" name="_'.$name.'" class="'.$class.'">';
		echo '<option value="" disabled '.$selected.'>'.$placeholder.'</option>';

		if($selected)
			foreach ($fieldOptionsData as $optionKey => $optionData) {
				echo '<option value="'.$optionKey.'">'.$optionData.'</option>';
			}
		else
			foreach ($fieldOptionsData as $optionKey => $optionData) {
				echo '<option value="'.$optionKey.'" '.(($value == $optionKey) ? "selected" : null).'>'.$optionData.'</option>';
			}

		echo'</select>';
		echo ($tag)?'</'.$tag.'>':'';
	}
	
	public function selectPages($tag, $name, $data, $value, $placeholder, $class){
		/**
		 * $data schema = array('type' =>'taxonomy', 'name'=>'', 'data'=array();
		 */

		// Get Field Data
		$value = $this->getFieldValue($value, $name);

		// Get Field Options Data
		

		// Select none option
		$selected = (!array_key_exists($value, $fieldOptionsData)) ? 'selected' : null ;

		// Build Field
		echo ($tag)?'<'.$tag.'>':'';
		echo'<select sptype="'.__FUNCTION__.'" name="_'.$name.'" class="'.$class.'">';
		echo '<option value="" disabled '.$selected.'>'.$placeholder.'</option>';
		  $pages = get_pages(); 
		  foreach ( $pages as $page ) {
			$option = '<option value="' .$page->ID. '" '.(($value == $page->ID) ? "selected" : null).'>';
			$option .= $page->post_title;
			$option .= '</option>';
			echo $option;
		  }

		echo'</select>';
		echo ($tag)?'</'.$tag.'>':'';
	}

	public function checkbox($tag, $name, $value, $placeholder, $class){
		/**
		 * $data schema = array('type' =>'taxonomy', 'name'=>'', 'data'=array();
		 */

		// Get Field Data
		$value = $this->getFieldValue($value, $name);


		// Select none option
		//$selected = (!array_key_exists($value, $fieldOptionsData)) ? 'selected' : null ;

		// Build Field
		$id = get_current_user_id();
		$checked = "";
		if($value == true) $checked = 'checked="checked"';
		if(is_super_admin($id))
		{
			echo ($tag)?'<'.$tag.'>':'';
			echo '<label>'.$placeholder.' : </label>';
			echo '<input type="checkbox" '.$checked.' sptype="'.__FUNCTION__.'" name="_'.$name.'" class="'.$class.'">';
			echo ($tag)?'</'.$tag.'>':'';
		}
		
	}

	public function image($tag, $name, $value){
		
		// Get Field Data
		$value = $this->getFieldValue($value, $name);
		
		// Image Library Init
		$upload_link = esc_url( get_upload_iframe_src( 'image', $this->postId ) );
		$your_img_src = wp_get_attachment_image_src( $value, 'full' );
		$you_have_img = is_array( $your_img_src );

		// Build Field
		echo ($tag)?'<'.$tag.'>':'';

		echo '<input sptype="'.__FUNCTION__.'" class="idImg" name="_'.$name.'" type="hidden" value="'.$value.'" />';

		// Show Image if exist
		if ( $you_have_img ) :
			echo '<img src="'.$your_img_src[0].'" alt="" class="previewImg" />';
		endif;

		// Add & Remove image
		echo '<a class="uploadImg '.(($you_have_img) ? "hidden" : null).'" href="'.$upload_link.'">Set image</a>';
		echo '<a class="deleteImg '.(($you_have_img) ? null : "hidden").'" href="#">Remove image</a>';

		echo ($tag)?'</'.$tag.'>':'';
	}

	public function pdf($tag, $name, $value){
		
		// Get Field Data
		$value = $this->getFieldValue($value, $name);
		
		// Image Library Init
		$upload_link = esc_url( get_upload_iframe_src( 'application/pdf', $this->postId ) );
		$your_pdf_src = wp_get_attachment_url($value);

		// Build Field
		echo ($tag)?'<'.$tag.'>':'';

		echo '<input sptype="'.__FUNCTION__.'" class="idPdf" name="_'.$name.'" type="hidden" value="'.$value.'" />';

		// Show Image if exist
		if ( $your_pdf_src ) :
			echo '<div class="previewPdf"><span class="dashicons dashicons-media-default"></span><a class="title" href="'.$your_pdf_src.'">'.get_the_title($value).'</a></div>';
		endif;

		// Add & Remove image
		echo '<a class="uploadPdf '.(($your_pdf_src) ? "hidden" : null).'" href="'.$upload_link.'">Set PDF</a>';
		echo '<a class="deletePdf '.(($your_pdf_src) ? null : "hidden").'" href="#">Remove PDF</a>';

		echo ($tag)?'</'.$tag.'>':'';
	}

	public function listItems($fields, $name, $value, $title){
	    /*
			$FieldsSchema = array(
				array(
					'type'		=> 'image',
					'tag' 	=> 'p',
					'name'	=> 'image',
					'value'	=> ''
				),
				array(
					'type'			=> 'text',
					'tag'  			=> 'p',
					'name' 			=> 'title',
					'value'			=> '',
					'placeholder'	=> 'Product Title',
					'class'			=> 'large-text'
				),
				array(
					'type'			=> 'textarea',
					'tag'  			=> 'p',
					'name' 			=> 'subtitle',
					'value'			=> '',
					'placeholder'	=> 'Product Sub Title',
					'class'			=> 'large-text'
				),
				array(
					'type'			=> 'select',
					'tag'  			=> 'p',
					'name' 			=> 'brand',
					'data'			=> array('type' =>'taxonomy', 'name'=>'brand'),
					'value'			=> '',
					'placeholder'	=> 'Product Brand',
					'class'			=> ''
				)
			);
         */

	    // Get Field Data
		$value = $this->getFieldValue($value, $name);
		//echo $value);
		// Build List
		
		echo '<div class="itemsPreview">';
			echo '<h3>'.$title.'</h3>';
			if($value == ''){
				echo "<input sptype='".__FUNCTION__."' class='itemsListData' name='_".$name."' type='hidden' value='".$value."' />";
				
				echo '<div class="itemsPreviewContainer">';
					echo '<article class="itemPreview">';
						// Build Fields by type
						foreach ($fields as $fieldValue) {
							switch ($fieldValue['type']) {
								case "image":
									$this->image($fieldValue['tag'], $fieldValue['name'], $fieldValue['value']);
									break;
								case "textarea":
									$this->textarea($fieldValue['tag'], $fieldValue['name'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
									break;
								case "text":
									$this->text($fieldValue['tag'], $fieldValue['name'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
									break;
								case "select":
									$this->select($fieldValue['tag'], $fieldValue['name'], $fieldValue['data'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
									break;
								case "selectPages":
									$this->selectPages($fieldValue['tag'], $fieldValue['name'], $fieldValue['data'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
									break;
								case "pdf":
									$this->pdf($fieldValue['tag'], $fieldValue['name'], $fieldValue['value']);
									break;
								default:
									echo '<p><u>'.$fieldKey.'</u> is invalid field.</p>';
							}
						}
						echo '<button class="itemRemove">Remove</button>';
					echo '</article>';
				echo '</div>';
			} else {
				echo "<input sptype='".__FUNCTION__."' class='itemsListData' name='_".$name."' type='hidden' value='".base64_encode($value)."' />";
				$value = json_decode($value, true);
				echo '<div class="itemsPreviewContainer">';
					foreach ($value as $items) {
						echo '<article class="itemPreview">';
							foreach ($items as $itemFieldIndex => $itemFieldValue) {
								$fieldArgs = $fields[$itemFieldIndex];
								switch ($itemFieldValue['type']) {
									case "image":
										$this->image($fieldArgs['tag'], $fieldArgs['name'], $itemFieldValue['value']);
										break;
									case "textarea":
										$this->textarea($fieldArgs['tag'], $fieldArgs['name'], $itemFieldValue['value'], $fieldArgs['placeholder'], $fieldArgs['class']);
										break;
									case "text":
										$this->text($fieldArgs['tag'], $fieldArgs['name'], $itemFieldValue['value'], $fieldArgs['placeholder'], $fieldArgs['class']);
										break;
									case "select":
										$this->select($fieldArgs['tag'], $fieldArgs['name'], $fieldArgs['data'], $itemFieldValue['value'], $fieldArgs['placeholder'], $fieldArgs['class']);
										break;
									case "selectPages":
										$this->selectPages($fieldArgs['tag'], $fieldArgs['name'], $fieldArgs['data'], $itemFieldValue['value'], $fieldArgs['placeholder'], $fieldArgs['class']);
										break;
									case "pdf":
										$this->pdf($fieldArgs['tag'], $fieldArgs['name'], $itemFieldValue['value']);
										break;
									default:
										echo '<p><u>'.$field['type'].'</u> is invalid field.</p>';
								}
							}
							echo '<button class="itemRemove">Remove</button>';
						echo '</article>';
					}
				echo '</div>';
			}
			echo '<button class="addItem">+ Add New</button>';
			echo '<div class="clear"></div>';
		echo '</div>';
	}

	public function line(){
		echo '<hr class="separateur">';
	}

	public function metaboxFieldsBuilder($id, $fields){
		/*
			if('value' => 'getIt') get stored field value
			
			$FieldsSchema = array(
				array(
					'type' => 'text',
					'tag' => 'p',
					'name' => 'title',
					'value' => null,
					'placeholder' => 'Title',
					'class' => 'large-text',
					),
				array(
					'type' => 'textarea',
					'tag' => 'p',
					'name' => 'subtitle',
					'value' => null,
					'placeholder' => 'Sub Title',
					'class' => 'large-text',
					),
				array( 
					'type' => 'image',
					'tag' => 'p',
					'name' => 'cover',
					'value' => null,
					),
				array(
					'type'			=> 'select',
					'tag'  			=> 'p',
					'name' 			=> 'brand',
					'data'			=> array('type' =>'taxonomy', 'name'=>'brand'),
					'value'			=> null,
					'placeholder'	=> 'Product Brand',
					'class'			=> '',
				),
				array(
					'type' => 'listItems',
					'fields' => $listItemsFields,
					'name'	=> 'products',
					'value'	=> null,
					'title' => 'Products List',
					),
			);
		*/

		// Set Variables
		$fieldsNamePrefix = substr($id, 0, -6);

		// Build Metabox
		echo '<div class="inside introSection">';
			// Create Nonce Input for security
			wp_nonce_field( $fieldsNamePrefix, $fieldsNamePrefix.'_nonce' );

			// Build Fields
			foreach ($fields as $fieldValue) {
				switch ($fieldValue['type']) {
					case "image":
						$this->image($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value']);
						break;
					case "pdf":
						$this->pdf($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value']);
						break;
					case "textarea":
						$this->textarea($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
						break;
					case "text":
						$this->text($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
						break;
					case "select":
						$this->select($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['data'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
						break;
					case "selectPages":
						$this->selectPages($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['data'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
						break;	
					case "checkbox":
						$this->checkbox($fieldValue['tag'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value'], $fieldValue['placeholder'], $fieldValue['class']);
						break;
					case "listItems":
						$this->listItems($fieldValue['fields'], $fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value'], $fieldValue['title']);
						break;
					case "editor":
						$this->editor($fieldsNamePrefix.'_'.$fieldValue['name'], $fieldValue['value']);
						break;
					case "line":
						$this->line();
						break;
					default:
						echo '<p><u>'.$fieldValue['type'].'</u> is invalid field.</p>';
				}
			}
		echo '</div>';
	}

	public function metaboxBuilder($functionName){
		// Get Metabox Fields
		$metaboxFields = call_user_func(substr($functionName, 0, -6).'_fields');

		// Build Metabox
		$this->metaboxFieldsBuilder($functionName, $metaboxFields);
	}

	public function metaboxSave($functionName){
		$fieldsNamePrefix = substr($functionName, 0, -5);

		// Verify metabox nonce
		if ( !isset( $_POST[$fieldsNamePrefix.'_nonce'] ) || !wp_verify_nonce( $_POST[$fieldsNamePrefix.'_nonce'], $fieldsNamePrefix ) ){
			return;
		}

		// return if autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
			return;
		}
		
		// Check the user's permissions.
		if ( ! current_user_can( 'edit_post', $this->postId ) ){
			return;
		}

		// Get Metabox Fields
		$metaboxFields = $fieldsNamePrefix.'_fields';
		$metaboxFields = call_user_func($metaboxFields);

		// Store Metabox Fields values
		foreach ($metaboxFields as $field) {

			// Set Field Name
			$fieldName = '_'.$fieldsNamePrefix.'_'.$field['name'];
			
			// Check if not null
			if( isset( $_POST[$fieldName]) ){
				//Save Data
				update_post_meta($this->postId, $fieldName, $_POST[$fieldName]); 
			}else{
				// Delete data
				delete_post_meta( $this->postId, $fieldName );
			}
		}
	}



}