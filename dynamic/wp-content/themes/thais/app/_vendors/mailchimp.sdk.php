<?php

namespace mailchimp;

use Httpful\Request;

class MailchimpSDK{
	// API URL
	private $api_key = '64cfe5a9081566a161c80a27173eb4fb-us10';

	// API List
	public $api_list_id = '02f4c13505';

	// API URL
	public  $api_url = 'https://us10.api.mailchimp.com/3.0';

	// API embers List URL
	private $api_url_members_list;

	// HTTP Client
	private $MailchimpClient;


	function __construct(){
		// Import Vendors
		require_once('httpful.phar');

		// Store $api_url_member_list value
		$this->api_url_members_list = $this->api_url.'/lists/'.$this->api_list_id.'/members/';

		// Set HTTP Client
		$this->MailchimpClient = Request::init()
			->sendsJson()
			->authenticateWith('username', $this->api_key);
		Request::ini($this->MailchimpClient);
	}

	// Add a new subscriber
	public function AddNewSubscriber($subscriber_infos){
		// Encode Subscriber Infos
		$subscriber_infos = json_encode($subscriber_infos);

		// Post Request
		$add_subscriber = Request::post($this->api_url_members_list)
			->body($subscriber_infos)
			->send();

		// Return response
		return($add_subscriber);
	}

	// Update subscriber infos
	public function UpdateNewSubscriber($subscriber_infos){
		// Generate user Hash
		$subscriber_hash = md5($subscriber_infos['email_address']);

		// Encode Subscriber Infos
		$subscriber_infos = json_encode($subscriber_infos);

		// Put Request
		$update_subscriber = Request::put($this->api_url_members_list . $subscriber_hash)
			->body($subscriber_infos)
			->send();

		// Return response
		return($update_subscriber);
	}

	// Check if subscriber exist
	public function CheckSubscriberExist($subscriber_email){
		// Generate user Hash
		$subscriber_hash = md5($subscriber_email);

		// Get Request
		$check_subscriber = Request::get($this->api_url_members_list . $subscriber_hash)
			->send();

		// Return response
		return($check_subscriber);
	}
}

?>





<?php






/* How to use : Exemple
$add_subscriber_infos = array(
	"email_address" => "hamza@houssam.net", 
	"status" => "subscribed",
	"merge_fields" => array(
		"FNAME" => "Houssam",
		"LNAME" => "Hamza",
		"DOB" => "03/12/1299",
		"SEX" => "M"
		),
	"interests" => array(
		"5a011e8a54" => true,
		"c9ff45afea" => true,
		"877092df85" => true,
		"e61c93ceda" => true,
		"51afb8c166"=> true
		)
	);

$hamza = new MailchimpSDK();
$houssam = $hamza->CheckSubscriberExist($add_subscriber_infos);
echo $houssam;

*/
?>