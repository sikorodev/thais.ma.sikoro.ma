<?php
 
class formNonce {
    //Store the generated nonce
    private $nonce;
     
    //Store the old nonce
    private $old_nonce;

    //Store the form nonce name
    private $form_nonce_name;

    //The constructor stores the nonce (if one excists) in our class variable
    function __construct($form_name){
        //Get the nonce form name and store it inside the class
        $this->form_nonce_name = 'form_'.$form_name.'_nonce';

        //We need the previous nonce so we store it
        if(isset($_SESSION[$this->form_nonce_name]))
        {
            $this->old_nonce = $_SESSION[$this->form_nonce_name];
        }
    }
 
    //Function to generate the nonce
    private function generateNonce(){
        //User IP-address
        $ip = $_SERVER['REMOTE_ADDR'];
         
        //Generating random numbers

        $uniqid = uniqid(mt_rand(), true);
         
        //Return the hash
        return md5($ip . $uniqid);
    }
 
     
    //Function to output the form nonce input
    public function outputNonce(){
        //Generate the nonce and store it inside the class
        $this->nonce = $this->generateNonce();
        //Store the form nonce in the session
        $_SESSION[$this->form_nonce_name] = $this->nonce;
         
        //Output the form nonce input
        echo "<input type='hidden' name='".$this->form_nonce_name."' id='".$this->form_nonce_name."' value='".$this->nonce."' />";
    }
 
     
    //Function that validated the form nonce POST data
    public function validate(){
        //We use the old nonce and not the new generated version
        if(@isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && $_POST[$this->form_nonce_name] == $this->old_nonce && $_SERVER['HTTP_REFERER'] == "http://luxyne.dev/Landing Page/" ){
            //The nonce is valid, return true.
            return true;
        } else {
            //The nonce is invalid, return false.
            return false;
        }
    }


}


?>