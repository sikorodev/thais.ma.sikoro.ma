<?php

/**
 *
 */
function conferencePosttype() {
    $labels = array(
        'name'                  => 'Conference',
        'singular_name'         => 'Conference',
        'menu_name'             => 'Conference',
        'name_admin_bar'        => 'Conference',
        'add_new'               => 'Ajouter une nouvelle',
        'add_new_item'          => 'Ajouter un nouvel Intevenant',
        'new_item'              => 'Nouvelle Intervenant',
        'edit_item'             => 'Editer un Intervenant',
        'view_item'             => 'Visualiser Interventn',
        'all_items'             => 'Liste des Intervenants',
        'search_items'          => 'Chercher Intervenant',
        'parent_item_colon'     => 'Parent Intervenant',
        'not_found'             => 'Aucune Intervenant Trouvé',
        'not_found_in_trash'    => 'Aucune Intervenant Trouvé.',
        'featured_image'        => 'Intervenant cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Intervenant archives',
        'insert_into_item'      => 'Inserer dans le Intervenant',
        'uploaded_to_this_item' => 'Uploader dans le Intervenant',
        'filter_items_list'     => 'Filtrer la liste des Intervenants',
        'items_list_navigation' => 'Liste Navigation des Intervenants',
        'items_list'            => 'Liste des Intervenants',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-welcome-widgets-menus',
		'supports'            => array( 'title','editor', 'excerpt', 'thumbnail' ),
        'has_archive'        => false,
        'rewrite'            => array( 'slug' => 'conference', 'with_front' => false ),
        //
        'public'              => true,
        'exclude_from_search' => false, 
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => true,
        'register_meta_box_cb'=> null,
    );
 
    register_post_type( 'conference', $args );
}

add_action( 'init', 'conferencePosttype');

?>