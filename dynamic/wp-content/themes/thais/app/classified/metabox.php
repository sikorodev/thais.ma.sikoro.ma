<?php

use framework\metaboxFieldsBuilder as fieldBuilder;


// Set Variables
$metaboxesPosttype = 'conference';

/**
 * Add Metaboxes
 */

function conferenceAddMetaboxes( $post ){
	// Set Metaboxes as Array
	$metaboxes = array(
					array(
							'id' 		=> 'Introconference',
							'title' 	=> 'Infos conference',
						)
				 );

	// Register Metaboxes
	foreach ($metaboxes as $metabox) {
		add_meta_box( 'conferenceAddMetaboxes'.$metabox['id'], $metabox['title'], 'conferenceAddMetaboxes'.$metabox['id'].'_build', $metaboxesPosttype, 'normal', 'high' );
	}









}
add_action( 'add_meta_boxes_'.$metaboxesPosttype, 'conferenceAddMetaboxes' );



/**
 * Build Metaboxe : Mettre en Avant
 */

function conferenceAddMetaboxesEnAvant($post){

	$enAvant = get_post_meta( get_the_ID(), '_enavant', true );
	$checked = ($enAvant) ? 'checked': NULL ;
	echo '<input type="checkbox" name="_enavant" value="1" '.$checked.'>';
}

function conferenceAddMetaboxesEnAvant_save( $post_id ){
	// Check if not null
	if( isset( $_POST['_enavant']) ){
		//Save Data
		update_post_meta(get_the_ID(), '_enavant', $_POST['_enavant']); 
	}else{
		// Delete data
		delete_post_meta( get_the_ID(), '_enavant' );
	}
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesEnAvant_save' );


function conferenceAddMetaboxesIntroconference_fields(){
	$metaboxFields = array(
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionconference',
			'value'			=> 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Intro Inervenant',
			'class' => 'large-text'
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'Type',
			'data'			=> array('type' =>'array', 'data'=>array('animer'=>'animée par',"Introduction"=>"Introduction","paroleExpert"=>"Paroles d’experts","paroleEntreprise"=>"Paroles d’entreprises","paroleStartUp"=>"Paroles de start up","Conclusion"=>"Conclusion","Cloture"=>"Clôture")),
			'value'			=> 'getIt',
			'placeholder'	=> 'Type',
			'class'			=> '',
		)
	);

	return $metaboxFields;
}
function conferenceAddMetaboxesIntroconference_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntroconference_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntroconference_save' );

function conferenceAddMetaboxesIntervenantsCloture_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosCloture',
			'value'			=> 'getIt',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameCloture',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionCloture',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionCloture',
			'value'			=> 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Description',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function conferenceAddMetaboxesIntervenantsCloture_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntervenantsCloture_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntervenantsCloture_save' );

function conferenceAddMetaboxesIntervenantsConclusion_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosConclusion',
			'value'			=> 'getIt',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionConclusion',
			'value'			=> 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Description',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function conferenceAddMetaboxesIntervenantsConclusion_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntervenantsConclusion_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntervenantsConclusion_save' );

function conferenceAddMetaboxesIntervenantsParoleStart_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosStartVisu',
			'value'			=> 'getIt',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameStart',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionStart',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionStart',
			'value' => 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Description',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photosStart',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function conferenceAddMetaboxesIntervenantsParoleStart_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntervenantsParoleStart_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntervenantsParoleStart_save' );

function conferenceAddMetaboxesIntervenantsParoleEntreprise_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosEntrepriseVisu',
			'value'			=> 'getIt',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameEntreprise',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionEntreprise',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionEntreprise',
			'value' => 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Description',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photosEntreprise',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function conferenceAddMetaboxesIntervenantsParoleEntreprise_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntervenantsParoleEntreprise_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntervenantsParoleEntreprise_save' );

function conferenceAddMetaboxesIntervenantsParoleExpert_fields(){
	$metaboxListItemsField = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'section',
			'name'	=> 'exprtVisu',
			'value'			=> 'getIt'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExprtVisu',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprtVisu',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExprtVisu',
			'tag'  			=> 'section',
			'placeholder'	=> 'Description',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);
	$metaboxField = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsField,
			'name'	=> 'paroleExpertListe',
			'value' => 'getIt',
			'title' => '',
			),
	);

	return $metaboxField;
}
function conferenceAddMetaboxesIntervenantsParoleExpert_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntervenantsParoleExpert_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntervenantsParoleExpert_save' );
function conferenceAddMetaboxesIntervenantsIntroduction_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photos',
			'value'			=> 'getIt',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'name',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonction',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'description',
			'value'			=> 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Description',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function conferenceAddMetaboxesIntervenantsIntroduction_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesIntervenantsIntroduction_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesIntervenantsIntroduction_save' );
function conferenceAddMetaboxesAnimerPar_fields(){

	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosAnim',
			'value'			=> 'getIt',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionAnim',
			'value'			=> 'getIt',
			'tag'  			=> 'p',
			'placeholder'	=> 'Description',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}

function conferenceAddMetaboxesAnimerPar_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesAnimerPar_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesAnimerPar_save' );



/**
 * Build Metaboxe : Photos du Produit
 */

function conferenceAddMetaboxesMesuresPlan_fields(){

	// Prepare Metabox Fields
	$metaboxListItemsFieldss = array(
		array(
			'type'		=> 'pdf',
			'tag' 	=> 'div',
			'name'	=> 'pdf',
			'value'	=> null
		),
	);



	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'hauteur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Hauteur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'largeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Largeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'profondeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Profondeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'longueur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Longueur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'piecesDispo',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pieces Disponible',
			'class'			=> ''
		),
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFieldss,
			'name'	=> 'pdfs',
			'value'	=> 'getIt',
			'title' => 'Plans PDFs',
			),
	);

	return $metaboxFields;
}

function conferenceAddMetaboxesMesuresPlan_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesMesuresPlan_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesMesuresPlan_save' );



/**
 * Build Metaboxe : Description du Produit
 */

function conferenceAddMetaboxesDescription_fields(){

	$metaboxFields = array(
		array(
			'type' => 'textarea',
			'name' => 'description',
			'value' => 'getIt',
			'class' => 'large-text'
			),
		array('type'=>'line'),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'etat',
			'data'			=> array('type' =>'array', 'data'=>array('Neuf'=>'Neuf',"Exposition"=>"d'exposition")),
			'value'			=> 'getIt',
			'placeholder'	=> 'Etat',
			'class'			=> '',
		),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'coloris',
			'data'			=> array('type' =>'taxonomy', 'name'=>'coloris'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coloris',
			'class'			=> '',
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'finition',
			'data'			=> array('type' =>'taxonomy', 'name'=>'finition'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Finition',
			'class'			=> '',
		),
		array(
			'type'			=> 'checkbox',
			'tag'  			=> 'p',
			'name' 			=> 'typeDisplayHome',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coup de coeur',
			'class'			=> '',
		),
	);

	return $metaboxFields;
}

function conferenceAddMetaboxesDescription_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesDescription_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesDescription_save' );



/**
 * Build Metaboxe : Prix du Produit
 */

function conferenceAddMetaboxesPrix_fields(){

	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbase',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix de base',
			'class'			=> ''
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'prixRemiseType',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Remise',
			'class'			=> '',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbrade',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix Bradé',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixpercent',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pourcentage %',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}

function conferenceAddMetaboxesPrix_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function conferenceAddMetaboxesPrix_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'conferenceAddMetaboxesPrix_save' );