<?php

/*
error_reporting(E_ALL);
ini_set('display_errors', 'on');
*/


add_action( 'wp_ajax_searchPostAjax', 'searchPostAjax' );
add_action( 'wp_ajax_nopriv_searchPostAjax', 'searchPostAjax' );

function searchPostAjax(){

	global $wpdb; // this is how you get access to the database
	
	// Create Result Variable to return
	$result=array();


	// Get Variables From Post
	$query = json_decode(stripslashes($_POST['query']), true);







	// Query Arguments
    $args = array(
                    'post_status' => 'publish',
                    'post_type' => 'annonce',
                    'posts_per_page' => 3,
                    'order' => "ASC",
                    'orderby' => "date",
                    'meta_key'   => '_enavant',
                    'meta_value' => '1'
                );


    // Add Category | SubCategories to Query

    
	if( $query['subcat'] ){
		(is_array($query['subcat']))? $args['cat'] = implode(",",$query['subcat']) : $args['cat'] = $query['subcat'];
	} else if($query['cat']){
		$args['cat'] = $query['cat'];
	}



    $postsExclude = array();


    // QUERY
    $query1 = new WP_Query( $args );

    if ( $query1->have_posts() ) {

        // The Loop
        while ( $query1->have_posts() ) {

            $query1->the_post();

            $postsExclude[] = get_the_ID();

            // Build Annonce Excerpt
           // buildAnnonceExcerpt(get_the_ID(), get_the_title(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true), get_permalink(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ), 4, 'ads-bloc');
        }  
        
        //Restore original Post Data 
        wp_reset_postdata(); 
    }























	// Query Arguments
	$args = array(
					'post_status' => 'publish',
					'post_type' => 'annonce',
					'posts_per_page' => 9,
					'meta_query' => array(
						'relation' => 'AND',
						array(
							'key'     => '_annonceAddMetaboxesPrix_prixbase',
							'value'   => array( (int)str_replace(' ','',$query['priceMin']), (int)str_replace(' ','',$query['prixMax']) ),
							'type'    => 'numeric',
							'compare' => 'BETWEEN',
						),
					),
				);

	// Add Category | SubCategories to Query
	if( $query['subcat'] ){
		(is_array($query['subcat']))? $args['cat'] = implode(",",$query['subcat']) : $args['cat'] = $query['subcat'];
	} else if($query['cat']){
		$args['cat'] = $query['cat'];
	}


	// Add Offset To Query (Load More)
	if($query['offset']){
		$args['offset'] = $query['offset'];
	}


	// Add Order To Query
	if($query['ordre']){
		$args['order'] = $query['ordre'];
		$args['orderby'] = "date";
	}


	// Add Keyword To Query
	if($query['keyword']){
		$args['s'] = $query['keyword'];
	}


	// Add Magasin To Query
	if($query['magasin']){
		(is_array($query['magasin']))? $args['author'] = implode(",",$query['magasin']) : $args['author'] = $query['magasin'];
	}


	// Add Colors To Query
	if($query['color']){
		if(is_array($query['color'])){
			$colorArg = array('relation'=> 'OR');

			foreach ($query['color'] as $color) {
				array_push($colorArg,array('key'=>'_annonceAddMetaboxesDescription_coloris','value'=>$color,'compare'=>'='));
			}

			array_push($args['meta_query'],$colorArg);

		} else {
			array_push($args['meta_query'],array('key'=>'_annonceAddMetaboxesDescription_coloris','value'=>$query['color'],'compare'=>'='));
		}
	}


	// Add Finition To Query
	if($query['finition']){
		if(is_array($query['finition'])){
			$finitionArg = array('relation'=> 'OR');

			foreach ($query['finition'] as $finition) {
				array_push($finitionArg,array('key'=>'_annonceAddMetaboxesDescription_finition','value'=>$finition,'compare'=>'='));
			}

			array_push($args['meta_query'],$finitionArg);

		} else {
			array_push($args['meta_query'],array('key'=>'_annonceAddMetaboxesDescription_finition','value'=>$query['finition'],'compare'=>'='));
		}
	}
	
	// Add Brand To Query
	
	if($query['marques']){
		if(is_array($query['marques'])){
			$marqueArg = array('relation'=> 'OR');

			foreach ($query['marques'] as $marque) {
				array_push($marqueArg,array('taxonomy'=>'brand','terms'=>$marque,'field'=>'term_id', 'operator'=>'='));
			}/*

			$arrayM = array();
			$args['tax_query']=array();
			foreach ($query['marques'] as $marque) {
				array_push($arrayM,$marque);
			}
			array_push($marqueArg,array('taxonomy'=>'brand','field'=>'term_id','terms'=>$query['marques'],'operator' => 'IN'));

			*/
			array_push($args['tax_query'],$marqueArg);

		} else {
			$arrayM =$query['marques'];
			$args['tax_query']=array();
			//$marqueArg = array('relation'=> 'AND');
			//array_push($marqueArg,array('taxonomy'=>'brand','terms'=>$arrayM,'field'=>'term_id'));
			//array_push($args['tax_query'],$marqueArg);
			array_push($args['tax_query'],array('taxonomy'=>'brand','terms'=>(int)$arrayM,'field'=>'term_id','operator' => 'IN'));
		}
	}
	
	if($query['marques_intuitive']){
				$arrayMF =$query['marques_intuitive'];
				$args['tax_query']=array();
				//$marqueArg = array('relation'=> 'AND');
				//array_push($marqueArg,array('taxonomy'=>'brand','terms'=>$arrayM,'field'=>'term_id'));
				//array_push($args['tax_query'],$marqueArg);
				array_push($args['tax_query'],array('taxonomy'=>'brand','terms'=>$arrayMF,'field'=>'name','operator' => 'IN'));
		}

	

	// QUERY
	
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {

		// The Loop
		
		while ( $query->have_posts() ) {
			$query->the_post();

			// Build Annonce Excerpt
			buildAnnonceExcerpt(get_the_ID(), get_the_title(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesDescription_description',true), get_permalink(), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbase',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPrix_prixbrade',true ), get_post_meta(get_the_ID(),'_annonceAddMetaboxesPhotos_photos',true ), 4);
			/*
		    echo '<div class="gr-4 gr-12@mobile"><div class="bloc-offer ads-bloc js-cta-href">';
				echo '<a href="#"><div class="owl-carousel owl-theme">';
					echo '<div class="item"><img src="uploads/thumbs/thumb-listing-01.jpg" alt=""></div>';
				echo '</div></a>';
				echo '<span class="price">350,00 €</span>';
				echo '<h2><a href="">'.get_the_title().'</a></h2>';
				echo '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>';
				echo '<div class="cta-offer"><a href="" class="cta-more">Détails</a><a href="#" class="cta-add-favoris"><i class="fa fa-heart-o"></i></a> </div>';
            echo '</div></div>';*/
		}
		
		//Restore original Post Data 
		wp_reset_postdata(); 
	} else {
		echo 'Aucune annonce trouvée';
	}


	wp_die(); // this is required to terminate immediately and return a proper response

}