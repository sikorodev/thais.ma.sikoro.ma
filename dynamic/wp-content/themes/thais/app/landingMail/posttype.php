<?php

/**
 *
 */
function landingmaillistPosttype() {
    $labels = array(
        'name'                  => 'Landing Mail List',
        'singular_name'         => 'Landing Mail List',
        'menu_name'             => 'Landing Mail List',
        'name_admin_bar'        => 'Landing Mail List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau Landing Mail',
        'new_item'              => 'Nouveau Landing Mail',
        'edit_item'             => 'Editer Landing vMail',
        'view_item'             => 'Visualiser Landing Mail',
        'all_items'             => 'Tous les Landing Mails',
        'search_items'          => 'Chercher Landing Mail',
        'parent_item_colon'     => 'Parent Landing Mail',
        'not_found'             => 'Aucun Landing Mail Trouvé',
        'not_found_in_trash'    => 'Aucun Landing Mail Trouvé.',
        'featured_image'        => 'Mail cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Landing Mails archives',
        'insert_into_item'      => 'Inserer dans Landing Mail',
        'uploaded_to_this_item' => 'Uploader dans Landing Mail',
        'filter_items_list'     => 'Filtrer la liste des Landing Mails',
        'items_list_navigation' => 'Liste Navigation des Landing Mail',
        'items_list'            => 'Liste des Landing Mails',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-email',
        'supports'            => array( 'title'),
        'has_archive'        => false,
        'rewrite'            => false,
        //
        'public'              => false,
        'exclude_from_search' => true, 
        'publicly_queryable'  => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => false,
    );
 
    register_post_type( 'landingmaillist', $args );
}

add_action( 'init', 'landingmaillistPosttype');

?>