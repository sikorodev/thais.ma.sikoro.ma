<?php

add_action( 'wp_ajax_nopriv_addSubscriberLanding', 'addSubscriberLanding' );
add_action( 'wp_ajax_addSubscriberLanding', 'addSubscriberLanding' );

function addSubscriberLanding() {
	global $wpdb; // this is how you get access to the database

	// Get Variables From Post
	$nom = $_POST['nom'];
	$prenom = $_POST['prenom'];
	$societe = $_POST['societe'];
	$postocuppe = $_POST['postocuppe'];
	$email = $_POST['email'];
	$cgu = $_POST['cgu'];
	

	// If email is valid
	if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

		// Query Arguments
		$args = array(
					'post_status' => 'publish',
					'post_type'	  => 'landingmaillist',
					's'			  => $email
				);

		$query = new WP_Query( $args );

		// Exist Subscriber
		if ( $query->have_posts() ) {
			// Create Reponse Result
			$result=array(
				'status'=>'exist',
				'msg' => 'l\'adresse email que vous avez entrée existe déjà',
			);

			// Return Json Response
			echo json_encode($result, true);

		} else {
			// Args
			$args = array('post_title'=>$email,'post_status'=>'publish','post_type'=>'landingmaillist','meta_input'   => array(
        		'_landingmaillistAddMetaboxesFormulaire_nameLanding' => $nom,
				'_landingmaillistAddMetaboxesFormulaire_prenomLanding' => $prenom,
				'_landingmaillistAddMetaboxesFormulaire_societeLanding' => $societe,
				'_landingmaillistAddMetaboxesFormulaire_posteLanding' => $postocuppe,
				'_landingmaillistAddMetaboxesFormulaire_emailLanding' => $email,
				'_landingmaillistAddMetaboxesFormulaire_cguLanding' => $cgu
    		));

			// Register Subscriber
			wp_insert_post($args);

			// Create Reponse Result
			$result=array(
				'status'=>'added',
				'msg' => "Votre inscription a bien été prise en compte. Vous recevrez votre bulletin téléchargeable très rapidement à l’adresse indiquée.",
			);

			// Return Json Response
			echo json_encode($result, true);
		}

	// If email Not Valid
	} else {
		// Create Reponse Result
		$result=array(
			'status'=>'error',
			'msg' => '',
		);

		// Return Json Response
		echo json_encode($result, true);
	}

	
	wp_reset_postdata(); // Restore original Post Data 
	wp_die(); // this is required to terminate immediately and return a proper response

}