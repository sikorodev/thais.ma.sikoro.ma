<?php

// Create taxonomy, brands
function villesTaxonomy() {

	$labels = array(
		'name'                       => 'Villes',
		'singular_name'              => 'Ville',
		'menu_name'                  => 'Villes',
		'all_items'                  => 'Toutes les Villes',
		'edit_item'                  => 'Editer Ville',
		'view_item'					 => 'Visualiser Ville',
		'update_item'                => 'Mettre à jour Ville',
		'add_new_item'               => 'Ajouter Ville',
		'new_item_name'              => 'Nom nouvelle Ville',
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'search_items'               => 'Chercher Ville',
		'popular_items'              => 'Villes populaires',
		'separate_items_with_commas' => 'Separer par une virgule',
		'add_or_remove_items'        => 'Ajouter ou Supprimer Ville',
		'choose_from_most_used'      => 'Choisir parmis les plus utilisés',
		'not_found'                  => 'Colori introuvable.',
	);

	$args = array(
		'labels'                => $labels,
		'public'				=> true,
		'show_ui'               => true,
		'show_in_nav_menus'		=> true,
		'show_tagcloud'			=> false,
		'show_in_quick_edit'	=> true,
		'show_admin_column'     => true,
		'hierarchical'          => false,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => false,
		'rewrite'               => false
		//
	);

	register_taxonomy( 'villes', null, $args );
}

// hook into the init action and call brandTaxonomy when it fires
add_action( 'init', 'villesTaxonomy', 0 );