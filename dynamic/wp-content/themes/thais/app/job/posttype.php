<?php

/**
 *
 */
function carrierePosttype() {
    $labels = array(
        'name'                  => 'Carriere',
        'singular_name'         => 'Carriere List',
        'menu_name'             => 'Carriere',
        'name_admin_bar'        => 'Carriere List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau Carriere',
        'new_item'              => 'Nouveau Carriere',
        'edit_item'             => 'Editer Carriere',
        'view_item'             => 'Visualiser Carriere',
        'all_items'             => 'Liste des Carriere',
        'search_items'          => 'Chercher Carriere',
        'parent_item_colon'     => 'Parent Carriere',
        'not_found'             => 'Aucun Carriere Trouvé',
        'not_found_in_trash'    => 'Aucun Carriere Trouvé.',
        'featured_image'        => 'Team Carriere image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Carriere archives',
        'insert_into_item'      => 'Inserer dans Carriere',
        'uploaded_to_this_item' => 'Uploader dans Carriere',
        'filter_items_list'     => 'Filtrer la liste des Carriere',
        'items_list_navigation' => 'Liste Navigation des Carriere',
        'items_list'            => 'Liste des Carriere',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'=>  27,
        'menu_icon'           => 'dashicons-groups',
        'supports'            => array( 'title', 'thumbnail', 'excerpt','editor' ),
        'has_archive'        => false,
		'rewrite'            => array( 'slug' => 'carriere', 'with_front' => false ),
        //
         'public'              => true,
        'exclude_from_search' => false, 
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => true,
        'register_meta_box_cb'=> null,
		'taxonomies'          => array('villes'),
    );
 
    register_post_type( 'carriere', $args );
}

add_action( 'init', 'carrierePosttype');

?>