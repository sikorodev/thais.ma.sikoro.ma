<?php

use framework\metaboxFieldsBuilder as fieldBuilder;


// Set Variables
$metaboxesPosttype = 'carriere';

/**
 * Add Metaboxes
 */

function carriereAddMetaboxes( $post ){
	// Set Metaboxes as Array
	$metaboxes = array(
					array(
							'id' 		=> 'infoscarriere',
							'title' 	=> 'Infos Carriére',
						)
				 );

	// Register Metaboxes
	foreach ($metaboxes as $metabox) {
		add_meta_box( 'carriereAddMetaboxes'.$metabox['id'], $metabox['title'], 'carriereAddMetaboxes'.$metabox['id'].'_build', $metaboxesPosttype, 'normal', 'high' );
	}









}
add_action( 'add_meta_boxes_'.$metaboxesPosttype, 'carriereAddMetaboxes' );



/**
 * Build Metaboxe : Mettre en Avant
 */

function carriereAddMetaboxesEnAvant($post){

	$enAvant = get_post_meta( get_the_ID(), '_enavant', true );
	$checked = ($enAvant) ? 'checked': NULL ;
	echo '<input type="checkbox" name="_enavant" value="1" '.$checked.'>';
}

function carriereAddMetaboxesEnAvant_save( $post_id ){
	// Check if not null
	if( isset( $_POST['_enavant']) ){
		//Save Data
		update_post_meta(get_the_ID(), '_enavant', $_POST['_enavant']); 
	}else{
		// Delete data
		delete_post_meta( get_the_ID(), '_enavant' );
	}
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesEnAvant_save' );


function carriereAddMetaboxesinfoscarriere_fields(){
	$metaboxFields = array(
		array(
			'type' => 'textarea',
			'name' => 'profile',
			'value' => 'getIt',
			'class' => 'large-text',
			'placeholder'	=> 'Profil',
		),
		array(
			'type'			=> 'checkbox',
			'tag'  			=> 'p',
			'name' 			=> 'isActive',
			'data'			=> array('type' =>'array', 'data'=>array('Oui'=>'Oui','Non'=>'Non')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Active',
			'class'			=> '',
		),
	);

	return $metaboxFields;
}
function carriereAddMetaboxesinfoscarriere_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesinfoscarriere_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesinfoscarriere_save' );

function carriereAddMetaboxesIntervenantsConclusion_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosConclusion',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionConclusion',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function carriereAddMetaboxesIntervenantsConclusion_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesIntervenantsConclusion_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesIntervenantsConclusion_save' );

function carriereAddMetaboxesIntervenantsParoleStart_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function carriereAddMetaboxesIntervenantsParoleStart_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesIntervenantsParoleStart_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesIntervenantsParoleStart_save' );

function carriereAddMetaboxesIntervenantsParoleEntreprise_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function carriereAddMetaboxesIntervenantsParoleEntreprise_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesIntervenantsParoleEntreprise_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesIntervenantsParoleEntreprise_save' );

function carriereAddMetaboxesIntervenantsParoleExpert_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function carriereAddMetaboxesIntervenantsParoleExpert_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesIntervenantsParoleExpert_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesIntervenantsParoleExpert_save' );
function carriereAddMetaboxesIntervenantsIntroduction_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photos',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'name',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonction',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'description',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function carriereAddMetaboxesIntervenantsIntroduction_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesIntervenantsIntroduction_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesIntervenantsIntroduction_save' );
function carriereAddMetaboxesAnimerPar_fields(){

	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosAnim',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionAnimp',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}

function carriereAddMetaboxesAnimerPar_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesAnimerPar_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesAnimerPar_save' );



/**
 * Build Metaboxe : Photos du Produit
 */

function carriereAddMetaboxesMesuresPlan_fields(){

	// Prepare Metabox Fields
	$metaboxListItemsFieldss = array(
		array(
			'type'		=> 'pdf',
			'tag' 	=> 'div',
			'name'	=> 'pdf',
			'value'	=> null
		),
	);



	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'hauteur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Hauteur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'largeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Largeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'profondeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Profondeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'longueur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Longueur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'piecesDispo',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pieces Disponible',
			'class'			=> ''
		),
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFieldss,
			'name'	=> 'pdfs',
			'value'	=> 'getIt',
			'title' => 'Plans PDFs',
			),
	);

	return $metaboxFields;
}

function carriereAddMetaboxesMesuresPlan_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesMesuresPlan_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesMesuresPlan_save' );



/**
 * Build Metaboxe : Description du Produit
 */

function carriereAddMetaboxesDescription_fields(){

	$metaboxFields = array(
		array(
			'type' => 'textarea',
			'name' => 'description',
			'value' => 'getIt',
			'class' => 'large-text'
			),
		array('type'=>'line'),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'etat',
			'data'			=> array('type' =>'array', 'data'=>array('Neuf'=>'Neuf',"Exposition"=>"d'exposition")),
			'value'			=> 'getIt',
			'placeholder'	=> 'Etat',
			'class'			=> '',
		),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'coloris',
			'data'			=> array('type' =>'taxonomy', 'name'=>'coloris'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coloris',
			'class'			=> '',
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'finition',
			'data'			=> array('type' =>'taxonomy', 'name'=>'finition'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Finition',
			'class'			=> '',
		),
		array(
			'type'			=> 'checkbox',
			'tag'  			=> 'p',
			'name' 			=> 'typeDisplayHome',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coup de coeur',
			'class'			=> '',
		),
	);

	return $metaboxFields;
}

function carriereAddMetaboxesDescription_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesDescription_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesDescription_save' );



/**
 * Build Metaboxe : Prix du Produit
 */

function carriereAddMetaboxesPrix_fields(){

	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbase',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix de base',
			'class'			=> ''
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'prixRemiseType',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Remise',
			'class'			=> '',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbrade',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix Bradé',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixpercent',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pourcentage %',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}

function carriereAddMetaboxesPrix_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function carriereAddMetaboxesPrix_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'carriereAddMetaboxesPrix_save' );