<?php

/**
 *
 */
function sliderPosttype() {
    $labels = array(
        'name'                  => 'Slider',
        'singular_name'         => 'Slider',
        'menu_name'             => 'Slider',
        'name_admin_bar'        => 'slider',
        'add_new'               => 'Ajouter une nouvelle Slider',
        'add_new_item'          => 'Ajouter une nouvelle Slider',
        'new_item'              => 'Nouvelle Slider',
        'edit_item'             => 'Editer une Slider',
        'view_item'             => 'Visualiser Slider',
        'all_items'             => 'Liste des Slider',
        'search_items'          => 'Chercher Slider',
        'parent_item_colon'     => 'Parent Slider',
        'not_found'             => 'Aucune Slider Trouvé',
        'not_found_in_trash'    => 'Aucune Slider Trouvé.',
        'featured_image'        => 'Slider cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Slider archives',
        'insert_into_item'      => 'Inserer dans le Slider',
        'uploaded_to_this_item' => 'Uploader dans le Slider',
        'filter_items_list'     => 'Filtrer la liste des Slider',
        'items_list_navigation' => 'Liste Navigation des Slider',
        'items_list'            => 'Liste des Slider',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'       => null,
        'menu_icon'           => 'dashicons-welcome-widgets-menus',
		'supports' => array( 'title', 'thumbnail', 'excerpt', 'editor'),
        'has_archive'        => false,
        'rewrite'            => array( 'slug' => 'slider', 'with_front' => false ),
        //
        'public'              => true,
        'exclude_from_search' => false, 
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => true,
        'register_meta_box_cb'=> null,
    );
 
    register_post_type( 'slider', $args );
}

add_action( 'init', 'sliderPosttype');

?>