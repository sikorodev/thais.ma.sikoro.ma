<?php

/**
 *
 */
function clientPosttype() {
    $labels = array(
        'name'                  => 'Client',
        'singular_name'         => 'Clients List',
        'menu_name'             => 'Clients',
        'name_admin_bar'        => 'Clients List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau Clients',
        'new_item'              => 'Nouveau Client',
        'edit_item'             => 'Editer Client',
        'view_item'             => 'Visualiser Client',
        'all_items'             => 'Liste des Clients',
        'search_items'          => 'Chercher Client',
        'parent_item_colon'     => 'Parent Client',
        'not_found'             => 'Aucun Clients Trouvé',
        'not_found_in_trash'    => 'Aucun Clients Trouvé.',
        'featured_image'        => 'Client cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Clients archives',
        'insert_into_item'      => 'Inserer dans Client',
        'uploaded_to_this_item' => 'Uploader dans Client',
        'filter_items_list'     => 'Filtrer la liste des Clients',
        'items_list_navigation' => 'Liste Navigation des Clients',
        'items_list'            => 'Liste des Clients',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'=>  27,
        'menu_icon'           => 'dashicons-admin-multisite',
        'supports'            => array( 'title', 'thumbnail' ),
        'has_archive'        => false,
        'rewrite'            => false,
        //
        'public'              => false,
        'exclude_from_search' => true, 
        'publicly_queryable'  => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => false,
    );
 
    register_post_type( 'client', $args );
}

add_action( 'init', 'clientPosttype');

?>