<?php

/**
 *
 */
function galleryPosttype() {
    $labels = array(
        'name'                  => 'Gallery',
        'singular_name'         => 'Gallery List',
        'menu_name'             => 'Gallery',
        'name_admin_bar'        => 'Gallery List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau Gallery',
        'new_item'              => 'Nouveau Gallery',
        'edit_item'             => 'Editer Gallery',
        'view_item'             => 'Visualiser Gallery',
        'all_items'             => 'Liste des Gallery',
        'search_items'          => 'Chercher Gallery',
        'parent_item_colon'     => 'Parent Gallery',
        'not_found'             => 'Aucun Gallery Trouvé',
        'not_found_in_trash'    => 'Aucun Gallery Trouvé.',
        'featured_image'        => 'Gallery cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Gallery archives',
        'insert_into_item'      => 'Inserer dans Gallery',
        'uploaded_to_this_item' => 'Uploader dans Gallery',
        'filter_items_list'     => 'Filtrer la liste des Gallery',
        'items_list_navigation' => 'Liste Navigation des Gallery',
        'items_list'            => 'Liste des Gallery',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'=>  27,
        'menu_icon'           => 'dashicons-format-image',
        'supports'            => array( 'title', 'thumbnail' ),
        'has_archive'        => false,
        'rewrite'            => false,
        //
        'public'              => false,
        'exclude_from_search' => true, 
        'publicly_queryable'  => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => false,
    );
 
    register_post_type( 'gallery', $args );
}

add_action( 'init', 'galleryPosttype');

?>