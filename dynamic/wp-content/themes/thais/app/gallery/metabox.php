<?php

use framework\metaboxFieldsBuilder as fieldBuilder;


// Set Variables
$metaboxesPosttype = 'partenaire';

/**
 * Add Metaboxes
 */

function partenaireAddMetaboxes( $post ){
	// Set Metaboxes as Array
	$metaboxes = array(
					array(
							'id' 		=> 'lienPartenaire',
							'title' 	=> 'Lien Partenaire',
						)
				 );

	// Register Metaboxes
	foreach ($metaboxes as $metabox) {
		add_meta_box( 'partenaireAddMetaboxes'.$metabox['id'], $metabox['title'], 'partenaireAddMetaboxes'.$metabox['id'].'_build', $metaboxesPosttype, 'normal', 'high' );
	}









}
add_action( 'add_meta_boxes_'.$metaboxesPosttype, 'partenaireAddMetaboxes' );



/**
 * Build Metaboxe : Mettre en Avant
 */

function partenaireAddMetaboxesEnAvant($post){

	$enAvant = get_post_meta( get_the_ID(), '_enavant', true );
	$checked = ($enAvant) ? 'checked': NULL ;
	echo '<input type="checkbox" name="_enavant" value="1" '.$checked.'>';
}

function partenaireAddMetaboxesEnAvant_save( $post_id ){
	// Check if not null
	if( isset( $_POST['_enavant']) ){
		//Save Data
		update_post_meta(get_the_ID(), '_enavant', $_POST['_enavant']); 
	}else{
		// Delete data
		delete_post_meta( get_the_ID(), '_enavant' );
	}
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesEnAvant_save' );


function partenaireAddMetaboxeslienPartenaire_fields(){
	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'lienPartenaireD',
			'value'			=> 'getIt',
			'placeholder'	=> 'Lien Partenaire',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}
function partenaireAddMetaboxeslienPartenaire_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxeslienPartenaire_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxeslienPartenaire_save' );

function partenaireAddMetaboxesIntervenantsConclusion_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosConclusion',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionConclusion',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function partenaireAddMetaboxesIntervenantsConclusion_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesIntervenantsConclusion_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesIntervenantsConclusion_save' );

function partenaireAddMetaboxesIntervenantsParoleStart_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function partenaireAddMetaboxesIntervenantsParoleStart_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesIntervenantsParoleStart_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesIntervenantsParoleStart_save' );

function partenaireAddMetaboxesIntervenantsParoleEntreprise_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function partenaireAddMetaboxesIntervenantsParoleEntreprise_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesIntervenantsParoleEntreprise_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesIntervenantsParoleEntreprise_save' );

function partenaireAddMetaboxesIntervenantsParoleExpert_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function partenaireAddMetaboxesIntervenantsParoleExpert_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesIntervenantsParoleExpert_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesIntervenantsParoleExpert_save' );
function partenaireAddMetaboxesIntervenantsIntroduction_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photos',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'name',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonction',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'description',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function partenaireAddMetaboxesIntervenantsIntroduction_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesIntervenantsIntroduction_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesIntervenantsIntroduction_save' );
function partenaireAddMetaboxesAnimerPar_fields(){

	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosAnim',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionAnimp',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}

function partenaireAddMetaboxesAnimerPar_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesAnimerPar_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesAnimerPar_save' );



/**
 * Build Metaboxe : Photos du Produit
 */

function partenaireAddMetaboxesMesuresPlan_fields(){

	// Prepare Metabox Fields
	$metaboxListItemsFieldss = array(
		array(
			'type'		=> 'pdf',
			'tag' 	=> 'div',
			'name'	=> 'pdf',
			'value'	=> null
		),
	);



	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'hauteur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Hauteur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'largeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Largeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'profondeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Profondeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'longueur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Longueur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'piecesDispo',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pieces Disponible',
			'class'			=> ''
		),
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFieldss,
			'name'	=> 'pdfs',
			'value'	=> 'getIt',
			'title' => 'Plans PDFs',
			),
	);

	return $metaboxFields;
}

function partenaireAddMetaboxesMesuresPlan_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesMesuresPlan_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesMesuresPlan_save' );



/**
 * Build Metaboxe : Description du Produit
 */

function partenaireAddMetaboxesDescription_fields(){

	$metaboxFields = array(
		array(
			'type' => 'textarea',
			'name' => 'description',
			'value' => 'getIt',
			'class' => 'large-text'
			),
		array('type'=>'line'),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'etat',
			'data'			=> array('type' =>'array', 'data'=>array('Neuf'=>'Neuf',"Exposition"=>"d'exposition")),
			'value'			=> 'getIt',
			'placeholder'	=> 'Etat',
			'class'			=> '',
		),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'coloris',
			'data'			=> array('type' =>'taxonomy', 'name'=>'coloris'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coloris',
			'class'			=> '',
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'finition',
			'data'			=> array('type' =>'taxonomy', 'name'=>'finition'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Finition',
			'class'			=> '',
		),
		array(
			'type'			=> 'checkbox',
			'tag'  			=> 'p',
			'name' 			=> 'typeDisplayHome',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coup de coeur',
			'class'			=> '',
		),
	);

	return $metaboxFields;
}

function partenaireAddMetaboxesDescription_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesDescription_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesDescription_save' );



/**
 * Build Metaboxe : Prix du Produit
 */

function partenaireAddMetaboxesPrix_fields(){

	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbase',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix de base',
			'class'			=> ''
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'prixRemiseType',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Remise',
			'class'			=> '',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbrade',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix Bradé',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixpercent',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pourcentage %',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}

function partenaireAddMetaboxesPrix_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function partenaireAddMetaboxesPrix_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'partenaireAddMetaboxesPrix_save' );