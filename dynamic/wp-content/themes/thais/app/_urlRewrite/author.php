<?php

add_action('init', 'rewriteAuthorBase');

function rewriteAuthorBase() {
    global $wp_rewrite;
    $author_slug = 'magasin'; // change slug name
    $wp_rewrite->author_base = $author_slug;
}