<?php


add_action( 'admin_menu', 'buyCredit' );

function buyCredit() {
	add_menu_page( 'Achat Credit', 'Achat Credit', 'publish_posts', 'buyCredit', 'buyCreditPage', 'dashicons-cart', 6  );
}


function buyCreditPage() {
?>
	<div id="paypal-button"></div>
	<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>

	<script>
	    paypal.Button.render({
	    
	        env: 'sandbox', // Optional: specify 'sandbox' environment
	    
	        client: {
	            sandbox:    'ATlNYF6wDiwV5il6yehF9BIumQ9qbuNS3seib9tXd-hYTq8Ispcv7Pcz_4xnAlWuujA1hHhIPDZrfM9n',
	            production: 'xxxxxxxxx'
	        },

	        payment: function() {
	        
	            var env    = this.props.env;
	            var client = this.props.client;
	        
	            return paypal.rest.payment.create(env, client, {
	                transactions: [
	                    {
	                        amount: { total: '1.00', currency: 'USD' }
	                    }
	                ]
	            });
	        },

	        commit: true, // Optional: show a 'Pay Now' button in the checkout flow

	        onAuthorize: function(data, actions) {
	        
	            // Optional: display a confirmation page here
	        
	            return actions.payment.execute().then(function() {
	                // Show a success page to the buyer
	                console.log('asd');
	            });
	        }

	    }, '#paypal-button');
	</script>

<?php
}