function ConvChar( str ) {
  c = {'<':'&lt;', '>':'&gt;', '&':'&amp;', '"':'&quot;', "'":'&#039;',
       '#':'&#035;' };
  return str.replace( /[<&>'"#]/g, function(s) { return c[s]; } );
}

BACKEND_METABOX = {

	/*
	 * Init & Data
	 */

	Init : function(){
		this.mediaLibrary('image');
		this.mediaLibrary('pdf');
		this.listItems.load();
	},


	mediaLibrary : function(type){
		// Set variables
		if (type=='image') {
			var addSelector  = '.uploadImg',
				delSelector	 = '.deleteImg',
				prevSelector = '.previewImg',
				idSelector 	 = '.idImg';

		} else if(type=='pdf') {
			var addSelector  = '.uploadPdf',
				delSelector	 = '.deletePdf',
				prevSelector = '.previewPdf',
				idSelector 	 = '.idPdf';
		}

		// Add Media
		jQuery(document).on("click", addSelector,function(event){
			event.preventDefault();

			// Fixing Bug : Remove existing frame before open a new frame
			jQuery('.media-modal.wp-core-ui').parent().remove();

			// Set variables
			var frame,
				containerElm	= jQuery(this).parent(),
				addElm			= jQuery(this),
				delElm			= containerElm.find(delSelector),
				prevElm			= containerElm.find(prevSelector),
				idElm 			= containerElm.find(idSelector);

			// If the media frame already exists, reopen it.
			if ( frame ) {
				frame.open();
				return;
			}

			// Create a new media frame
			if (type=='image') {
				frame = wp.media({
					title: 'Select or Upload Media Of Your Chosen Persuasion',
					button: {
						text: 'Use this image'
					},
					library: {
						type: 'image'
					},
					multiple: false,
				});

			} else if (type=='pdf') {
				frame = wp.media({
					title: 'Select or Upload Media Of Your Chosen Persuasion',
					button: {
						text: 'Use this PDF'
					},
					library: {
						type: 'application/pdf'
					},
					multiple: false,
				});
			}

			

			// When an image is selected in the media frame...
			frame.on( 'select', function() {

				// Get media attachment details from the frame state
				var attachment = frame.state().get('selection').first().toJSON();

				// Send the attachment id to our hidden input
				idElm.val( attachment.id ).change();

				// Hide the add image link
				addElm.addClass( 'hidden' );

				// Unhide the remove image link
				delElm.removeClass( 'hidden' );

				// Preview our selected attachement
				if (type=='image') {
					idElm.after('<img src="'+attachment.url+'" alt="" class="previewImg" />');
				} else if(type=='pdf') {
					idElm.after('<div class="previewPdf"><span class="dashicons dashicons-media-default"></span><a class="title" href="'+attachment.url+'">'+attachment.title+'</a></div>');
				}

				// Fixing Bug : Remove Frame after select image
				jQuery('.media-modal.wp-core-ui').parent().remove();

				// Check if is element in items list
				if(containerElm.closest('.itemsPreview').length){

					// Set Variables
					var itemsDataInput = containerElm.closest('.itemsPreview').find('input.itemsListData'),
						itemsDataValue = itemsDataInput.val(),
						itemIndex = containerElm.closest('.itemPreview').index();

					if(itemsDataValue){
						// Parse Data to JSON
						itemsDataValue=JSON.parse(itemsDataValue);

						if(itemIndex in itemsDataValue){
							if (type=='image') {
								// Update Data
								itemsDataValue[itemIndex][containerElm.index()] = {'type' : 'image', 'value' : attachment.id};
							} else if(type=='pdf'){
								itemsDataValue[itemIndex][containerElm.index()] = {'type' : 'pdf', 'value' : attachment.id};
							}

							// Put Data in Field
							itemsDataInput.val(JSON.stringify(itemsDataValue));
						}
					}
				}
			});

			// Finally, open the modal on click
			frame.open();
		});

		// Delete Img
		jQuery(document).on("click", delSelector,function(event){
			event.preventDefault();

			// Set variables
			var containerElm	= jQuery(this).parent(),
				addElm			= containerElm.find(addSelector),
				delElm			= jQuery(this),
				prevElm			= containerElm.find(prevSelector),
				idElm 			= containerElm.find(idSelector);

			// Remove the preview image
			prevElm.remove();

			// Un-hide the add image link
			addElm.removeClass( 'hidden' );

			// Hide the delete image link
			delElm.addClass( 'hidden' );

			// Delete the image id from the hidden input
			idElm.val('');
			
			// Check if is element in items list
			if(containerElm.closest('.itemsPreview').length){
				// Set Variables
				var itemsDataInput = containerElm.closest('.itemsPreview').find('input.itemsListData'),
					itemsDataValue = itemsDataInput.val(),
					itemIndex = containerElm.closest('.itemPreview').index();

				if(itemsDataValue){
					// Parse Data to JSON
					itemsDataValue=JSON.parse(itemsDataValue);

					if(itemIndex in itemsDataValue){
						// Update Data
						itemsDataValue[itemIndex][containerElm.index()] = {'type' : 'image', 'value' : null};

						// Put Data in Field
						itemsDataInput.val(JSON.stringify(itemsDataValue));
					}
				}
			}
		});
	},


	listItems :{

		load : function(){
			this.crudData();
			this.addItem();
			this.removeItem();
		},

		crudData : function(){
			jQuery(".itemsPreview input:not(.itemsListData), .itemPreview textarea, .itemPreview select").focus().change();
			jQuery(document).on("change paste", ".itemsPreview input:not(.itemsListData), .itemPreview textarea, .itemPreview select",function(event){
				// Set Variables
				var 
					listItemsSelector = jQuery(this).closest('.itemsPreview'),
					listItemsDataInput = listItemsSelector.find('input.itemsListData'),
					listItemsData = listItemsDataInput.val();

				// Check if we have Data : We don't have it, Let create it ;)
				if( listItemsData == '' ){

					// Set Variables
					var fieldData = new Array();
					listItemsData = new Array();

					// Get Data from each Item Block
					listItemsSelector.find('.itemPreview').each(function(){	
						
						//Get Data and store it in Array
						jQuery(this).find('input, select, textarea').each(function(){
							var	fieldType  =  jQuery(this).attr('sptype'),
								fieldValue =  jQuery(this).val();
								
								if(fieldValue.indexOf('\'') > 0) fieldValue = ConvChar(fieldValue);
								
							fieldData.push({'type' : fieldType, 'value' : fieldValue});
						});

						// Push Item Block Data to Global Array
						listItemsData.push(fieldData);

						// Reset Item Block Data
						fieldData = new Array();
					});

					// Put Data in Field
					listItemsDataInput.val(JSON.stringify(listItemsData));

				} else { // We Have Data, Yeaaaah Enjoy it ;)
					// Set Variables
					var itemSelector = jQuery(this).parents('.itemPreview'),
						itemIndex  =  itemSelector.index(),
						fieldIndex =  jQuery(this).parent().index(),
						fieldType  =  jQuery(this).attr('sptype'),
						fieldValue =  jQuery(this).val(),
						fieldData = new Array();
						
						if(fieldValue.indexOf('\'') > 0) {fieldValue = ConvChar(fieldValue);}
					//fieldValue = fieldValue.replace(/'/g, "\\'");
					//var stringJson = listItemsData.replace(/\'/g, "\"");
					
					// Parse Data to JSON
					//listItemsData = JSON.stringify(listItemsData);
					//console.log(decodeURIComponent(escape(window.atob( listItemsData ))));
					//console.log(listItemsData)
					try {
						listItemsData = decodeURIComponent(escape(window.atob( listItemsData )));
					} catch(e) {
						listItemsData = decodeURIComponent(listItemsData);
					}
					
					listItemsData=JSON.parse(listItemsData);
					
					//alert(listItemsData)
					//console.log(listItemsData)
					// Check if block data exist "existing Block"
					if(itemIndex in listItemsData){
						// Update Data
						listItemsData[itemIndex][fieldIndex] = {'type' : fieldType, 'value' : fieldValue};
					} else { // New Block
						//Get Data and store it in Array
						itemSelector.find('input, select, textarea').each(function(){
							var	fieldType  =  jQuery(this).attr('sptype'),
								fieldValue =  jQuery(this).val(),
								fieldName  =  jQuery(this).attr('name');
								
								if(fieldValue.indexOf('\'') > 0) fieldValue = ConvChar(fieldValue);

							fieldData.push({'type' : fieldType, 'value' : fieldValue});
						});

						// Push Item Block Data to Global Array
						listItemsData.push(fieldData);

						// Reset Item Block Data
						fieldData = new Array();
					}

					// Put Data in Field
					listItemsDataInput.val(JSON.stringify(listItemsData));
				}
			});
		},

		addItem : function(){
			jQuery(document).on("click", ".itemsPreview button.addItem",function(event){
				// Stop Click Event
				event.preventDefault();

				// Set Variables
				var newItemContentContainer = jQuery(this).parent().find('.itemsPreviewContainer'),
					newItemContent = jQuery(this).parent().find('article.itemPreview').first().clone();

				// Reset Item Block Input, Select, Textarea
				newItemContent.find('input, select, textarea').val('');
				newItemContent.find('img.previewImg, .previewPdf').remove();
				newItemContent.find('.uploadImg, .uploadPdf').removeClass('hidden');
				newItemContent.find('.deleteImg, .deletePdf').addClass('hidden');
				newItemContent.find('.itemRemove').show();

				// Add Item Block
				newItemContentContainer.append(newItemContent).find('article.itemPreview').last().hide().fadeIn();

				// Load Required Functions
				//BACKEND_METABOX.mediaLibrary('image');
				//BACKEND_METABOX.listItems.crudData();
				//BACKEND_METABOX.listItems.removeItem();
			});
		},

		removeItem : function(){
			jQuery(document).on("click", ".itemPreview .itemRemove",function(event){
				// Stop Click Event
				event.preventDefault();

				// Set Variables
				var itemIndex  			=  jQuery(this).parent().index(),
					listItemsDataInput 	= jQuery(this).parents('.itemsPreview').find('input.itemsListData'),
					listItemsData 		= listItemsDataInput.val();
				
				// Check if we have Data : We have Data
				if(listItemsData !== '' && listItemsData !== 'undefined' ){

					// Parse Data to JSON
					listItemsData=JSON.parse(listItemsData);
					
					// Check if we have item Block Data to delete it
					if(itemIndex in listItemsData){
						listItemsData.splice(itemIndex,1);
					}
					
					// Put Data in Field
					listItemsDataInput.val(JSON.stringify(listItemsData));
				}

				// Delete Item Block
				jQuery(this).parent('article.itemPreview').fadeOut(500, function(){
					jQuery(this).remove();
				});
			});
		}
	},
};





BACKEND_THEMEOPTIONS = {

	/*
	 * Init & Data
	 */

	Init : function(){
		this.postsSelect.load();
	},

	postsSelect :{
		load : function(){
			this.findPosts();
			this.addRemovePost();
		},

		findPosts : function(){
			// Function : Add Posts To Search Section Result
			function addPostsSearchResult(buttonSelector, response){
				jQuery.each(response.posts, function() {
					// Set Variables
					var chosenPosts = buttonSelector.parents('.posts').find('input.postsChosen_data').val();

					// Check if Post Chosen Not Empty
					if(chosenPosts !== ''){
						// Json Parse
						chosenPosts = JSON.parse(chosenPosts);

						// Check if Post Chosen 'Remove Active'
						if(jQuery.inArray(this.id.toString(), chosenPosts) !== -1){
							// Add Posts (Chosen Post) 
							buttonSelector.parents('.posts')
								.find('.postsSearchSection .postsSearchSection_searchResult')
								.append('<article id="'+this.id+'" class="remove"><img src="'+this.thumbnail+'"><h1>'+this.title+'</h1><button class="add">add</button><button class="remove">Remove</button></article>');

						} else {
							// Add Posts 'Add Active'
							buttonSelector.parents('.posts')
								.find('.postsSearchSection .postsSearchSection_searchResult')
								.append('<article id="'+this.id+'" class="add"><img src="'+this.thumbnail+'"><h1>'+this.title+'</h1><button class="add">add</button><button class="remove">Remove</button></article></article>');
						}
					} else {
						// Add Posts 'Add Active'
						buttonSelector.parents('.posts')
							.find('.postsSearchSection .postsSearchSection_searchResult')
							.append('<article id="'+this.id+'" class="add"><img src="'+this.thumbnail+'"><h1>'+this.title+'</h1><button class="add">add</button><button class="remove">Remove</button></article></article>');
					}
				});
			};

			// Disable KeyUP (Enter) for Search Form Input Text
			jQuery('.posts .postsSearchSection_searchKeyword').on('keyup keypress', function(e) {
				var keyCode = e.keyCode || e.which;
				if (keyCode === 13) { 
					e.preventDefault();
					return false;
				}
			});

			// Add Posts --> Show Search Posts Section
			jQuery(document).on("click", ".posts .postsChosen button.addPost",function(event) {
				// Stop Click Event
				event.preventDefault();

				// Set Variables
				var buttonSelector = jQuery(this),
					filter = jQuery.parseJSON(buttonSelector.parent().find('input.postsChosen_data').attr('spFilter')),
					data = {
						'action': 'findPosts',
						'filter' : filter,
					};

				//
				if(buttonSelector.parents('.posts').find('.postsSearchSection .postsSearchSection_searchResult article').length){
					buttonSelector.parents('.posts').find('.postsSearchSection').show();
					return;
				}

				// Disable Button Temp
				buttonSelector.prop("disabled",true);

				// Request Server via AJAX
				jQuery.post(ajaxurl, data, function(response) {
					// Encode Json Responsehmedia
					response = jQuery.parseJSON(response);

					// Add Data to Search Section Results
					addPostsSearchResult(buttonSelector, response);

					// Show Posts Search Section
					buttonSelector.parents('.posts').find('.postsSearchSection').show();

					//Enable Button
					buttonSelector.prop("disabled",false);

					// Call AddRemovePost Function
					//BACKEND_THEMEOPTIONS.postsSelect.addRemovePost();
				});
			});

			// Load More Posts --> In Search Posts Section
			jQuery(document).on("click", ".posts .postsSearchSection_loadMore button",function(event){
				// Stop Click Event
				event.preventDefault();

				// Set Variables
				var buttonSelector = jQuery(this),
					filter = jQuery.parseJSON(buttonSelector.parents('.posts').find('input.postsChosen_data').attr('spFilter')),
					offset = buttonSelector.parents('.postsSearchSection_boxContent').find('.postsSearchSection_searchResult article').length,
					data = {
						'action': 'findPosts',
						'filter' : filter,
						'postsperpage' : filter.posts_per_page,
						'keyword' : filter.keyword,
						'offset' : offset
					};

				// Disable Button Temp
				buttonSelector.prop("disabled",true);

				// Request Server via AJAX
				jQuery.post(ajaxurl, data, function(response) {
					//Enable Button
					buttonSelector.prop("disabled",false);

					// If hace Posts
					if(response == ''){
						if (!buttonSelector.parents('.posts').find('.postsSearchSection .postsSearchSection_searchResult .alert').length ){
							buttonSelector.parents('.posts')
								.find('.postsSearchSection .postsSearchSection_searchResult')
								.append('<div class="alert">No More Posts</div>');
							buttonSelector.hide();
						}
						return
					}

					// Encode Json Responsehmedia
					response = jQuery.parseJSON(response);

					// Add Data to Search Section Results
					addPostsSearchResult(buttonSelector, response);

					// Call AddRemovePost Function
					//BACKEND_THEMEOPTIONS.postsSelect.addRemovePost();
				});
			});

			// Search Posts by Keyword --> In Search Posts Section
			jQuery(document).on("click", ".posts .postsSearchSection_searchForm button",function(event){
				// Stop Click Event
				event.preventDefault();

				// Set Variables
				var buttonSelector = jQuery(this),
					filter = jQuery.parseJSON(buttonSelector.parents('.posts').find('input.postsChosen_data').attr('spFilter')),
					keyword = buttonSelector.parent().find('.postsSearchSection_searchKeyword').val(),
					data = {
						'action': 'findPosts',
						'filter' : filter,
						'postsperpage' : filter.posts_per_page,
						'keyword' : keyword,
					};

				// Disable Button Temp
				buttonSelector.prop("disabled",true);

				// Add Keyword to Search Filter
				filter.keyword =keyword;
				buttonSelector.parents('.posts').find('input.postsChosen_data').attr({'spFilter':JSON.stringify(filter)});

				// Request Server via AJAX
				jQuery.post(ajaxurl, data, function(response) {
					// Remove Old Search Result
					buttonSelector.parents('.posts').find('.postsSearchSection .postsSearchSection_searchResult').empty();

					// Remove Old Search Title
					buttonSelector.parents('.posts').find('.postsSearchSection_searchTitle').empty();

					//Enable Button
					buttonSelector.prop("disabled",false);

					if(response == ''){
						if (!buttonSelector.parents('.posts').find('.postsSearchSection .postsSearchSection_searchResult .alert').length ){
							buttonSelector.parents('.posts')
								.find('.postsSearchSection .postsSearchSection_searchResult')
								.append('<div class="alert">No Result for your Keyword : '+ keyword +'</div>');
						}
						buttonSelector.parents('.posts').find('.postsSearchSection_loadMore button').hide();
						return ;
					} else {
						if(keyword !== ''){
							buttonSelector.parents('.posts').find('.postsSearchSection_searchTitle').html('Search Result for your Keyword : '+keyword);
						}
					}

					// Encode Json Responsehmedia
					response = jQuery.parseJSON(response);

					// Add Data to Search Section Results
					addPostsSearchResult(buttonSelector, response);

					// Show Load More Button
					buttonSelector.parents('.posts').find('.postsSearchSection_loadMore button').show();

					// Call AddRemovePost Function
					//BACKEND_THEMEOPTIONS.postsSelect.addRemovePost();
				});
			});

			// Operation Done
			jQuery(document).on("click", ".posts .postsSearchSection_boxFooter button.done",function(event){
				// Stop Click Event
				event.preventDefault();

				// Hide Section Box
				jQuery(this).parents('.posts').children('.postsSearchSection').hide();
			});
		},

		addRemovePost : function(){
			// Add Post (from Search Box)
			jQuery(document).on("click", ".posts .postsSearchSection_searchResult article button.add",function(event){
				// Stop Click Event
				event.preventDefault();
				
				//Set Variables
				var buttonSelector = jQuery(this),
					id = buttonSelector.parent().attr('id'),
					thumbnail = buttonSelector.parent().children('img').attr('src'),
					title = buttonSelector.parent().children('h1').text(),
					chosenIds = buttonSelector.parents('.posts').find('input.postsChosen_data').val();

				// Hide Button Add and Show Button Remove
				buttonSelector.parent().removeClass('add').addClass('remove');
				
				// Insert Post Id to Save (Input : PostsChosen_data)
				if( chosenIds == '' ){
					// Convert chosenId to array
					chosenIds = new Array();

					// Push Data to array
					chosenIds.push(id);

					// Put Data in Form Field
					buttonSelector.parents('.posts').find('input.postsChosen_data').val(JSON.stringify(chosenIds));
				} else {
					// Convert to JSON
					chosenIds=JSON.parse(chosenIds);

					// Check if Post Exist
					if(jQuery.inArray(id, chosenIds) == -1){
						// Push Data to array
						chosenIds.push(id);

						// Put Data in Form Field
						buttonSelector.parents('.posts').find('input.postsChosen_data').val(JSON.stringify(chosenIds));
					}
				}

				// Insert Post To View Posts
				buttonSelector.parents('.posts')
					.find('.postsChosen_list')
					.append('<article id="'+id+'"><img src="'+thumbnail+'"><h1>'+title+'</h1><button class="remove">Remove</div></article>');

				// Load Remove Chosen Posts
				//BACKEND_THEMEOPTIONS.postsSelect.removeChosenPost();
			});
		
			// Remove Post (from Search Box)
			jQuery(document).on("click", ".posts .postsSearchSection_searchResult article button.remove",function(event){
				// Stop Click Event
				event.preventDefault();

				//Set Variables
				var buttonSelector = jQuery(this),
					id = buttonSelector.parent().attr('id'),
					chosenIds = buttonSelector.parents('.posts').find('input.postsChosen_data').val();

				// Hide Button Remove and Show Button 
				buttonSelector.parent().removeClass('remove').addClass('add');
				
				// Convert to JSON
				chosenIds=JSON.parse(chosenIds);

				// Get Post Index
				var postIndex = chosenIds.indexOf(id);

				// Delete Post
				if (postIndex > -1) {
					chosenIds.splice(postIndex, 1);
				}

				// Put Data in Form Field
				buttonSelector.parents('.posts').find('input.postsChosen_data').val(JSON.stringify(chosenIds));

				// Remove Post From View Posts
				buttonSelector.parents('.posts')
					.find('.postsChosen_list article[id='+id+']')
					.remove();
			});

			
			// Remove Posts (from Chosed List)
			jQuery(document).on("click", ".posts .postsChosen button.remove",function(event){
				// Stop Click Event
				event.preventDefault();
				
				//Set Variable
				var id = jQuery(this).parent().attr('id'),
					chosenIds = jQuery(this).parents('.posts').find('input.postsChosen_data').val();

				// Convert to JSON
				chosenIds=JSON.parse(chosenIds);
				
				// Get Post Index
				var postIndex = chosenIds.indexOf(id);

				// Remove Post ID from Input Field
				if (postIndex > -1) {
					chosenIds.splice(postIndex, 1);
				}

				// Put Data in Form Field
				jQuery(this).parents('.posts').find('input.postsChosen_data').val(JSON.stringify(chosenIds));

				// Remove Post From List
				jQuery(this).parent().remove();
			});
			
			jQuery("#latestPostHome").change(function() {
				
				if(jQuery(this).is(":checked")) jQuery(".postsChosen .addPost").hide();
				else jQuery(".postsChosen .addPost").show();
			})
			if(jQuery("#latestPostHome").size() != 0)
			{
				if(jQuery("#latestPostHome").is(":checked")) jQuery(".postsChosen .addPost").hide();
				else jQuery(".postsChosen .addPost").show();
			}
			
			
		}
	}

};

	
jQuery(function() {
	BACKEND_METABOX.Init();
	BACKEND_THEMEOPTIONS.Init();
});