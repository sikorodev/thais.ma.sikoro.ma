<?php

/**
 * Init Config Variables
 */

$modulesConfig = array();
$templateDirectory = get_template_directory();



/**
 * Shared modules
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'_shared',
			'files'	=>	array(
							'taxonomyClassifiedVilles'
						)
);
array_push($modulesConfig, $module);

/**
 * Framework modules
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'_framework',
			'files'	=>	array(
							'metaboxFieldsBuilder',
							'themeOptionsFieldsBuilder',
							'userFieldsBuilder'
						)
);
array_push($modulesConfig, $module);



/**
 * Shared modules
 */


array_push($modulesConfig, $module);



/**
 * Url Rewrite
 */
$module = array(
			'active'			=> true,
			'directory'	=>	'_urlRewrite',
			'files'	=>	array(
							'author',
						)
);
array_push($modulesConfig, $module);



/**
 * Classified modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'classified',
			'files'	=>	array(
							'posttype',
							'metabox',
							'searchPostAjax',
						)
);
array_push($modulesConfig, $module);




/**
 * Village des start Up modules
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'slider',
			'files'	=>	array(
							'posttype'
						)
);
array_push($modulesConfig, $module);


/**
 * Maillist modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'maillist',
			'files'	=>	array(
							'posttype',
							'addSubscriberAjax'
						)
);
array_push($modulesConfig, $module);



/**
 * Landing Maillist modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'landingMail',
			'files'	=>	array(
							'posttype',
							'metabox',
							'addSubscriberAjax'
						)
);
array_push($modulesConfig, $module);


/**
 * Partenaire modules
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'partener',
			'files'	=>	array(
							'posttype',
							'metabox'
						)
);
array_push($modulesConfig, $module);


/**
 * Team modules
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'job',
			'files'	=>	array(
							'posttype',
							'metabox'
						)
);
array_push($modulesConfig, $module);


/**
 * Gallery modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'gallery',
			'files'	=>	array(
							'posttype',
						)
);
array_push($modulesConfig, $module);

/**
 * Metiers modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'metiers',
			'files'	=>	array(
							'posttype',
							'metabox'
						)
);
array_push($modulesConfig, $module);


/**
 * En une ligne modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'en_une_ligne',
			'files'	=>	array(
							'posttype',
							'metabox'
						)
);
array_push($modulesConfig, $module);


/**
 * Equipes modules
 */

$module = array(
			'active'			=> false,
			'directory'	=>	'equipe',
			'files'	=>	array(
							'posttype',
							'metabox'
						)
);
array_push($modulesConfig, $module);

/**
 * Website Options
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'siteOptions',
			'files'	=>	array(
							'general'
						)
);
array_push($modulesConfig, $module);



/**
 * Website Modules
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'_modules',
			'files'	=>	array(
							'findPostsAjax'
						)
);
array_push($modulesConfig, $module);





/**
 * Front Functions
 */

$module = array(
			'active'			=> true,
			'directory'	=>	'_frontFunctions',
			'files'	=>	array(
							'annonce',
							'shared'
						)
);
array_push($modulesConfig, $module);










/**
 * Load Modules
 */

foreach ($modulesConfig as $moduleConfig) {
	if($moduleConfig['active']){
		foreach ($moduleConfig['files'] as $fileName) {
			$fileName = $templateDirectory.'/app/'.$moduleConfig['directory'].'/'.$fileName.'.php';
			require_once ($fileName);
		}
	}
}













/*
 * Load JS & CSS
 */

function JsCssLoad($hook) {
    if ( 'edit.php' == $hook ) {
        return;
    }

    wp_enqueue_script( 'backend_script', get_template_directory_uri() . '/app/_assets/js/backend.js' );
    wp_enqueue_style( 'backend_script', get_template_directory_uri() . '/app/_assets/css/backend.css' );
}
add_action( 'admin_enqueue_scripts', 'JsCssLoad' );