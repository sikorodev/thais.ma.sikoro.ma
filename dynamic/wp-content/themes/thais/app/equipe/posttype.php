<?php

/**
 *
 */
function equipePosttype() {
    $labels = array(
        'name'                  => 'Equipes',
        'singular_name'         => 'List',
        'menu_name'             => 'Equipes',
        'name_admin_bar'        => 'List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau',
        'new_item'              => 'Nouveau',
        'edit_item'             => 'Editer',
        'view_item'             => 'Visualiser',
        'all_items'             => 'Liste',
        'search_items'          => 'Chercher',
        'parent_item_colon'     => 'Parent',
        'not_found'             => 'Aucun Item Trouvé',
        'not_found_in_trash'    => 'Aucun Item Trouvé.',
        'featured_image'        => 'Item cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Items archives',
        'insert_into_item'      => 'Inserer',
        'uploaded_to_this_item' => 'Uploader',
        'filter_items_list'     => 'Filtrer la liste des Items',
        'items_list_navigation' => 'Liste Navigation des Items',
        'items_list'            => 'Liste des Items',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'=>  27,
        'menu_icon'           => 'dashicons-admin-users',
        'supports'            => array( 'title', 'thumbnail' ),
        'has_archive'        => false,
        'rewrite'            => false,
        //
        'public'              => false,
        'exclude_from_search' => true, 
        'publicly_queryable'  => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => false,
    );
 
    register_post_type( 'equipe', $args );
}

add_action( 'init', 'equipePosttype');

?>