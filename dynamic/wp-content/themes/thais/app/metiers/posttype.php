<?php

/**
 *
 */
function temoignagesPosttype() {
    $labels = array(
        'name'                  => 'Temoignages',
        'singular_name'         => 'Temoignages List',
        'menu_name'             => 'Temoignages',
        'name_admin_bar'        => 'Temoignages List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau Temoignages',
        'new_item'              => 'Nouveau Temoignages',
        'edit_item'             => 'Editer Temoignages',
        'view_item'             => 'Visualiser Temoignages',
        'all_items'             => 'Liste des Temoignages',
        'search_items'          => 'Chercher Temoignages',
        'parent_item_colon'     => 'Parent Temoignages',
        'not_found'             => 'Aucun Temoignages Trouvé',
        'not_found_in_trash'    => 'Aucun Temoignages Trouvé.',
        'featured_image'        => 'Temoignages cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Temoignages archives',
        'insert_into_item'      => 'Inserer dans Temoignages',
        'uploaded_to_this_item' => 'Uploader dans Temoignages',
        'filter_items_list'     => 'Filtrer la liste des Temoignages',
        'items_list_navigation' => 'Liste Navigation des Temoignages',
        'items_list'            => 'Liste des Temoignages',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'=>  27,
        'menu_icon'           => 'dashicons-portfolio',
        'supports'            => array( 'title', 'thumbnail', 'excerpt', 'editor' ),
        'has_archive'        => false,
        'rewrite'            => false,
        //
        'public'              => false,
        'exclude_from_search' => true, 
        'publicly_queryable'  => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => false,
    );
 
    register_post_type( 'temoignages', $args );
}

add_action( 'init', 'temoignagesPosttype');

?>