<?php

use framework\metaboxFieldsBuilder as fieldBuilder;


// Set Variables
$metaboxesPosttype = 'temoignages';

/**
 * Add Metaboxes
 */

function temoignagesAddMetaboxes( $post ){
	// Set Metaboxes as Array
	$metaboxes = array(
					array(
							'id' 		=> 'slugtemoignages',
							'title' 	=> 'Slug temoignages',
						)
				 );

	// Register Metaboxes
	foreach ($metaboxes as $metabox) {
		add_meta_box( 'temoignagesAddMetaboxes'.$metabox['id'], $metabox['title'], 'temoignagesAddMetaboxes'.$metabox['id'].'_build', $metaboxesPosttype, 'normal', 'high' );
	}









}
add_action( 'add_meta_boxes_'.$metaboxesPosttype, 'temoignagesAddMetaboxes' );



/**
 * Build Metaboxe : Mettre en Avant
 */

function temoignagesAddMetaboxesEnAvant($post){

	$enAvant = get_post_meta( get_the_ID(), '_enavant', true );
	$checked = ($enAvant) ? 'checked': NULL ;
	echo '<input type="checkbox" name="_enavant" value="1" '.$checked.'>';
}

function temoignagesAddMetaboxesEnAvant_save( $post_id ){
	// Check if not null
	if( isset( $_POST['_enavant']) ){
		//Save Data
		update_post_meta(get_the_ID(), '_enavant', $_POST['_enavant']); 
	}else{
		// Delete data
		delete_post_meta( get_the_ID(), '_enavant' );
	}
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesEnAvant_save' );


function temoignagesAddMetaboxesslugtemoignages_fields(){
	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'slugtemoignagesD',
			'value'			=> 'getIt',
			'placeholder'	=> 'Slug temoignages',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}
function temoignagesAddMetaboxesslugtemoignages_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesslugtemoignages_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesslugtemoignages_save' );

function temoignagesAddMetaboxesIntervenantsConclusion_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosConclusion',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionConclusion',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function temoignagesAddMetaboxesIntervenantsConclusion_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesIntervenantsConclusion_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesIntervenantsConclusion_save' );

function temoignagesAddMetaboxesIntervenantsParoleStart_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function temoignagesAddMetaboxesIntervenantsParoleStart_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesIntervenantsParoleStart_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesIntervenantsParoleStart_save' );

function temoignagesAddMetaboxesIntervenantsParoleEntreprise_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function temoignagesAddMetaboxesIntervenantsParoleEntreprise_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesIntervenantsParoleEntreprise_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesIntervenantsParoleEntreprise_save' );

function temoignagesAddMetaboxesIntervenantsParoleExpert_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function temoignagesAddMetaboxesIntervenantsParoleExpert_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesIntervenantsParoleExpert_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesIntervenantsParoleExpert_save' );
function temoignagesAddMetaboxesIntervenantsIntroduction_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photos',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'name',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonction',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'description',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function temoignagesAddMetaboxesIntervenantsIntroduction_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesIntervenantsIntroduction_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesIntervenantsIntroduction_save' );
function temoignagesAddMetaboxesAnimerPar_fields(){

	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosAnim',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionAnimp',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}

function temoignagesAddMetaboxesAnimerPar_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesAnimerPar_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesAnimerPar_save' );



/**
 * Build Metaboxe : Photos du Produit
 */

function temoignagesAddMetaboxesMesuresPlan_fields(){

	// Prepare Metabox Fields
	$metaboxListItemsFieldss = array(
		array(
			'type'		=> 'pdf',
			'tag' 	=> 'div',
			'name'	=> 'pdf',
			'value'	=> null
		),
	);



	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'hauteur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Hauteur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'largeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Largeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'profondeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Profondeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'longueur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Longueur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'piecesDispo',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pieces Disponible',
			'class'			=> ''
		),
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFieldss,
			'name'	=> 'pdfs',
			'value'	=> 'getIt',
			'title' => 'Plans PDFs',
			),
	);

	return $metaboxFields;
}

function temoignagesAddMetaboxesMesuresPlan_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesMesuresPlan_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesMesuresPlan_save' );



/**
 * Build Metaboxe : Description du Produit
 */

function temoignagesAddMetaboxesDescription_fields(){

	$metaboxFields = array(
		array(
			'type' => 'textarea',
			'name' => 'description',
			'value' => 'getIt',
			'class' => 'large-text'
			),
		array('type'=>'line'),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'etat',
			'data'			=> array('type' =>'array', 'data'=>array('Neuf'=>'Neuf',"Exposition"=>"d'exposition")),
			'value'			=> 'getIt',
			'placeholder'	=> 'Etat',
			'class'			=> '',
		),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'coloris',
			'data'			=> array('type' =>'taxonomy', 'name'=>'coloris'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coloris',
			'class'			=> '',
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'finition',
			'data'			=> array('type' =>'taxonomy', 'name'=>'finition'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Finition',
			'class'			=> '',
		),
		array(
			'type'			=> 'checkbox',
			'tag'  			=> 'p',
			'name' 			=> 'typeDisplayHome',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coup de coeur',
			'class'			=> '',
		),
	);

	return $metaboxFields;
}

function temoignagesAddMetaboxesDescription_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesDescription_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesDescription_save' );



/**
 * Build Metaboxe : Prix du Produit
 */

function temoignagesAddMetaboxesPrix_fields(){

	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbase',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix de base',
			'class'			=> ''
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'prixRemiseType',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Remise',
			'class'			=> '',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbrade',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix Bradé',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixpercent',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pourcentage %',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}

function temoignagesAddMetaboxesPrix_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function temoignagesAddMetaboxesPrix_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'temoignagesAddMetaboxesPrix_save' );