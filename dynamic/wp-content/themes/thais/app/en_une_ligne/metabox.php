<?php

use framework\metaboxFieldsBuilder as fieldBuilder;


// Set Variables
$metaboxesPosttype = 'en_une_ligne';

/**
 * Add Metaboxes
 */

function en_une_ligneAddMetaboxes( $post ){
	// Set Metaboxes as Array
	$metaboxes = array(
					array(
							'id' 		=> 'nombreen_une_ligne',
							'title' 	=> 'Nombre',
						)
				 );

	// Register Metaboxes
	foreach ($metaboxes as $metabox) {
		add_meta_box( 'en_une_ligneAddMetaboxes'.$metabox['id'], $metabox['title'], 'en_une_ligneAddMetaboxes'.$metabox['id'].'_build', $metaboxesPosttype, 'normal', 'high' );
	}









}
add_action( 'add_meta_boxes_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxes' );



/**
 * Build Metaboxe : Mettre en Avant
 */

function en_une_ligneAddMetaboxesEnAvant($post){

	$enAvant = get_post_meta( get_the_ID(), '_enavant', true );
	$checked = ($enAvant) ? 'checked': NULL ;
	echo '<input type="checkbox" name="_enavant" value="1" '.$checked.'>';
}

function en_une_ligneAddMetaboxesEnAvant_save( $post_id ){
	// Check if not null
	if( isset( $_POST['_enavant']) ){
		//Save Data
		update_post_meta(get_the_ID(), '_enavant', $_POST['_enavant']); 
	}else{
		// Delete data
		delete_post_meta( get_the_ID(), '_enavant' );
	}
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesEnAvant_save' );


function en_une_ligneAddMetaboxesnombreen_une_ligne_fields(){
	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nombreEnligneM',
			'value'			=> 'getIt',
			'placeholder'	=> 'Nombre',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'uniteEnligneM',
			'value'			=> 'getIt',
			'placeholder'	=> 'Unité',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}
function en_une_ligneAddMetaboxesnombreen_une_ligne_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesnombreen_une_ligne_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesnombreen_une_ligne_save' );

function en_une_ligneAddMetaboxesIntervenantsConclusion_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosConclusion',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionConclusion',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionConclusion',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function en_une_ligneAddMetaboxesIntervenantsConclusion_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesIntervenantsConclusion_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesIntervenantsConclusion_save' );

function en_une_ligneAddMetaboxesIntervenantsParoleStart_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function en_une_ligneAddMetaboxesIntervenantsParoleStart_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesIntervenantsParoleStart_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesIntervenantsParoleStart_save' );

function en_une_ligneAddMetaboxesIntervenantsParoleEntreprise_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function en_une_ligneAddMetaboxesIntervenantsParoleEntreprise_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesIntervenantsParoleEntreprise_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesIntervenantsParoleEntreprise_save' );

function en_une_ligneAddMetaboxesIntervenantsParoleExpert_fields(){
	$metaboxListItemsFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosExpr',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameExpr',
			'value'			=> null,
			'placeholder'	=> 'Name'
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionExprr',
			'value'			=> null,
			'placeholder'	=> 'Fonction'
		),
		array(
			'type' => 'textarea',
			'name' => 'descriptionExpr',
			'value' => 'getIt',
			'class' => 'large-text'
		)
	);

	$metaboxFields = array(
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFields,
			'name'	=> 'photos',
			'value'	=> 'getIt',
			'title' => '',
			),
	);

	return $metaboxFields;
}
function en_une_ligneAddMetaboxesIntervenantsParoleExpert_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesIntervenantsParoleExpert_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesIntervenantsParoleExpert_save' );
function en_une_ligneAddMetaboxesIntervenantsIntroduction_fields(){
	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photos',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'name',
			'value'			=> 'getIt',
			'placeholder'	=> 'name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonction',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'description',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}
function en_une_ligneAddMetaboxesIntervenantsIntroduction_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesIntervenantsIntroduction_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesIntervenantsIntroduction_save' );
function en_une_ligneAddMetaboxesAnimerPar_fields(){

	$metaboxFields = array(
		array(
			'type'		=> 'image',
			'tag' 	=> 'p',
			'name'	=> 'photosAnim',
			'value'	=> null
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'nameAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Name',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'fonctionAnim',
			'value'			=> 'getIt',
			'placeholder'	=> 'Fonction',
			'class'			=> ''
		),
		array(
			'type'			=> 'textarea',
			'name' 			=> 'descriptionAnimp',
			'value'			=> 'getIt',
			'class' => 'large-text'
		)
	);

	return $metaboxFields;
}

function en_une_ligneAddMetaboxesAnimerPar_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesAnimerPar_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesAnimerPar_save' );



/**
 * Build Metaboxe : Photos du Produit
 */

function en_une_ligneAddMetaboxesMesuresPlan_fields(){

	// Prepare Metabox Fields
	$metaboxListItemsFieldss = array(
		array(
			'type'		=> 'pdf',
			'tag' 	=> 'div',
			'name'	=> 'pdf',
			'value'	=> null
		),
	);



	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'hauteur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Hauteur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'largeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Largeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'profondeur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Profondeur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'longueur',
			'value'			=> 'getIt',
			'placeholder'	=> 'Longueur',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'piecesDispo',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pieces Disponible',
			'class'			=> ''
		),
		array(
			'type' => 'listItems',
			'fields' => $metaboxListItemsFieldss,
			'name'	=> 'pdfs',
			'value'	=> 'getIt',
			'title' => 'Plans PDFs',
			),
	);

	return $metaboxFields;
}

function en_une_ligneAddMetaboxesMesuresPlan_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesMesuresPlan_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesMesuresPlan_save' );



/**
 * Build Metaboxe : Description du Produit
 */

function en_une_ligneAddMetaboxesDescription_fields(){

	$metaboxFields = array(
		array(
			'type' => 'textarea',
			'name' => 'description',
			'value' => 'getIt',
			'class' => 'large-text'
			),
		array('type'=>'line'),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'etat',
			'data'			=> array('type' =>'array', 'data'=>array('Neuf'=>'Neuf',"Exposition"=>"d'exposition")),
			'value'			=> 'getIt',
			'placeholder'	=> 'Etat',
			'class'			=> '',
		),
		
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'coloris',
			'data'			=> array('type' =>'taxonomy', 'name'=>'coloris'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coloris',
			'class'			=> '',
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'finition',
			'data'			=> array('type' =>'taxonomy', 'name'=>'finition'),
			'value'			=> 'getIt',
			'placeholder'	=> 'Finition',
			'class'			=> '',
		),
		array(
			'type'			=> 'checkbox',
			'tag'  			=> 'p',
			'name' 			=> 'typeDisplayHome',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Coup de coeur',
			'class'			=> '',
		),
	);

	return $metaboxFields;
}

function en_une_ligneAddMetaboxesDescription_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesDescription_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesDescription_save' );



/**
 * Build Metaboxe : Prix du Produit
 */

function en_une_ligneAddMetaboxesPrix_fields(){

	$metaboxFields = array(
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbase',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix de base',
			'class'			=> ''
		),
		array(
			'type'			=> 'select',
			'tag'  			=> 'p',
			'name' 			=> 'prixRemiseType',
			'data'			=> array('type' =>'array', 'data'=>array('percent'=>'Pourcentage','price'=>'Prix Bradé','none'=>'Pas de remise')),
			'value'			=> 'getIt',
			'placeholder'	=> 'Remise',
			'class'			=> '',
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixbrade',
			'value'			=> 'getIt',
			'placeholder'	=> 'Prix Bradé',
			'class'			=> ''
		),
		array(
			'type'			=> 'text',
			'tag'  			=> 'p',
			'name' 			=> 'prixpercent',
			'value'			=> 'getIt',
			'placeholder'	=> 'Pourcentage %',
			'class'			=> ''
		)
	);

	return $metaboxFields;
}

function en_une_ligneAddMetaboxesPrix_build($post){

	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxBuilder(__FUNCTION__);
}

function en_une_ligneAddMetaboxesPrix_save( $post_id ){
	
	// Load fieldBuilder Class
	$fieldBuilder = new fieldBuilder;

	// Build Metabox
	$fieldBuilder->metaboxSave(__FUNCTION__);
}

add_action( 'save_post_'.$metaboxesPosttype, 'en_une_ligneAddMetaboxesPrix_save' );