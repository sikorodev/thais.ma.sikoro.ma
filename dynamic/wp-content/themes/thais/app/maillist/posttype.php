<?php

/**
 *
 */
function maillistPosttype() {
    $labels = array(
        'name'                  => 'Mail List',
        'singular_name'         => 'Mail List',
        'menu_name'             => 'Mail List',
        'name_admin_bar'        => 'Mail List',
        'add_new'               => 'Ajouter nouveau',
        'add_new_item'          => 'Ajouter nouveau Mail',
        'new_item'              => 'Nouveau Mail',
        'edit_item'             => 'Editer Mail',
        'view_item'             => 'Visualiser Mail',
        'all_items'             => 'Tous les Mails',
        'search_items'          => 'Chercher Mail',
        'parent_item_colon'     => 'Parent Mail',
        'not_found'             => 'Aucun Mail Trouvé',
        'not_found_in_trash'    => 'Aucun Mail Trouvé.',
        'featured_image'        => 'Mail cover image',
        'set_featured_image'    => 'Set cover image',
        'remove_featured_image' => 'Remove cover image',
        'use_featured_image'    => 'Use as cover image',
        'archives'              => 'Mails archives',
        'insert_into_item'      => 'Inserer dans Mail',
        'uploaded_to_this_item' => 'Uploader dans Mail',
        'filter_items_list'     => 'Filtrer la liste des Mails',
        'items_list_navigation' => 'Liste Navigation des Mail',
        'items_list'            => 'Liste des Mails',
    );
 
    $args = array(
        'labels'              => $labels,
        'hierarchical'        => false,
        'menu_position'=>  30,
        'menu_icon'           => 'dashicons-email',
        'supports'            => array( 'title'),
        'has_archive'        => false,
        'rewrite'            => false,
        //
        'public'              => false,
        'exclude_from_search' => true, 
        'publicly_queryable'  => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        //
        'query_var'          => false,
    );
 
    register_post_type( 'maillist', $args );
}

add_action( 'init', 'maillistPosttype');

?>