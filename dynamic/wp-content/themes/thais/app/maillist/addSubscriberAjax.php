<?php

add_action( 'wp_ajax_nopriv_addSubscriber', 'addSubscriber' );
add_action( 'wp_ajax_addSubscriber', 'addSubscriber' );

function addSubscriber() {
	global $wpdb; // this is how you get access to the database

	// Get Variables From Post
	$email = $_POST['email'];

	// If email is valid
	if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

		// Query Arguments
		$args = array(
					'post_status' => 'publish',
					'post_type'	  => 'maillist',
					's'			  => $email
				);

		$query = new WP_Query( $args );

		// Exist Subscriber
		if ( $query->have_posts() ) {
			// Create Reponse Result
			$result=array(
				'status'=>'exist',
				'msg' => 'l\'adresse email que vous avez entrée existe déjà',
			);

			// Return Json Response
			echo json_encode($result, true);

		} else {
			// Args
			$args = array('post_title'=>$email,'post_status'=>'publish','post_type'=>'maillist');

			// Register Subscriber
			wp_insert_post($args);

			// Create Reponse Result
			$result=array(
				'status'=>'added',
				'msg' => 'Merci pour votre inscription à notre newsletter',
			);

			// Return Json Response
			echo json_encode($result, true);
		}

	// If email Not Valid
	} else {
		// Create Reponse Result
		$result=array(
			'status'=>'error',
			'msg' => '',
		);

		// Return Json Response
		echo json_encode($result, true);
	}

	
	wp_reset_postdata(); // Restore original Post Data 
	wp_die(); // this is required to terminate immediately and return a proper response

}