// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var plumber = require('gulp-plumber');

gulp.task('img:optim', function(){
    return gulp.src(config.assets.images_src + '/*')
        .pipe(plumber())
        .pipe(imagemin([
            imagemin.gifsicle({
                optimizationLevel: 3,
                colors: 32
            }),
            imagemin.jpegtran({
                progressive: true
            }),
            imageminPngquant({
                floyd: 0.4,
                quality: '30-50'
            }),
            imagemin.optipng({
                optimizationLevel: 6
            }),
            imagemin.svgo()
            ], {}))
        .pipe(gulp.dest(config.assets.images_dest));
});
