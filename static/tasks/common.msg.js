var gulp = require('gulp');
var gutil = require('gulp-util');
var chalk = require('chalk');

var templateChalkGreen = chalk.bold.green;

gulp.task('common:msg:deployend', function(callback) {
    console.log(templateChalkGreen('/------------------/'));
    console.log(templateChalkGreen('/ Starter Deployed /'));
    console.log(templateChalkGreen('/------------------/'));
});
