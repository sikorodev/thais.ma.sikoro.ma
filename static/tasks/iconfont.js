// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var iconfont = require('gulp-iconfont');
var del = require('del');
var consolidate = require('gulp-consolidate');
var browserSync = require('browser-sync').create();
var runTimestamp = Math.round(Date.now()/1000);

// iconFont
gulp.task('iconfont', function(){
    del(config.fonticon.build);
    del(config.styles.vendor_build + '/fonticon.css');

    return gulp.src([config.fonticon.src + '*.svg'])
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'iconfont', // required
            appendUnicode: true, // recommended option
            formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp, // recommended to get consistent builds when watching files
        }))
        .on('glyphs', function(glyphs, options) {
            // CSS templating, e.g.
            gulp.src(config.fonticon.tpl + 'fonticon.css')
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: 'iconfont',
                    fontPath: config.fonticon.dist,
                    className: 'c-fonticon'
                }))
                .pipe(gulp.dest(config.styles.vendor_build));
        })
        .pipe(gulp.dest(config.fonticon.build))
        .pipe(browserSync.stream());
});

// generate pca_common fonticon
gulp.task('fonticon:common', function(){
    del(config.fonticon.build_common);
    del(config.styles.vendor_build + '/sik__common.css');

    return gulp.src([config.fonticon.src_common])
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'sik__common', // required
            appendUnicode: true, // recommended option
            formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp, // recommended to get consistent builds when watching files
        }))
        .on('glyphs', function(glyphs, options) {
            // CSS templating
            gulp.src(config.fonticon.tpl_common)
                .pipe(plumber())
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: 'sik__common',
                    fontPath: config.fonticon.dist_common,
                    className: 'c-fsik'
                }))
                .pipe(gulp.dest(config.styles.vendor_build));
        })
        .pipe(gulp.dest(config.fonticon.build_common));
});
