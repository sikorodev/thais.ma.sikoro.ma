'use strict';

// Config
// ========================================= //

// Config
var config = require('./config.json');

// Require
var emoji = require('node-emoji')
var gulp = require('gulp');
var gutil = require('gulp-util');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var es6Promise = require('es6-promise').polyfill();
var chalk = require('chalk');

var templateChalkGreen = chalk.bold.green;

var requireDir = require('require-dir');
requireDir('./tasks');

// run server / watch
gulp.task('live', function() {
    browserSync.init({
        startPath: config.views.build,
        server: {
            baseDir: './build/',
            directory: true
        },
		ui:{
			port: 8888
		},
        files: [
            config.styles.local_build + '/*.css',
            config.scripts.src
        ]
    });

    gulp.watch(config.views.build + '/*.html', function() {
        browserSync.reload();
    });
    gulp.watch(config.styles.local_libs, ['styles:local']);
    gulp.watch(config.views.tpl, ['views:pug']);
    gulp.watch(config.scripts.src, ['scripts:jshint']);
    gulp.watch(config.fonticon.src + '*.svg', ['fonticon']);
    // gulp.watch(config.assets.images_src + '/**.*', ['img:optim']);

});

// deploy project
gulp.task('deploy:finished', function(callback) {
    var flag = emoji.get('checkered_flag');
    console.log(templateChalkGreen(flag +'  Starter Deploy'));
});

gulp.task('deploy', function(callback) {
    runSequence('bower', 'copy:normalize:css' ,'copy:foundation:scss', 'copy:foundation:vendor:scss', 'styles:vendor:foundation', 'copy:vendor', 'clean:bower', 'common:msg:deployend', callback);
});

// generate favicon
gulp.task('favicon', function(callback) {
    runSequence('favicon:info', 'favicon:generate', callback);
})

// default
gulp.task('default', ['styles:local', 'views:pug', 'scripts:jshint', 'live']);
