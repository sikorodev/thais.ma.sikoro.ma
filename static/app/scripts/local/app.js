// Global variables

var $document = $(document),
    $window = $(window),
    $htmlBody = $('html, body'),
    $body = $('body');



// Global namespace
var app = app || {};
app.page = app.page || {};

/**
 * Print a tooltip with the image size (useful for bLazy breakpoints)
 * @param  {string}  	selector 	The targetted images
 * @param  {boolean} 	resize   	If set to true, update sizes on $window.resize();
 * @param  {boolean} 	scroll   	If set to true, update sizes on $window.scroll();
 */
app.imgSize = function(selector, resize, scroll) {
    var timer;
    $(selector).each(function(i, e) {

        var $this = $(this),
            width = $this.outerWidth(),
            height = $this.outerHeight();

        if ( $this.next('.size-tooltip').length ) {
            $this.next('.size-tooltip').html(width +' x '+ height);
        } else {
            $this.parent().append('<div class="size-tooltip">'+ width +' x '+ height +'</div>');
        }
    });

    if ( resize ) {
        $window.resize(function(event) {

            clearTimeout(timer);
            timer = setTimeout(function() {
                app.imgSize(selector, true);
            }, 300);

        });
    }

    if ( scroll ) {
        $window.scroll(function(event) {

            clearTimeout(timer);
            timer = setTimeout(function() {
                app.imgSize(selector, resize, true);
            }, 300);

        });
    }

};

/**
 * Custom the scroll bar
 * @param  {objHTML}  	$container 	Container which need to custom scrollbar
 * @param  {array} 		options   	Options for the customing scrollbar
 */
app.customScrollbar = function($container, options) {

    var _options = {
        scrollInertia : 0
    };

    var settings = $.extend({}, _options, options);

    return $container.mCustomScrollbar(settings);
};

/**
 * Helper for prefixed CSS 3D transform with fallback
 * to 2D transform if the 3D ones are not supported
 * @param  {integer}	x		Value for the X axis
 * @param  {integer}	y		Value for the Y axis
 * @param  {integer}	z		Value for the Z axis (not used with 2D transform, obviously)
 * @return {object}
 */
app.translate = function(x,y,z) {
    if ( Modernizr.csstransforms3d ) {
        return {
            '-webkit-transform' : 'translate3d('+ x +', ' + y + 'px, '+ z +')',
            '-ms-transform'     : 'translate3d('+ x +', ' + y + 'px, '+ z +')',
            'transform'         : 'translate3d('+ x +', ' + y + 'px, '+ z +')'
        };
    } else if ( !Modernizr.csstransforms3d && Modernizr.csstransforms ) {
        return {
            '-webkit-transform' : 'translate('+ x +', ' + y + 'px)',
            '-ms-transform'     : 'translate('+ x +', ' + y + 'px)',
            'transform'         : 'translate('+ x +', ' + y + 'px)'
        };
    }
};

app.transition = function(value) {
    if ( Modernizr.csstransitions ) {
        return {
            '-webkit-transition' : value,
            '-ms-transition'     : value,
            'transition'         : value
        };
    }
};

app.svgUrl = function(val) {
    var url = (Detectizr.browser.name != 'ie') ? 'assets/svg/sprite.svg#svg-' : "#svg-";
    return '<svg am-s="'+val.uid+'" viewbox="'+val.viewbox+'"><use xlink:href="'+url+val.uid+'"></use></svg>';
};

app.cleanTimeline = function(tl) {

    var children = tl.getChildren();
    for( var i = 0; i < children.length; i++ ) {
        if(typeof children[i].target === 'undefined') continue;
        TweenMax.set($(children[i].target), { clearProps : 'x, autoAlpha, y' });
    }
};
app.parallax = function(el, speed, async, parent) {

    if ( Detectizr.device.type === 'desktop' ) {
        // Half window height
        var wH = $window.outerHeight()/2,
            vW = $window.outerWidth(),
            timer,
            _parent = typeof parent === 'undefined' ? 'section' : parent;

        el.each(function(i, e) {
            var $this = $(this),
                parentOffset = $this.closest(_parent).offset().top,
                sp = speed === 'inherit' ? $this.data('speed') : speed,
                s = async ? sp*(i+0.8) : sp; // if the elements must be asyncronous

            $window.scroll(function() {
                var scrolled = $window.scrollTop(),
                    offsetTop = $this.offset().top + ($this.outerHeight()/2), // calculate offsetTop on scroll because Lazy Load :(
                    y = (Math.round( (scrolled + wH - offsetTop) * s ) / 100); // round that number to 2 decimals

                clearTimeout( timer );

                timer = setTimeout(function() {
                    vW = $window.outerWidth();
                }, 100);

                if ( vW >= 992 )
                    $this.css( app.translate( 0, y, 0) );
                else
                    $this.css( app.translate( 0, 0, 0) );
            });
        });
    }
};