require.config({
    baseURL: '.',
    paths: {
        conf: 'conf/appconf',
        jquery: 'vendor/jquery.min',
        Modernizr: 'vendor/modernizr.min',
        createjs: 'vendor/preloadjs-0.4.1.min',
        Swiper: 'vendor/swiper.min',
        Detectizr: 'vendor/detectizr',
        waypoint: 'vendor/jquery.waypoints.min',
        LazyLoad: 'vendor/lazyload.min',
        'async': 'vendor/requirejs-plugins/src/async'
    },
    shim: {
        conf : {
            exports: 'conf'
        },
        jquery: {
            deps: [],
            exports: '$',
            init: function () {
                return window.jQuery = $;
            }
        },
        Modernizr: {
            exports: 'Modernizr'
        },
        createjs: {
            exports: 'createjs'
        },
        Swiper: {
            exports: 'Swiper'
        },
        Detectizr: {
            exports: 'Detectizr'
        },
        waypoint : {
            exports: 'waypoint',
            deps: ['jquery']
        },
        LazyLoad: {
            exports: 'LazyLoad'
        },
        'async': 'vendor/requirejs-plugins/src/async'
    }
});

require([
    // Load our app module and pass it to our definition function
    'app',
], function (App) {
    // The "app" dependency is passed in as "App"
    App.initialize.init();
});