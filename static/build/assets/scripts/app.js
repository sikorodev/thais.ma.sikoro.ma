define([
    'conf',
    'jquery',
    'Modernizr',
    'createjs',
    'Swiper',
    'Detectizr',
    'waypoint',
    'LazyLoad',
    'async!https://maps.google.com/maps/api/js?sensor=false'
], function (conf, $, Modernizr, createjs, Swiper, Detectizr, waypoint, LazyLoad, gmaps) {

    var sikMpas;
    var mapStyle = [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}];
    sikMpas = {
        $goToTop: $('.c-action__gototop'),

        init: function()
        {
            this._eventinitMap();
        },

        _eventinitMap: function()
        {
            if(!jQuery("#gmap").length) return false;

            var markers = [];

            var options = {

                center: new google.maps.LatLng(33.592998,-7.620568),

                zoom : 16,

                //panControl: false,

                ///zoomControl: false,

                zoomControl: true,

                zoomControlOptions: {

                    style: google.maps.ZoomControlStyle.LARGE,

                    position: google.maps.ControlPosition.RIGHT_BOTTOM

                },

                scaleControl: false,

                streetViewControl: true,

                scrollwheel: false,

                styles: mapStyle,

                draggable : true

            };

            var map = new google.maps.Map(jQuery("#gmap").get(0), options);

            var urlmap="";

            var marker = new google.maps.Marker({

                position: new google.maps.LatLng(33.592998,-7.620568),

                map: map,

                icon: {
                    url: "/assets/media/icons/map-marker-point.svg", // url
                    scaledSize: new google.maps.Size(50, 50), // scaled size
                    origin: new google.maps.Point(0,0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                }

                // shape: shape,

            });

            markers.push(marker);







            //map.setCenter(center);



            // if($(window).width() <= 768) showMarkers(); else clearMarkers();

            google.maps.event.addDomListener(window, "resize", function() {

                //if($(window).width() <= 768) showMarkers(); else clearMarkers();

                var center = map.getCenter();

                google.maps.event.trigger(map, "resize");

                map.setCenter(center);

            });
        }
    };

    var pcaGoToTop;

    pcaGoToTop = {
        $goToTop: $('.c-action__gototop'),

        init: function()
        {
            this._eventHandler();
            this._waypoints();
        },

        _eventHandler: function()
        {
            var self = this;

            this.$goToTop.on('click', function(ev) {
                ev.preventDefault();
                self._goToTop();
            });
        },

        _waypoints: function()
        {
            var self = this;

            if($('.c-action__gototop').length > 0)
            {
                $('body').waypoint({
                    handler: function () {
                        self.$goToTop.toggleClass('is-visible');
                    },
                    offset: -85
                });
            }
            /*$('body').waypoint({
                handler: function () {
                    $("header").toggleClass('is-nav-fixed-level');
                },
                offset: -150
            });

            $('body').waypoint({
                handler: function () {
                    $("header").toggleClass('is-nav-fixed');
                },
                offset: -250
            });*/
        },

        _goToTop: function()
        {
            $('body, html').animate({scrollTop : 0}, 500);

        }
    };

    var pcaOffCanvas;
    var sikNav;

    sikNav = {
        $navItem0 : $('.c-nav__level0 > li'),
        $navItem1 : $('.c-nav__level1 > li'),
        $navSubDisplay : $('.c-nav__show'),
        $navSubHide : $('.c-nav__hide'),
        subMenu : '[class*="c-nav__level"]',

        init: function() {
            this._eventHandler();
        },

        _eventHandler: function() {
            var self = this;

            $('body').on('click.pca', '.c-nav__show', function(ev) {
                self._displayNavSubLevel($(this));
            });

            $('body').on('click.pca', '.c-nav__hide', function(ev) {
                self._hideNavSubLevel($(this));
            });

            $(document).on('CLOSE_OFFCANVAS', function() {
                self._closeAllSubLevel();
            });
        },

        _getSubMenu: function($el) {
            var $subMenu = $el.find(this.subMenu).first();

            return $subMenu;
        },

        _hasSubMenu: function($el) {
            var $subMenu = this._getSubMenu($el);

            if(!$subMenu.length)
            {
                return false;
            }
            else
            {
                return true;
            }
        },

        _toggleActive: function($el) {
            $el.find('a').first().toggleClass('is-active');
        },

        _toggleSubMenu: function(ev, $el) {
            var $subMenu = this._getSubMenu($el);

            $subMenu.toggleClass('is-visible');
        },

        _displayNavSubLevel: function($el) {
            var navSubLevel = $el.next(this.subMenu);
            var navSubLevel__megaMenu = $el.next('.c-nav__megaMenu').find(this.subMenu).first();

            if(navSubLevel__megaMenu.length)
            {
                navSubLevel__megaMenu.addClass('is-inViewport');
            }
            else
            {
                navSubLevel.addClass('is-inViewport');
            }
        },

        _hideNavSubLevel: function($el) {
            var navSubLevel = $el.parents('.is-inViewport').first();

            navSubLevel.removeClass('is-inViewport');
        },

        _closeAllSubLevel: function() {
            this.$navSubHide.parent(this.subMenu).removeClass('is-inViewport');
        },
    };
    pcaOffCanvas = {
        /**
         * Initialize variables
         */
        // icon menu
        $iconHolder: $('.c-nav__icon'),
        // offcanvas
        $menuHolder: $('.o-nav'),
        // overlay for closure
        $closeOffcanvas: $('.c-nav__close'),
        // wrapper offset
        $wrapper : $('body'),
        // scrollTop for fixed nav
        scrollTop : $(window).scrollTop(),
        scrollTopSave : undefined,

        /**
         * Initialize event
         */
        CLOSE_OFFCANVAS : 'CLOSE_OFFCANVAS',

        /**
         * [init] constructor
         */
        init: function() {
            this._eventHolder();
            this._eventTrigger();
            this._updateScrollTop();
        },

        /**
         * Catch click event
         */
        _eventHolder: function() {
            var self = this;

            // click on burger icon
            this.$iconHolder.on('click', function(ev) {
                ev.preventDefault();
                if($(ev.target).hasClass('is-active'))
                {
                    self._closeOffCanvas();
                    $(document).trigger('CLOSE_OFFCANVAS');
                }
                else
                {
                    self._openOffCanvas();
                }
            });

            // bind CLOSE_OFFCANVAS to document
            $(document).on('CLOSE_OFFCANVAS', function() {
                self._closeOffCanvas();
            });

            // pragmatical closure
            $('.o-nav .c-close').on('click.pca', function() {
                self._closeOffCanvas();
            });
        },

        /**
         * Trigger custom event
         */
        _eventTrigger: function() {
            this.$closeOffcanvas.on('click', function(ev) {
                ev.preventDefault();
                $(document).trigger('CLOSE_OFFCANVAS');
            });
        },

        /**
         * Hide / show offcanvas
         */
        _openOffCanvas: function($el) {
            // save scrollTop
            this.scrollTopSave = $(window).scrollTop();

            // swap burger icon <-> cross icon
            this.$iconHolder.toggleClass('is-active');

            // display / hide offcanvas
            this.$menuHolder.toggleClass('is-inViewport');
            this.$wrapper.toggleClass('is-fixed');

            // display / hide $closeOffcanvas
            this.$closeOffcanvas.toggleClass('is-active');

            // udpade $('body') position
            this._updateDomElmntPosition();
        },

        /**
         * listen to CLOSE_OFFCANVAS, for pragmatic closure
         */
        _closeOffCanvas: function() {
            // swap cross icon -> burger icon
            this.$iconHolder.removeClass('is-active');

            // hide offcanvas
            this.$menuHolder.removeClass('is-inViewport');
            this.$wrapper.removeClass('is-fixed');

            // hide $closeOffcanvas
            this.$closeOffcanvas.removeClass('is-active');

            // release body position
            $('body, .c-nav').css({
                top : 0
            });

            $(window).scrollTop(this.scrollTopSave);
        },

        _updateScrollTop: function() {
            var self = this;
            var resizeTimer;

            $(window).on('scroll', function(){
                // lock function during scroll handler
                clearTimeout(resizeTimer);
                resizeTimer = setTimeout(function() {
                    self.scrollTop = $(window).scrollTop();
                }, 250);
            });
        },

        _updateDomElmntPosition: function() {
            var self = this;

            $('body').css({
                'top' : -1*self.scrollTop
            });
        },
    };
    var initialize = {
        init : function () {
            $.noConflict();
            pcaGoToTop.init();
            pcaOffCanvas.init();
            sikNav.init();
            sikMpas.init();
            this.swiperIntro();
            this.swiperActus();
            this.selectEvents();
            //this.hideHeaderOnScroll();
            this.LazyLoadInit();
            //console.log(Detectizr.device.type)
        },
        LazyLoadInit : function(){
            new LazyLoad({
                elements_selector: ".lazy",
                failure_limit: 99999,
                threshold: 10,
            });
        },
        hideHeaderOnScroll: function () {
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            var navbarHeight = $('header').outerHeight();

            $(window).scroll(function (event) {
                didScroll = true;
            });

            setInterval(function () {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 550);

            function hasScrolled() {
                var st = $(this).scrollTop();
                // Make sure they scroll more than delta
                if (Math.abs(lastScrollTop - st) <= delta)
                    return;

                // If they scrolled down and are past the navbar, add class .nav-up.
                // This is necessary so you never see what is "behind" the navbar.
                if (st <= 20) {
                    $('header').removeClass('headactive').removeClass('headactive-second');
                }
                else if (st > lastScrollTop && st > navbarHeight) {
                    // Scroll Down
                    $('header').removeClass('headactive-second').addClass('headactive').removeClass("");
                } else {
                    // Scroll Up
                    if (st + $(window).height() < $(document).height()) {
                        $('header').removeClass('headactive').addClass('headactive-second');
                    }
                }

                lastScrollTop = st;
            }
        },
        selectEvents : function(){
            $('.wizard-form-fields').find("input, textarea").focus(function() {
                $(this).parent(".field").addClass('focus');
            }).blur(function() {
                if($(this).val() == "") $(this).parent(".field").removeClass("focus");
            });
            $(".js-toggle-post").on('click', function(){
                $(this).parents(".job-item").toggleClass("opened");
                return false;
            })
        },
        swiperActus : function(){
            // Params
            if(!$(".main-slider").length) return false;
            var mainSliderSelector = '.main-slider',
                navSliderSelector = '.nav-slider',
                interleaveOffset = 0.5;

// Main Slider
            var mainSliderOptions = {
                loop: true,
                speed:1000,
                autoplay:{
                    delay:3000
                },
                loopAdditionalSlides: 10,
                grabCursor: true,
                watchSlidesProgress: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                on: {
                    init: function(){
                        this.autoplay.stop();
                    },
                    imagesReady: function(){
                        this.el.classList.remove('loading');
                        this.autoplay.start();
                    },
                    slideChangeTransitionEnd: function(){
                        var swiper = this,
                            captions = swiper.el.querySelectorAll('.animated');
                        $(swiper.el).find(".animated").each(function (index, item) {
                            $(item).remove('show');
                        })
                        /*for (var i = 0; i < captions.length; ++i) {
                            captions[i].classList.remove('show');
                        }*/
                        $(swiper.slides).eq(swiper.activeIndex).find(".animated").addClass("show")
                    },
                    progress: function(){
                        var swiper = this;
                        for (var i = 0; i < swiper.slides.length; i++) {
                            var slideProgress = swiper.slides[i].progress,
                                innerOffset = swiper.width * interleaveOffset,
                                innerTranslate = slideProgress * innerOffset;
                            swiper.slides[i].querySelector(".slide-bgimg").style.transform =
                                "translate3d(" + innerTranslate + "px, 0, 0)";
                        }
                    },
                    touchStart: function() {
                        var swiper = this;
                        for (var i = 0; i < swiper.slides.length; i++) {
                            swiper.slides[i].style.transition = "";
                        }
                    },
                    setTransition: function(speed) {
                        var swiper = this;
                        for (var i = 0; i < swiper.slides.length; i++) {
                            swiper.slides[i].style.transition = speed + "ms";
                            swiper.slides[i].querySelector(".slide-bgimg").style.transition =
                                speed + "ms";
                        }
                    }
                }
            };
            var mainSlider = new Swiper(mainSliderSelector, mainSliderOptions);

// Navigation Slider
            var navSliderOptions = {
                loop: true,
                loopAdditionalSlides: 10,
                speed:1000,
                spaceBetween: 5,
                slidesPerView: 5,
                centeredSlides : true,
                touchRatio: 0.2,
                slideToClickedSlide: true,
                direction: 'vertical',
                on: {
                    imagesReady: function(){
                        this.el.classList.remove('loading');
                    },
                    click: function(){
                        mainSlider.autoplay.stop();
                    }
                }
            };
            var navSlider = new Swiper(navSliderSelector, navSliderOptions);

// Matching sliders
            mainSlider.controller.control = navSlider;
            navSlider.controller.control = mainSlider;  
        },
        swiperIntro : function () {
            if(!$(".js-swiper-hp-intro").length) return false;
            new Swiper ('.js-swiper-hp-intro', {
                // Optional parameters
                loop: true,
                speed: 1400,
                spaceBetween: 30,
                centeredSlides: true,
                autoplay: true,
                delay: 5500,
                effect: 'fade',
                //direction: "vertical",
                lazyLoading: true,
                lazy: {
                    loadPrevNext: true,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                onInit : function () {

                },
                onSlideChangeEnd : function(){

                }
            })
        }

    }
    return {
        initialize: initialize
    };
});