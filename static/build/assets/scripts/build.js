({
    baseURL: '.',
    paths: {
        conf: 'conf/appconf',
        jquery: 'vendor/jquery.min',
        Modernizr: 'vendor/modernizr.min',
        Swiper: 'vendor/swiper.min',
        Detectizr: 'vendor/detectizr',
        waypoint: 'vendor/jquery.waypoints.min',
        LazyLoad: 'vendor/lazyload.min',
        'async': 'vendor/requirejs-plugins/src/async'
    },
    shim: {
        conf : {
            exports: 'conf'
        },
        jquery: {
            deps: [],
            exports: '$',
            init: function () {
                return window.jQuery = $;
            }
        },
        Modernizr: {
            exports: 'Modernizr'
        },
        Swiper: {
            exports: 'Swiper'
        },
        Detectizr: {
            exports: 'Detectizr'
        },
        waypoint : {
            exports: 'waypoint',
            deps: ['jquery']
        },
        LazyLoad: {
            exports: 'LazyLoad'
        },
        'async': 'vendor/requirejs-plugins/src/async'
    },

    out: "main.min.js",
    mainConfigFile: 'app.js',
    name: 'main',
    findNestedDependencies: true,
    onBuildRead: function(moduleNames, path, contents) {
        return contents;
        //return contents.replace(/console\.log\(([^\)]+)\);/g, '')
        //              .replace(/debugger;/, '');
    }
})